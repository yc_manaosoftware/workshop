﻿/* eslint-disable no-unused-vars, no-console */
var settings;
var utility;
var app = angular.module('app', []);
var sys = {
    init: function () {
        angular.bootstrap(document, ['app']);
    }
};
$(document).ready(function () {
    var initInjector = angular.injector(['ng']);
    var $http = initInjector.get('$http');
    $http.get('/scripts/src/settings.js').then(
        function (response) {
            angular.element(document).ready(function () {
                sys.init();
            });
        }
    );
    utility = new Utility();
    //$('.eq-height').equalHeight(); //for test equalHeight plugin
    //$('.eq-height').truncateText(); //for test dotdotdot plugin
    if (settings.debug) {
        //console.log('Settings: ', settings);
    }
});/* eslint-enable */
