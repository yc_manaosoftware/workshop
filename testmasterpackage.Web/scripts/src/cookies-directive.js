﻿$(document).ready(function () {
    var cookie = $('#cookies-directive');
    if (cookie.attr('data-cookie-id')) {
        $.cookiesDirective({
            explicitConsent: parseInt(cookie.attr('data-explicit')),
            position: cookie.attr('data-position'),
            duration: parseInt(cookie.attr('data-duration')),
            limit: parseInt(cookie.attr('data-limit')),
            message: cookie.html(),
            acceptText: cookie.attr('data-accept-text'),
            buttonText: cookie.attr('data-button-text')
        });
    }
});
