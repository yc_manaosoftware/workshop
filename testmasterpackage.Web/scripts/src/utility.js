﻿/* eslint-disable no-unused-vars */
function Utility () {
    var context;
    var html = $('html');
    var body = $('body');
    var footer = $('.footer');

    this.init = function () {
        this.info();
        this.settings();
        this.events();
        this.footerAlwaysBottom();
    };

    this.info = function () {
        this.detectBrowsers();
        this.detectOS();
        this.detectLanguage();
        this.detectTouch();
        this.detectViewPort();
    };

    this.settings = function () {
        settings = new Settings(this);
    };

    this.events = function () {
        this.sizeChange();
    };

    this.sizeChange = function () {
        $(window).resize(function () {
            context.detectViewPort();
            context.settings();
        });
    };

    this.detectBrowsers = function () {
        //IE11
        var isIE = false;
        if (navigator.userAgent.match(/Trident\/7\./)) {
            this.browser = 'ie11';
            isIE = true;
        }
        //IE10-9
        else if (navigator.appVersion.indexOf('MSIE') !== -1) {
            var version = parseFloat(navigator.appVersion.split('MSIE')[1]);
            this.browser = 'ie' + version;
            isIE = true;
        }
        else if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
            this.browser = 'firefox';
        }
        else if (navigator.userAgent.indexOf('Safari') !== -1
            && navigator.userAgent.indexOf('Chrome') === -1) {
            this.browser = 'safari';
        }
        else if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
            this.browser = 'chrome';
        }
        else {
            this.browser = 'no-browser';
        }
        if (isIE) {
            html.addClass('ie');
        }
        html.addClass(this.browser);
    };

    this.detectTouch = function () {
        if ('ontouchstart' in window || 'onmsgesturechange' in window) {
            this.touch = true;
            html.addClass('touch');
        }
        else {
            this.touch = false;
        }
    };

    this.detectViewPort = function () {
        var scrollBarWidth = window.innerWidth - body.width();
        if (this.browser === 'safari') {
            scrollBarWidth = 0;
        }
        if ($(window).width() < (768 - scrollBarWidth)) {
            this.isMobile = true;
            this.isTablet = false;
            this.isLabtop = false;
            this.isDesktop = false;
        }
        else if ($(window).width() > (767 - scrollBarWidth) && $(window).width() < (992 - scrollBarWidth)) {
            this.isMobile = false;
            this.isTablet = true;
            this.isLabtop = false;
            this.isDesktop = false;
        }
        else if ($(window).width() > (991 - scrollBarWidth) && $(window).width() < (1200 - scrollBarWidth)) {
            this.isMobile = false;
            this.isTablet = false;
            this.isLabtop = true;
            this.isDesktop = false;
        }
        else {
            this.isMobile = false;
            this.isTablet = false;
            this.isLabtop = false;
            this.isDesktop = true;
        }
    };

    this.detectOS = function () {
        if (navigator.appVersion.indexOf('Win') !== -1) { this.os = 'windows'; }
        else if (navigator.appVersion.indexOf('Mac') !== -1) { this.os = 'macos'; }
        else { this.os = 'no-os'; }
        html.addClass(this.os);
    };

    this.detectLanguage = function () {
        this.lang = html.attr('lang') ? html.attr('lang') : 'no-lang';
    };

    this.footerAlwaysBottom = function () {
        var footerHeight = footer.outerHeight(true);
        body.css('padding-bottom', footerHeight);
        footer.addClass('footerStick');
    };

    context = this;
    this.init();
}/* eslint-enable */

(function ($) {
    $.fn.equalHeight = function () {
        var currentTallest = 0;
        var currentRowStart = 0;
        var topPostion = 0;
        var rowDivs = [];
        var container = this;
        var element;
        var currentDiv;

        function calculateHeight () {
            $(container).each(function () {
                element = $(this);
                $(element).height('auto');
                topPostion = element.position().top;
                if (currentRowStart !== topPostion) {
                    for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                        rowDivs[currentDiv].height(currentTallest);
                    }
                    rowDivs.length = 0;
                    currentRowStart = topPostion;
                    currentTallest = element.height();
                    rowDivs.push(element);
                } else {
                    rowDivs.push(element);
                    currentTallest = (currentTallest < element.height()) ? (element.height()) : (currentTallest);
                }
                for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                    rowDivs[currentDiv].height(currentTallest);
                }
            });
        }
        $(window).resize(function () {
            calculateHeight();
        });
    };
    $.fn.truncateText = function () {
        $(this).dotdotdot({
            ellipsis: '... ',
            wrap: 'word',
            watch: true
        });
    };
}(jQuery));
