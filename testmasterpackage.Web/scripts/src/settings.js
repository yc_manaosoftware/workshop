﻿/* eslint-disable no-unused-vars */
function Settings (utils) {
    this.lang = utils.lang;
    this.os = utils.os;
    this.browser = utils.browser;
    this.isTouch = utils.touch;
    this.isMobile = utils.isMobile;
    this.isTablet = utils.isTablet;
    this.isLabtop = utils.isLabtop;
    this.isDesktop = utils.isDesktop;
    this.debug = true;
}/* eslint-enable */
