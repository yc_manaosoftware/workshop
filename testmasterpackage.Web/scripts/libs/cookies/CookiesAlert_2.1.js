﻿/* Cookies Alert
* By : Manao Software
* Version: 2.1.0
* Updated: January 20th, 2014
*
* Licensed under the cookiesDirective.js License
* Version: 2.0.1
* Author: Ollie Phillips
*/

; (function ($) {
    $.cookiesDirective = function (options) {
        // Default Cookies Directive Settings
        var settings = $.extend({
            //Options
            explicitConsent: true, // false allows implied consent
            position: 'top', // top or bottom of viewport
            duration: 10, // display time in seconds
            limit: 0, // limit disclosure appearances, 0 is forever    
            message: null, // customise the disclosure message  
            acceptText: null, // customise the accept text for explicit consent
            buttonText: null, // customise the button text
            scriptWrapper: function () { }  // wrapper function for cookie setting scripts
        }, options);

        // Perform consent checks
        if (!getCookie('cookiesDirective')) {
            if (settings.limit > 0) {
                // Display limit in force, record the view
                if (!getCookie('cookiesDisclosureCount')) {
                    setCookie('cookiesDisclosureCount', 1, 1);
                } else {
                    var disclosureCount = getCookie('cookiesDisclosureCount');
                    disclosureCount++;
                    setCookie('cookiesDisclosureCount', disclosureCount, 1);
                }

                // Have we reached the display limit, if not make disclosure
                if (settings.limit >= getCookie('cookiesDisclosureCount')) {
                    disclosure(settings);
                }
            } else {
                // No display limit
                disclosure(settings);
            }

            // If we don't require explicit consent, load up our script wrapping function
            if (!settings.explicitConsent) {
                setCookie('implicit', 1, 365);
                settings.scriptWrapper.call();
            }
        } else {
            // Cookies accepted, load script wrapping function
            settings.scriptWrapper.call();
        }
    };

    // Used to load external javascript files into the DOM
    $.cookiesDirective.loadScript = function (options) {
        var settings = $.extend({
            uri: '',
            appendTo: 'body'
        }, options);

        var elementId = String(settings.appendTo);
        var sA = document.createElement("script");
        sA.src = settings.uri;
        sA.type = "text/javascript";
        sA.onload = sA.onreadystatechange = function () {
            if ((!sA.readyState || sA.readyState == "loaded" || sA.readyState == "complete")) {
                return;
            }
        }
        switch (settings.appendTo) {
            case 'head':
                $('head').append(sA);
                break;
            case 'body':
                $('body').append(sA);
                break;
            default:
                $('#' + elementId).append(sA);
        }
    }

    // Helper scripts
    // Get cookie
    var getCookie = function (name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }

    // Set cookie
    var setCookie = function (name, value, days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString();
        }
        else var expires = "";
        document.cookie = name + "=" + value + expires + "; path=/";
    }

    // Detect IE < 9
    var checkIE = function () {
        var version;
        if (navigator.appName == 'Microsoft Internet Explorer') {
            var ua = navigator.userAgent;
            var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
            if (re.exec(ua) != null) {
                version = parseFloat(RegExp.$1);
            }
            if (version <= 8.0) {
                return true;
            } else {
                if (version == 9.0) {
                    if (document.compatMode == "BackCompat") {
                        // IE9 in quirks mode won't run the script properly, set to emulate IE8	
                        var mA = document.createElement("meta");
                        mA.content = "IE=EmulateIE8";
                        document.getElementsByTagName('head')[0].appendChild(mA);
                        return true;
                    } else {
                        return false;
                    }
                }
                return false;
            }
        } else {
            return false;
        }
    }

    // Disclosure routines
    var disclosure = function (options) {
        var settings = options;
        settings.css = 'fixed';

        // IE 9 and lower has issues with position:fixed, either out the box or in compatibility mode - fix that
        if (checkIE()) {
            settings.position = 'top';
            settings.css = 'absolute';
        }

        // Create overlay, vary the disclosure based on explicit/implied consent
        // Set our disclosure/message if one not supplied
        var html = '';
        html += '<div id="cookiesdirective" class="cookieAlert ' + settings.position.toLowerCase() + '">';
        html += '<div class="container">';
        html += '<div class="row">';
        html += '<div class="col-sm-12 col-md-12 col-lg-10 col-lg-offset-1">';
        html += '<div class="cookieAlertContent">';
        html += settings.message;

        // Build the rest of the disclosure for implied and explicit consent
        if (settings.explicitConsent) {
            // Explicit consent disclosure
            // Use "acceptText" value for "Accept text"
            html += '<div class="acceptButtonContainer">' + settings.acceptText + ' <input type="checkbox" name="epdagree" id="epdagree" />&nbsp;';

            // Condition for "Button value"
            // If buttonText is empty, use default value for "Button value"
            if (!settings.buttonText) {
                html += '<a href="javascript:void(0)" name="explicitsubmit" id="explicitsubmit" class="generalButton">Continue</a></div>';
            }
            else // Use "buttonText" value for "Button value"
            {
                html += '<a href="javascript:void(0)" name="explicitsubmit" id="explicitsubmit" class="generalButton">' + settings.buttonText + '</a></div>';
            }
        } else {
            // Implied consent disclosure
            // Condition for "Button value"
            // If buttonText is empty, use default value for "Button value"
            if (!settings.buttonText) {
                // Edited by Manao software, add class="acceptButtonContainer" to div, remove all inline-style
                html += '<a href="#" name="impliedsubmit" id="impliedsubmit" class="generalButton">Do not show this message again</a></div>';
            }
            else // Edited by Manao software, use "buttonText" value for "Button value"
            {
                // Edited by Manao software, add class="acceptButtonContainer" to div, remove all inline-style
                html += '<a href="#" name="impliedsubmit" id="impliedsubmit" class="generalButton">' + settings.buttonText + '</a></div>';
            }
        }
        html += '</div></div></div></div>';
        $('body').append(html);

        // Serve the disclosure, and be smarter about branching
        var dp = settings.position.toLowerCase();
        if (dp != 'top' && dp != 'bottom') {
            dp = 'top';
        }
        var opts = new Array();
        if (dp == 'top') {
            opts['in'] = { 'top': '0' };
            opts['out'] = { 'top': '-300' };
        } else {
            opts['in'] = { 'bottom': '0' };
            opts['out'] = { 'bottom': '-300' };
        }

        // Start animation
        $('#cookiesdirective').animate(opts['in'], 1000, function () {
            // Set event handlers depending on type of disclosure
            if (settings.explicitConsent) {
                // Explicit, need to check a box and click a button
                $('#explicitsubmit').click(function () {
                    var hash = window.location.hash.indexOf('#') > -1 ? window.location.hash.replace('#', '') : '';
                    if ($('#epdagree').is(':checked')) {
                        // Set a cookie to prevent this being displayed again
                        setCookie('cookiesDirective', 1, 365);
                        // Close the overlay
                        $('#cookiesdirective').animate(opts['out'], 1000, function () {
                            // Remove the elements from the DOM and reload page
                            $('#cookiesdirective').remove();

                            //Restore hash before reload
                            if (hash) {
                                window.location.hash = hash;
                            }
                            location.reload(true);
                        });
                    } else {
                        // We need the box checked we want "explicit consent", display message
                        $('#epdnotick').css('display', 'block');
                    }
                });
            } else {
                // Implied consent, just a button to close it
                $('#impliedsubmit').click(function () {
                    // Set a cookie to prevent this being displayed again
                    setCookie('cookiesDirective', 1, 365);
                    // Close the overlay
                    $('#cookiesdirective').animate(opts['out'], 1000, function () {
                        // Remove the elements from the DOM and reload page
                        $('#cookiesdirective').remove();
                    });
                });
            }
            if (settings.duration > 0) {
                // Set a timer to remove the warning after 'settings.duration' seconds
                setTimeout(function () {
                    $('#cookiesdirective').animate({
                        opacity: '0'
                    }, 2000, function () {
                        $('#cookiesdirective').css(dp, '-300px');
                    });
                }, settings.duration * 1000);
            }
        });
    }
})(jQuery);