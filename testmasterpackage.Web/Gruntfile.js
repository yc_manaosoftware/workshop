module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        eslint: {
            options: {
                configFile: "config/eslint/eslint.json",
                rulePaths: ["config/eslint/rules"]
            },
            src: ['scripts/src/**/*.js', 'App_Plugins/Manao.LinkPicker/**/*.js', 'App_Plugins/Manao.MediaPicker/**/*.js', 'App_Plugins/Manao.NodeListPicker/**/*.js']
        },
        copy: {
            angular:
             {
                 expand: true,
                 cwd: 'node_modules/angular/',
                 src: '**/angular.min.js',
                 dest: 'scripts/libs/angular',
             },
            jquery:
              {
                  expand: true,
                  cwd: 'node_modules/jquery/dist/',
                  src: '**/jquery.min.js',
                  dest: 'scripts/libs/jquery',
              },
            jquery_dotdotdot:
              {
                  expand: true,
                  cwd: 'node_modules/dotdotdot/src/js',
                  src: '**/jquery.dotdotdot.min.js',
                  dest: 'scripts/libs/jquery_dotdotdot',
              },
            moment:
              {
                  expand: true,
                  cwd: 'node_modules/moment/min',
                  src: '**/moment-with-locales.min.js',
                  dest: 'scripts/libs/moment',
              },
            font_awesome:
              {
                files:
                [
                    {
                        expand: true,
                        cwd: 'node_modules/font-awesome/css',
                        src: '**/font-awesome.min.css',
                        dest: 'css/libs/font-awesome'
                    },                    
                    {
                        expand: true,
                        cwd: 'node_modules/font-awesome/fonts',
                        src: '**/*',
                        dest: 'css/libs/fonts',
                    }
                ]
              },
            bootstrap:
                {
                    files:
                    [
                        {
                            expand: true,
                            cwd: 'node_modules/bootstrap/dist/css',
                            src: [
                                    '**/bootstrap-theme.min.css',
                                    '**/bootstrap.min.css',
                            ],
                            dest: 'css/libs/bootstrap/',
                        },
                        {
                            expand: true,
                            cwd: 'node_modules/bootstrap/dist/js',
                            src: '**/bootstrap.min.js',
                            dest: 'scripts/libs/bootstrap',
                        },
                        {
                            expand: true,
                            cwd: 'node_modules/bootstrap/dist/fonts',
                            src: '*',
                            dest: 'css/libs/fonts',
                        },
                    ]
                }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('gruntify-eslint');
    grunt.registerTask('default', ['eslint']);
};
