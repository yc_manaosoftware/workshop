angular.module("umbraco").controller("Manao.ToggleBoxes.Controller", function ($scope, $timeout, angularHelper) {
    
    $scope.items = $scope.model.config.items;

    if ($scope.items.length > 0 ) {
        if (!$scope.model.value) {
            $scope.model.value = $scope.model.config.default;
        } else {
            var found = false;
            angular.forEach($scope.items, function (item, index) {
                if (item.value == $scope.model.value) {
                    found = true;
                    return;
                }
            });

            if (!found) {
                $scope.model.value = $scope.model.config.default;
            }
        }
    }
   

    $scope.setValue = function (item, e) {
        $scope.model.value = item.value;
        setActive(item.value, e.target);
        clearStyle(e.target);
    };

    $scope.bindClass = function ($index) {
        if ($index == 0)
            return 'firstItem';
        else if ($index == $scope.model.config.items.length - 1)
            return 'lastItem';
        else
            return 'item';

    }

    $scope.bindStyle = function (item) {
        var style = {};
        if (item.value == $scope.model.value) {
            style = {
                color: item.textColor,
                backgroundColor: item.color,
                borderColor: item.color
            };
        }
        return style;
    }

    $scope.onHover = function (item, e) {
        setActive(item, e.target);
    }

    $scope.onLeave = function (item, e) {
        if (item.value != $scope.model.value) {
            clearStyle(e.target);
        }
    }

    function clearStyle(element) {
        var boxes =$(element).parents('.manaoToggleBoxes');
        boxes.find('button[data-value!=' + $scope.model.value + ']').each(function () {
            $(this).css('background-color', '');
            $(this).css('border-color', '');
            $(this).css('color', '');
        });
    }

    function setActive(item, element) {
        $(element).css('background-color', item.color);
        $(element).css('border-color', item.color);
        $(element).css('color', item.textColor);
    }
});