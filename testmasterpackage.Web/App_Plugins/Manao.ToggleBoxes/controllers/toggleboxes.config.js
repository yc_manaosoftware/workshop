angular.module("umbraco").controller("Manao.ToggleBoxes.ConfigController", function ($scope, $timeout, angularHelper, serverValidationManager) {

    $scope.sortableOptions = {
        disabled: false
    }

    if (!$scope.model.value) {
        $scope.model.value = [];
    }

    $scope.add = function (e) {
        var item = { value: $scope.newItemValue || $scope.newItemText, text: $scope.newItemText, textColor: $scope.newItemTextColor, color: $('.colorpicker').val() };
        $scope.model.value.push(item);
        $scope.newItemValue = '';
        $scope.newItemText = '';
        $scope.newItemColor = '';
        console.log('Add: ', item);
        $timeout(function () {
            $scope.init();
        }, 0);

    };

    $scope.remove = function (index) {
        $scope.model.value.splice(index, 1);
    };

    $scope.init = function () {
        $scope.newItemTextColor = '#fff';

        $timeout(function () {
            $('.colorpicker').each(function () {
                $(this).minicolors({
                    hide: function () {
                        $scope.sortableOptions.disabled = false;
                    }
                });

                $(this).on('click', function () {
                    $scope.sortableOptions.disabled = true;
                });
            });
        }, 100);
    }

    function activeColorValidation() {
        var result = true;
        _.each($scope.model.value, function (item) {
            var isHex = /^#[0-9A-F]{6}$/i.test(item.color);
            if (!isHex) {
                result = false;
                return;
            }
        });
        return result;
    }

    function valueValidation() {
        var result = true;
        _.each($scope.model.value, function (firstLoop, indexX) {
            _.each($scope.model.value, function (secondLoop, indexY) {
                if (indexX != indexY && firstLoop.value == secondLoop.value) {
                    result = false;
                    return;
                }
            });
        });
        return result;
    }

    $scope.$on("formSubmitting", function (e, params) {
        if (!activeColorValidation() || !valueValidation()) {
            serverValidationManager.addPropertyError($scope.model.alias, null, 'Please make sure value is not duplicate and please make sure color code is valid.');
        }
    });

});