﻿angular.module("umbraco").controller("RobotsController", function ($scope, $routeParams, $element, dictionaryResource, angularHelper) {
    var textbox = $($element).find('.value-box');
    var checkbox = $($element).find('.checkbox');

    $scope.isDirty = false;

    $scope.getText = function () {
        $scope.text = {};
        dictionaryResource.getText("Robots").then(function (response) { 
                $scope.text.automatic = response.data["[Robots] Automatic"];
                $scope.text.disallowall = response.data["[Robots] Disallow all"];
                $scope.text.allowall = response.data["[Robots] Allow all"];
                $scope.text.disallowdirectory = response.data["[Robots] Disallow directory"];
        });
    }
    $scope.getText();

    if (!$scope.model.value) {
        $scope.model.value = {};
        $scope.model.value.option = 0;
        $scope.model.value.content = getDisallowAll();
        $scope.model.value.showcontent = $scope.model.value.content;
    }

    $scope.setTo = function (option) {
        $scope.model.value.option = option;
        $scope.setCheckbox();

        if (option == 0) {
            $scope.model.value.content = getDisallowAll();
            $scope.model.value.showcontent = $scope.model.value.content;
        }
        else if (option == 1) {
            $scope.model.value.content = getAllowAll();
            $scope.model.value.showcontent = $scope.model.value.content;
        }
        else if (option == 2) {
            $scope.model.value.content = getDisallowUmbraco();
            $scope.model.value.showcontent = $scope.text.automatic;
        }

        $scope.setDirty();
    }

    $scope.setCheckbox = function () {
        $(checkbox).removeClass('check');
        $(checkbox[$scope.model.value.option]).addClass('check');
    }
    $scope.setCheckbox();

    $scope.setDirty = function () {
        if (!$scope.isDirty) {
            $scope.isDirty = true;
            var currentForm = angularHelper.getCurrentForm($scope);
            currentForm.$setDirty();
        }
    }
});

function getDisallowAll() {
    return  'User-agent: *' + '\n' +
            'Disallow: /';
}
function getDisallowUmbraco() {
    return  'User-agent: *' + '\n' +
            'Disallow: /aspnet_client/' + '\n' +
            'Disallow: /bin/' + '\n' +
            'Disallow: /config/' + '\n' +
            'Disallow: /data/' + '\n' +
            'Disallow: /install/' + '\n' +
            'Disallow: /masterpages/' + '\n' +
            'Disallow: /umbraco/' + '\n' +
            'Disallow: /umbraco_client/' + '\n' +
            'Disallow: /usercontrols/' + '\n' +
            'Disallow: /xslt/';
}
function getAllowAll() {
    return  'User-agent: *' + '\n' +
            'Disallow:';    
}