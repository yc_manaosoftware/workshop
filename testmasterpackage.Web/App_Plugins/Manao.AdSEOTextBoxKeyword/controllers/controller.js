﻿
angular.module("umbraco").controller("textBoxKeywordController", function ($scope, dialogService, $timeout, $rootScope, $compile, dictionaryResource, angularHelper, notificationsService, shareSeoDataService) {
    $scope.isDirty = false;
    dictionaryResource.getText("[SEO] Plugins").then(function (response) {
        $scope.dictionary = response.data;
    });
    $scope.checkSpace = function (keywords) {
        return keywords.replace(/\s+/g, " ");
    }

    if (!$scope.model.value) {
        $scope.model.keywordList = [];
        $scope.model.keyword = '';
    }
    else {
        $scope.model.keyword = $scope.model.value;
        $scope.model.keywordList = $scope.checkSpace($scope.model.value).split(" ");
    }

    $scope.updateKeyword = function () {
        $scope.model.value = $scope.checkSpace($scope.model.keyword);
        if ($scope.model.value) {
            $scope.model.keywordList = $scope.model.value.split(" ");
        }
        else {
            $scope.model.keywordList = [];
        }

    }


    $scope.$watch('[model.keywordList,model.keyword]', function (newValue, oldValue) {
        if (newValue[0] != oldValue[0] || newValue[1] != oldValue[1]) {
            $scope.setDirty();
        }
    }, true);


    $scope.setDirty = function () {
        if (!$scope.isDirty) {
            $scope.isDirty = true;
            var currentForm = angularHelper.getCurrentForm($scope);
            currentForm.$setDirty();
        }
    }
    $scope.$on("formSubmitting", function (ev, args) {
        $scope.messageInfo = '';
        $scope.messageShow = '';
        var ordermessage = false;

        $scope.updateKeyword();

        if (!$scope.model.keywordList.length) {
            $scope.messageShow = $scope.dictionary['[SEO] Info Headline'] + ': ' + $scope.dictionary['[SEO] No Keyword Message'];
        }
        else {
            $scope.model.keywordList.forEach(function (obj) {
                $scope.Objhasfound = shareSeoDataService.checkHasFoundKeyword(obj);
                obj = obj.toLowerCase();
                if (!$scope.Objhasfound.title) {
                    $scope.messageInfo += $scope.dictionary['[SEO] Warning Headline'] + ': ' + $scope.dictionary['[SEO] Textbox Keyword Warning Message'].replace("{0}", "<strong>" + obj + "</strong>").replace("{1}", "<strong>Title</strong>");
                    ordermessage = true;
                }
                if (!$scope.Objhasfound.desc) {
                    if (ordermessage) {
                        $scope.messageInfo += " and <strong>Description</strong>";
                    }
                    else {
                        $scope.messageInfo += $scope.dictionary['[SEO] Warning Headline'] + ': ' + $scope.dictionary['[SEO] Textbox Keyword No Keyword Message'].replace("{0}", "<strong>" + obj + "</strong>").replace("{1}", "<strong>Description</strong>");

                    }
                }
                if (!$scope.Objhasfound.title || !$scope.Objhasfound.desc) {
                    $scope.messageInfo += '<br>';
                    $scope.messageShow = $scope.messageInfo;
                }
                ordermessage = false;
            });
        }
        shareSeoDataService.setMessage($scope.messageShow);

    });
});

