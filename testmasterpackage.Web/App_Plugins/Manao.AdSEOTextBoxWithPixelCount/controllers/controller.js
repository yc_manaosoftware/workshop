﻿angular.module("umbraco")
.factory("calWidth", function () {

    $(document).ready(function () {
        $.fn.textWidth = function (text, font) {
            if (!$.fn.textWidth.fakeEl) $.fn.textWidth.fakeEl = $('<span>').hide().appendTo(document.body);
            $.fn.textWidth.fakeEl.text(text || this.val() || this.text()).css('font', font || this.css('font'));
            return $.fn.textWidth.fakeEl.width();
        };
    });

    return function (text) {
        return $.fn.textWidth(text, '18px arial');
    };

})
angular.module("umbraco").controller("textBoxWithPixelCountController", function ($scope, $element, dialogService, $timeout, $rootScope, $compile, dictionaryResource, angularHelper, notificationsService, calWidth, shareSeoDataService) {

    $scope.isDirty = false;

    dictionaryResource.getText("[SEO] Plugins").then(function (response) {
        $scope.dictionary = response.data;
    });
    // Loads Default Max Count
    $scope.model.maxWidthCount = $scope.model.config.maxCount;
    //tranfer model to sharedata 
    shareSeoDataService.setTitle($scope.model.value, $scope.model.maxWidthCount);


    // Loads Default remainingCount
    if (!$scope.model.value) {
        $scope.model.remainingCount = 0;
    }


    $scope.calTextWidth = function () {
        shareSeoDataService.setTitle($scope.model.value, $scope.model.maxWidthCount);
        $scope.model.className = "number-counter-black";
        $scope.model.remainingCount = calWidth($scope.model.value);

        // Set the correct CSS class       
        if ($scope.model.remainingCount > $scope.model.maxWidthCount) {
            $scope.model.className = "number-counter-red";
        }

    };


    $scope.calTextWidth();
    $scope.$watch("model.value", function (newValue, oldValue) {
        if (newValue != oldValue) {
            $scope.setDirty();
        }
    }, true);

    $scope.setDirty = function () {
        if (!$scope.isDirty) {
            $scope.isDirty = true;
            var currentForm = angularHelper.getCurrentForm($scope);
            currentForm.$setDirty();
        }
    }

    $scope.$on("formSubmitting", function (ev, args) {
        $scope.messageShow = '';
        if ($scope.model.remainingCount > $scope.model.maxWidthCount) {
            $scope.messageShow = $scope.dictionary['[SEO] Warning Headline'] + ': ' + $scope.dictionary['[SEO] PixelCount Warning Message'];
        }
        if (!$scope.model.value) {
            $scope.messageShow = $scope.dictionary['[SEO] Info Headline'] + ': ' +$scope.dictionary['[SEO] PixelCount Info Message'];
        }
        if ($scope.messageShow) {
            shareSeoDataService.setMessage($scope.messageShow);
        }

    });

});

