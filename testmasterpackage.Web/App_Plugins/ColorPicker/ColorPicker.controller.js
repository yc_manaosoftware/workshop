﻿var colorPicker = angular.module('umbraco');

colorPicker.controller('PropertyEditors.ColorPickerController', function ($scope, $compile, $timeout) {

	$scope.model.items = [{
		className: "color1",
	}, {
		className: "color2",
	}, {
		className: "color3",
	}];
});