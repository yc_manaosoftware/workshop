﻿var positionPicker = angular.module('umbraco');

positionPicker.controller('PrevalueEditors.ColorPickerController', function ($scope, $compile, $timeout, $http, notificationsService) {

    if (!$scope.model && !$scope.model.value)
        $scope.model.value = [];

    $scope.add = function (evt) {
        evt.preventDefault();

        var newItem = {
            className: $scope.newItem
        };

        $scope.model.value.push(newItem);

        $scope.newItem = "";
    };
    $scope.remove = function (color, evt) {
        evt.preventDefault();

        $scope.model.value = _.reject($scope.model.value, function (x) {
            return x.className === color.className;
        });
    }

});