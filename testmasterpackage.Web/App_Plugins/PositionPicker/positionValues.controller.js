﻿var positionPicker = angular.module('umbraco');

positionPicker.controller('positionValuesController', function ($scope, $compile, $timeout, $http, notificationsService) {


    $http.get("/Umbraco/surface/Position/GetPositions").then(function (response) {
        $scope.positions = response.data;
    });

    $scope.add = function (evt) {
        evt.preventDefault();


        if ($scope.positionName) {

            var duplicate = _.filter($scope.positions, function (position) {
                return $scope.positionName.trim().toLowerCase() == position.PositionName.trim().toLowerCase();
            });

            if (!duplicate || duplicate.length == 0) {

                var position = { PositionName: $scope.positionName.trim(), IsManager: $scope.isManager };
                $http.post("/Umbraco/surface/Position/AddPosition", position).then(function (response) {
                    $scope.positions.push(position);

                    $scope.positionName = "";
                    $scope.isManager = false;

                    $scope.hasError = false;

                    notificationsService.success("Success", "Add " + position.PositionName  + " to database successful");

                    return;
                });

                return;
            }
        }

        //there was an error, do the highlight (will be set back by the directive)
        $scope.hasError = true;
    };

    $scope.remove = function (position, evt) {
        evt.preventDefault();

        $http.post("/Umbraco/surface/Position/DeletePosition", position).then(function (response) {
            debugger;
            if (response.data.IsSuccess) {
                $scope.positions = _.reject($scope.positions, function (x) {
                    return x.Id === position.Id;
                });

                notificationsService.success("Success", "Delete " + position.PositionName + " from database successful");
            }
        });

        $scope.positions = _.reject($scope.positions, function (x) {
            debugger;
            if (x.Id === position.Id) {
                $http.post("/Umbraco/surface/Position/DeletePosition", position).then(function (response) {

                    return response.data;
                });
            } else {
                return false;
            }
        });
    };

    $scope.edit = function (positionEdit, evt) {
        evt.preventDefault();

        var duplicate = _.filter($scope.positions, function (position) {
            return position.Id != positionEdit.Id && position.PositionName.trim().toLowerCase() == positionEdit.PositionName.trim().toLowerCase();
        });


        if (duplicate && duplicate.length > 0) {
            notificationsService.error("Failed", "Position can't duplicate");
        } else {
            var position = { Id: positionEdit.Id, PositionName: positionEdit.PositionName.trim(), IsManager: positionEdit.IsManager };
            $http.post("/Umbraco/surface/Position/EditPosition", position).then(function (response) {
                if (response.data.IsSuccess) {
                    notificationsService.success("Success", "Edit " + position.PositionName + " in database successful");
                } else {
                    if (!response.data.ValidationResult.IsValid) {
                        angular.forEach(response.data.ValidationResult.Errors, function (error, key) {
                            notificationsService.error("Error", error.PropertyName + ": " + error.ErrorMessage);
                        });
                    }
                    else {
                        notificationsService.error("Error", response.data.ErrorMessage);
                    }
                }
            });
        }
    }

});