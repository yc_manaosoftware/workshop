﻿var positionPicker = angular.module('umbraco');

positionPicker.controller('PositionPickerController', function ($scope, $compile, $timeout, $http) {

    $http.get("/Umbraco/surface/Position/GetPositions")
    .then(function (response) {
        $scope.positions = response.data;
    });
});