angular.module("umbraco").controller("manaoDropdownController", function ($scope, angularHelper) {
    if ($scope.model.value == '') {
        if ($scope.model.config.options.length) {
            $scope.dropdown = $scope.model.config.options[0];
            $scope.model.value = $scope.dropdown.value;
        }
    }
    else {
        var index = 0;
        var option= $scope.model.config.options.filter(function (option)
        {
            if (option.value == $scope.model.value) {
                $scope.dropdown = $scope.model.config.options[index];
            }
            index++;
        });
    }
    $scope.selectDropdown = function () {
        $scope.model.value = $scope.dropdown.value;
    }
    $scope.$watch('model.value', function (n,o) {
        
        if (n != o) {
            $scope.setDirty();
        }
    });
    $scope.setDirty = function () {
        if (!$scope.isDirty) {
            $scope.isDirty = true;
            var currentForm = angularHelper.getCurrentForm($scope);
            currentForm.$setDirty();
        }
    }
});