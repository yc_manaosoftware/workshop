angular.module('umbraco').controller('ManaoMediaPickerController',
function ($scope, dialogService, angularHelper, dictionaryResource, serverValidationManager, notificationsService) {
    var model = $scope.model;
    var settingView = '/App_Plugins/Manao.MediaPicker/views/panel.html';
    var dictionaryKey = 'Custom Media Picker';
    var editable = ($scope.model.contentTypeId === undefined);
    $scope.valid = true;

    $scope.openSetting = function () {
        dialogService.open({
            template: settingView,
            callback: function (response) {
                model.value = response;
            },
            dialogData: {
                model: angular.copy(model.value),
                imageOnly: model.config.imageOnly == 1,
                dictionary: $scope.dictionary
            }
        });
    };

    $scope.remove = function () {
        model.value.Guid = '00000000-0000-0000-0000-000000000000';
        model.value.Url = '';
        model.value.Title = '';
        model.value.Alt = '';
        model.value.Caption = '';
    };

    $scope.$on('formSubmitting', function () {
        if (editable) {
            validate();
            if (!$scope.valid) {
                serverValidationManager.addPropertyError($scope.model.alias, 'value', 'Value cannot be empty');
                notificationsService.error('Error', 'Value cannot be empty');
            }
        }
    });

    if (model) {
        if (!model.value) {
            $scope.model.value = {
                Guid: '00000000-0000-0000-0000-000000000000',
                Url: '',
                Title: '',
                Alt: '',
                Caption: ''
            };
        }
        $scope.validation = {
            isEmpty: false
        };
    }

    function getDictionaries () {
        dictionaryResource.getText(dictionaryKey).then(function (response) {
            $scope.dictionary = response.data;
        });
    }

    function validate () {
        if (model.validation.mandatory || model.config.required == true) {
            $scope.valid = model.value.Url ? true : false;
        }

        if ($scope.valid) {
            serverValidationManager.removePropertyError($scope.model.alias, 'value');
        }

    }

    $scope.$watch('model.value', function (newValue, oldValue) {
        if (newValue != oldValue) {
            $scope.setDirty();
        }
    }, true);


    $scope.setDirty = function () {
        if (!$scope.isDirty) {
            $scope.isDirty = true;
            var currentForm = angularHelper.getCurrentForm($scope);
            currentForm.$setDirty();
        }
    };

    getDictionaries();

});
