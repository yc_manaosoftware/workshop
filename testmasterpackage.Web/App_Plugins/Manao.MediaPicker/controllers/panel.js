﻿angular.module('umbraco').controller('ManaoMediaPickerSettingController',
function ($scope, dialogService, ManaoMediaPickerService) {
    $scope.model = $scope.dialogData.model;
    $scope.imageOnly = $scope.dialogData.imageOnly;
    $scope.dictionary = $scope.dialogData.dictionary;

    $scope.selectImage = function () {
        select();
    };

    $scope.remove = function () {
        reset();
    };

    $scope.save = function () {
        $scope.submit($scope.model);
    };

    function select () {
        dialogService.mediaPicker({
            startNodeId: -1,
            multiPicker: false,
            onlyImages: $scope.imageOnly,
            callback: function (data) {
                ManaoMediaPickerService.getMedia(data.key).then(function (response) {
                    $scope.model.Url = response.data.Url;
                    $scope.model.Alt = $scope.model.Alt ? $scope.model.Alt : response.data.Alt;
                    $scope.model.Title = $scope.model.Title ? $scope.model.Title : response.data.Title;
                    $scope.model.Guid = response.data.Guid;
                    $scope.model.Type = data.contentTypeAlias;
                });
            }
        });
    }

    function reset () {
        $scope.model.Guid = '00000000-0000-0000-0000-000000000000';
        $scope.model.Url = '';
    }

});
