﻿angular.module('umbraco').factory('ManaoMediaPickerService', ['$http',
    function ($http) {
        return {
            getMedia: function (guid) {
                return $http.get('/Umbraco/Api/ManaoMediaApi/GetImage?guid=' + guid);
            }
        };
    }
]);
