﻿angular.module('umbraco').factory('NodeListPickerPickerService', ['$http',
    function ($http) {
        return {
            getList: function (parents, childs, currentNodeId, allowOnlyRoot) {
                return $http.get('/umbraco/backoffice/api/ManaoNodeListApi/GetByAlias?parents=' + parents + '&childs=' + childs + '&currentNodeId=' + currentNodeId + '&allowOnlyRoot=' + allowOnlyRoot);
            },
            getNodeByModel: function (data) {
                return $http.post('/umbraco/backoffice/api/ManaoNodeListApi/GetNodeByModel', data);
            },
            getNodeByModels: function (data) {
                return $http.post('/umbraco/backoffice/api/ManaoNodeListApi/GetNodeByModels', data);
            }
        };
    }
]);
