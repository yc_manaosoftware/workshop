﻿angular.module('umbraco').controller('NodeListPickerPanelController', ['$scope', '$timeout',
    function ($scope, $timeout) {

        $scope.isMultiple = false;
        $scope.values = [];
        $scope.error = {};

        $scope.init = function () {
            $scope.values = $scope.dialogData.values;
            $scope.synValues = angular.copy($scope.dialogData.values);
            $scope.models = angular.copy($scope.dialogData.model);
            $scope.nodes = angular.copy($scope.dialogData.model);
            $scope.syncValue($scope.nodes, $scope.synValues);
            $scope.config = $scope.dialogData.config;
            $scope.isMultiple = $scope.config.multiple === '1' ? true : false;
            $scope.setPanelHeight();

        };

        $scope.syncValue = function (nodes, values) {
            $scope.clearSelected(nodes);
            if (Array.isArray(values)) {
                $scope.tempValues = [];
                $scope.syncSelectedListArray(nodes, values);
            }
            else {
                $scope.syncSelectedNode(nodes, values);
            }
        };

        $scope.clearSelected = function (nodes) {
            angular.forEach(nodes, function (node) {
                if (node.childs.length) {
                    $scope.clearSelected(node.childs);
                }
                node.selected = false;
            });
        };

        $scope.syncSelectedListArray = function (nodes, values) {
            var tempNode = {};
            if (values.length) {
                angular.forEach(nodes, function (node) {
                    angular.forEach(values, function (selectedNode, index) {
                        if (node.childs.length) {
                            $scope.syncSelectedListArray(node.childs, values);
                        }
                        if (node.Id === selectedNode.Id) {
                            if (!node.selected) {
                                if (values.length > 1) {
                                    values.splice(index, 1);
                                }
                                //$scope.tempValues.push(node);
                            }
                            node.selected = true;
                            tempNode = node;
                        }
                    });
                });

                angular.forEach($scope.values, function (node) {
                    if (tempNode.Id == node.Id) {
                        tempNode = node;
                    }
                });
                //$scope.values = angular.copy($scope.tempValues);
            }
            else {
                values = [];
            }
        };

        $scope.syncSelectedNode = function (nodes, value) {
            if (value) {
                angular.forEach(nodes, function (node) {
                    if (node.childs.length) {
                        $scope.syncSelectedNode(node.childs, value);
                    }
                    if (node.Id === value.Id) {
                        node.selected = true;
                    }
                });
            }
        };

        $scope.selected = function (node) {
            if (node.isAllowed) {
                if ($scope.isMultiple) {
                    if (node.selected) {
                        node.selected = false;
                        var index = 0;
                        var keepIndex = -1;
                        angular.forEach($scope.values, function (currentNode) {
                            if (currentNode.Id === node.Id) {
                                keepIndex = index;
                            }
                            index++;
                        });
                        if (keepIndex > -1) {
                            $scope.values.splice(keepIndex, 1);
                        }
                    }
                    else {
                        node.selected = true;
                        if (!$scope.values) {
                            $scope.values = [];
                        }
                        $scope.values.push(node);
                    }
                    $scope.validateMaxMin();
                }
                else {
                    $scope.submit(node);
                }
            }
        };

        $scope.done = function () {
            if ($scope.isMultiple) {
                if ($scope.validateMaxMin()) {
                    $scope.submit($scope.values);
                }
            }
            else {
                $scope.submit($scope.values);
            }
        };

        $scope.search = function () {
            $scope.models = angular.copy($scope.dialogData.model);
            if ($scope.keyword) {
                $scope.nodes = $scope.filter($scope.models);
                $scope.syncValue($scope.nodes, $scope.values);
            }
            else {
                $scope.nodes = angular.copy($scope.dialogData.model);
                $scope.syncValue($scope.nodes, $scope.values);
            }
        };

        $scope.filter = function (models) {
            var results = [];
            angular.forEach(models, function (node) {
                var tempChilds = angular.copy(node.childs);
                node.childs = $scope.filter(tempChilds);
                if (node.Name.toLowerCase().indexOf($scope.keyword.toLowerCase()) > -1) {
                    results.push(node);
                    node.isMatch = true;
                    node.expand = true;
                }
                else if (node.childs.length) {
                    node.expand = true;
                    results.push(node);
                }
            });
            return results;
        };

        $scope.setPanelHeight = function () {
            $timeout(function () {
                $('.node-pickers .node-list').height($(window).height() - 280);
            }, 500);
        };

        $scope.init();

        $scope.validateMaxMin = function () {
            //console.log('here');
            //console.log($scope.config);
            //console.log($scope.values.length);
            if (($scope.values.length <= parseInt($scope.config.max)) || $scope.config.max === '0' || !$scope.config.max) {
                $scope.error.max = false;
            }
            else {
                $scope.error.max = true;
            }
            if (($scope.values.length >= parseInt($scope.config.min)) || $scope.config.min === '0' || !$scope.config.min) {
                $scope.error.min = false;
            }
            else {
                $scope.error.min = true;
            }
            //console.log($scope.error);
            return !($scope.error.min || $scope.error.max);
        };

        $scope.adjustNodePosition = function (e) {
            e = angular.element(e.target);
            var list = e.closest('li');
            var level = e.parents('ul').length;
            $timeout(function () {
                var padLeft = (level * 10) / (level / 2) + 'px';
                list.find('li').css('padding-left', padLeft);
            }, 1);
        };

        $(window).resize(function () {
            $('.node-pickers .node-list').height($(window).height() - 280);
        });
    }]);
