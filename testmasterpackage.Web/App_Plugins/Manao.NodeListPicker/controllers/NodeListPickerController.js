﻿angular.module('umbraco').controller('NodeListPickerController',
    ['$scope', '$http', '$routeParams', '$element', 'angularHelper', 'dialogService', 'editorState', 'NodeListPickerPickerService', 'contentTypeResource',
        function ($scope, $http, $routeParams, $element, angularHelper, dialogService, editorState, NodeListPickerPickerService, contentTypeResource) {

        $scope.dataLoaded = false;

        function onSubmitted (value) {
            $scope.model.value = value;
            $scope.validateMaxMin();
        }

        $scope.init = function () {
            $scope.error = {};
            if ($scope.model.config.parents && $scope.model.config.childs) {
                if ($scope.model.config.allowOnlyRoot == null) {
                    $scope.model.config.allowOnlyRoot = 0;
                }
                NodeListPickerPickerService.getList($scope.model.config.parents, $scope.model.config.childs,editorState.current.key, $scope.model.config.allowOnlyRoot).then(function (res) {
                    setNodeList(res.data);
                });
            }
            $scope.correctConfig();
            $scope.correctModel();
            $scope.validateMaxMin();
            $scope.synchronization();

            if ($scope.model.config.debug === '1') {
                /* eslint-disable no-console */
                console.log('CONFIG: ', $scope.model.config);
                /* eslint-enable */
            }
        };

        $scope.correctConfig = function () {
            if ($scope.model.config.multiple === '1') {
                if (isNaN(parseInt($scope.model.config.max)) || parseInt($scope.model.config.max) < 0) {
                    $scope.model.config.max = 0;
                }
                if (isNaN(parseInt($scope.model.config.min)) || parseInt($scope.model.config.min) < 0) {
                    $scope.model.config.min = 0;
                }
            }
        };

        $scope.correctModel = function () {
            if ($scope.model.config.multiple === '1') {
                if (!$scope.model.value) {
                    $scope.model.value = [];
                }
                else if (!Object.prototype.toString.call($scope.model.value) === '[object Array]') {
                    $scope.model.value = [];
                }
            }
            else {
                if (!$scope.model.value) {
                    $scope.model.value = {};
                }
                else if (Object.prototype.toString.call($scope.model.value) === '[object Array]') {
                    $scope.model.value = {};
                }
            }
        };

        $scope.removeItem = function (index) {
            $scope.model.value.splice(index, 1);
            $scope.validateMaxMin();
        };

        $scope.openPicker = function () {
            if ($scope.dataLoaded) {
                var editor = '/App_Plugins/Manao.NodeListPicker/views/panel.html';
                dialogService.open({
                    template: editor,
                    callback: function (data) {
                        onSubmitted(data);
                    },
                    dialogData: {
                        model: $scope.nodes,
                        config: $scope.model.config,
                        values: angular.copy($scope.model.value)
                    }
                });
            }
        };

        $scope.removeModel = function () {
            $scope.model.value = undefined;
        };

        $scope.validateMaxMin = function () {
            if ($scope.model.config.multiple === '1') {
                var currentForm = angularHelper.getCurrentForm($scope);
                if (($scope.model.value.length <= $scope.model.config.max) || $scope.model.config.max === 0) {
                    $scope.error.max = false;
                }
                else {
                    $scope.error.max = true;
                }
                if (($scope.model.value.length >= $scope.model.config.min) || $scope.model.config.min === 0) {
                    $scope.error.min = false;
                }
                else {
                    $scope.error.min = true;
                }
                if ($scope.error.min || $scope.error.max) {
                    currentForm.$setValidity('model.value', false);
                }
                else {
                    currentForm.$setValidity('model.value', true);
                }
                return (($scope.model.value.length <= $scope.model.config.max) || $scope.model.config.max === 0) &&
                    (($scope.model.value.length >= $scope.model.config.min) || $scope.model.config.min === 0);
            }
            return true;
        };

        $scope.reset = function () {
            if ($scope.model.config.multiple === '1') {
                $scope.model.value = [];
            }
            else {
                $scope.model.value = {};
            }
        };

        $scope.synchronization = function () {
            if ($scope.model.value && $scope.model.config.multiple === '1' && $scope.model.value.length > 0)
            {
                NodeListPickerPickerService.getNodeByModels($scope.model.value).then(function (res) {
                    //console.log(res);
                    if (res.data !== 'null') {
                        $scope.model.value = res.data;
                    }
                    else {
                        $scope.model.value = [];
                    }
                });
            }
            else if (Object.prototype.toString.call($scope.model.value) === '[object Object]' && $scope.model.value)
            {
                NodeListPickerPickerService.getNodeByModel($scope.model.value).then(function (res) {
                    //console.log(res);
                    if (res.data !== 'null')
                    {
                        $scope.model.value = res.data;
                    }
                    else {
                        $scope.model.value = {};
                    }
                });
            }
        };


        function setNodeList(nodeList) {
            contentTypeResource.getAll().then(function (contentTypeList) {
                setNodeIcon(nodeList, contentTypeList);
                $scope.dataLoaded = true;
            });
        };

        function setNodeIcon(nodeList, contentTypeList) {
            angular.forEach(nodeList, function (node) {
                angular.forEach(contentTypeList, function (contentType) {
                    if (contentType.id == node.DocumentTypeId) 
                        node.Icon = contentType.icon;
                });
                if (node.childs != null && node.childs.length > 0)
                    setNodeIcon(node.childs, contentTypeList);
            });
            $scope.nodes = nodeList;
        }

        $scope.init();
    }]);
