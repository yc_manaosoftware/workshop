angular.module("umbraco").controller("ManaoDashboardController", function ($scope, $routeParams, dialogService, dictionaryResource) {
    $scope.text = {};
    $scope.getText = function () {
        dictionaryResource.getText("Dashboard").then(function (response) {
            $scope.text.title = response.data['[Dashboard] Title'];
            $scope.text.subtitle = response.data['[Dashboard] Subtitle'];
            $scope.text.pleasecontact = response.data['[Dashboard] Please Contact'];
            $scope.text.contactemail = response.data['[Dashboard] Contact Email'];
            $scope.text.for = response.data['[Dashboard] For'];
            $scope.text.support = response.data['[Dashboard] Support'];
            $scope.text.extensions = response.data['[Dashboard] Extensions'];
            $scope.text.offers = response.data['[Dashboard] Offers'];
        });
    }
    $scope.getText();
});
