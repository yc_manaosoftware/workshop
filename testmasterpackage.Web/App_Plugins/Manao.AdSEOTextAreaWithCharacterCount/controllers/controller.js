﻿angular.module("umbraco").controller("textAreaWithCharacterCountCountController", function ($scope, dialogService, $timeout, $rootScope, $compile, dictionaryResource, notificationsService, angularHelper, shareSeoDataService) {

    $scope.isDirty = false;

    dictionaryResource.getText("[SEO] Plugins").then(function (response) {
        $scope.dictionary = response.data;
    });
  
    // Loads Default Max Count
    $scope.model.maxCount = $scope.model.config.maxCount;

    // Tranfer model to sharedata 
    shareSeoDataService.setDescription($scope.model.value, $scope.model.maxCount);


    // Attempts to re-define Max Count by description text
    if ($scope.model.description) {
        var maxSetInDescription = $scope.model.description.match(/\d+/);
        if (maxSetInDescription) {
            var newMax = parseInt(maxSetInDescription[0]);
            if (newMax > 0)
                $scope.model.maxCount = $scope.model.description.match(/\d+/)[0];
        }
    }

    // Calculate a low and medium range so we can set the CSS appropriately
    var low = Math.ceil($scope.model.maxCount * 0.25);
    var med = Math.ceil($scope.model.maxCount * 0.5);

    // Called when changes are detected within the textbox
    $scope.update = function () {
        shareSeoDataService.setDescription($scope.model.value, $scope.model.maxCount);
        // Calculate the remaining characters
        $scope.model.remainingCount = 0;
        if ($scope.model.value) {
            $scope.model.remainingCount = $scope.model.value.length;
            $scope.model.className = "number-counter-black";
            // Is our maximum limit reached?
            if ($scope.model.remainingCount <= 0) {
                $scope.model.remainingCount = 0;
                $scope.model.value = $scope.model.value.substr(0, $scope.model.maxCount)
                return;
            }

            // Set the correct CSS class

            if ($scope.model.remainingCount > $scope.model.maxCount) {
                $scope.model.className = "number-counter-red";
            }
        }
    }


    // Run the update method at start
    $scope.update();

    $scope.$watch("model.value", function (newValue, oldValue) {
        if (newValue != oldValue) {
            $scope.setDirty();
        }
    }, true);

    $scope.setDirty = function () {
        if (!$scope.isDirty) {
            $scope.isDirty = true;
            var currentForm = angularHelper.getCurrentForm($scope);
            currentForm.$setDirty();
        }
    }
    $scope.$on("formSubmitting", function (ev, args) {
        $scope.messageShow = '';
        if ($scope.model.remainingCount > $scope.model.maxCount) {
            $scope.messageShow = $scope.dictionary['[SEO] Warning Headline'] + ': ' + $scope.dictionary['[SEO] CharacterCount Warning Message'];
        }
        if (!$scope.model.value) {
            $scope.messageShow = $scope.dictionary['[SEO] Info Headline'] + ': ' +$scope.dictionary['[SEO] CharacterCount Info Message'] ;
        }
        if ($scope.messageShow) {
            shareSeoDataService.setMessage($scope.messageShow);
        }
        // Set lastest notifications msg at here ( checking order by Keyword > SERP > Title > Desc) 
        $scope.message = shareSeoDataService.getMessage();
        if ($scope.message) {
            notificationsService.warning($scope.dictionary['[SEO] Warning Headline'], $scope.dictionary['[SEO] CharacterCount Sumary Message']);
        }
    });
});