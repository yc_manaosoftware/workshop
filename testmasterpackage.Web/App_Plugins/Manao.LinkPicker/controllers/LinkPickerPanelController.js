﻿angular.module('umbraco').controller('ManaoLinkPickerPanelController', ['$scope', '$timeout', '$http', '$routeParams', '$element', 'angularHelper', 'dialogService',
    function ($scope, $timeout, $http, $routeParams, $element, angularHelper, dialogService) {

        $scope.isMultiple = false;
        $scope.values = [];

        $scope.init = function () {
            $scope.values = $scope.dialogData.values;
            $scope.models = angular.copy($scope.dialogData.model);
            $scope.nodes = angular.copy($scope.dialogData.model);
            $scope.syncValue($scope.nodes, $scope.values);
            $scope.config = $scope.dialogData.config;
            $scope.isMultiple = $scope.config.multiple === '1' ? true : false;
            $scope.error = {};
            $scope.initPreValue($scope.values);
            $scope.setPanelHeight(500);
        };

        $scope.initPreValue = function (value) {
            if (!$scope.isMultiple) {
                if (value.Target) {
                    $scope.target = value.Target;
                }
                if (!value.Id) {
                    $scope.external = value.Url;
                }
                if (value.Title) {
                    $scope.title = value.Title;
                }
                if (value.IsMedia || value.IsExternal) {
                    $scope.external = value.Url;
                }
            }
        };

        $scope.syncValue = function (nodes, values) {
            $scope.clearSelected(nodes);
            if (Array.isArray(values)) {
                $scope.tempValues = [];
                $scope.syncSelectedListArray(nodes, values);
            }
            else {
                $scope.syncSelectedNode(nodes, values);
            }
        };

        $scope.clearSelected = function (nodes) {
            angular.forEach(nodes, function (node) {
                if (node.childs.length) {
                    $scope.clearSelected(node.childs);
                }
                node.selected = false;
            });
        };

        $scope.syncSelectedListArray = function (nodes, values) {
            if (values.length) {
                angular.forEach(values, function (selectedNode) {
                    angular.forEach(nodes, function (node) {
                        if (node.childs.length) {
                            $scope.syncSelectedListArray(node.childs, values);
                        }
                        if (node.Id === selectedNode.Id) {
                            if (!node.selected) {
                                $scope.tempValues.push(node);
                            }
                            node.selected = true;
                        }
                    });
                });
                $scope.values = angular.copy($scope.tempValues);
            }
            else {
                values = [];
            }
        };

        $scope.syncSelectedNode = function (nodes, value) {
            if (value) {
                angular.forEach(nodes, function (node) {
                    if (node.Id === value.Id) {
                        node.selected = true;
                    }
                    else {
                        if (!$scope.isMultiple) {
                            node.selected = false;
                        }
                    }
                    if (node.childs.length) {
                        $scope.syncSelectedNode(node.childs, value);
                    }
                });
            }
        };

        $scope.selected = function (node) {
            if (node.isAllowed) {
                if (node.selected) {
                    node.selected = false;
                    if ($scope.isMultiple) {
                        var index = 0;
                        var keepIndex = -1;
                        angular.forEach($scope.values, function (selectedNode) {
                            if (selectedNode.Id === node.Id) {
                                keepIndex = index;
                            }
                            index++;
                        });
                        if (keepIndex > -1) {
                            $scope.values.splice(keepIndex, 1);
                        }
                    }
                    else {
                        $scope.values = {};
                    }
                }
                else {
                    node.selected = true;
                    if ($scope.isMultiple) {
                        if (!$scope.values) {
                            $scope.values = [];
                        }
                        $scope.values.push(node);
                    }
                    else {
                        $scope.values = node;
                        $scope.syncSelectedNode($scope.nodes, $scope.values);
                    }
                }
                if ($scope.isMultiple) {
                    $scope.validateMaxMin();
                }
            }
        };

        $scope.done = function () {
            if ($scope.isMultiple) {
                if ($scope.validateMaxMin()) {
                    $scope.submit($scope.values);
                }
            }
            else {
                if (!$scope.values) {
                    $scope.values = {};
                }
                $scope.values.Target = $scope.target;
                $scope.values.Title = $scope.title;
                delete $scope.values.childs;
                if ($scope.external) {
                    if ($scope.external.substring(0, 7).toLowerCase() === '/media/') {
                        $scope.values.IsExternal = false;
                        $scope.values.IsMedia = true;
                        $scope.values.IsInternal = false;
                    }
                    else {
                        delete $scope.values.Id;
                        delete $scope.values.Guid;
                        $scope.values.IsExternal = true;
                        $scope.values.IsMedia = false;
                        $scope.values.IsInternal = false;
                    }
                    $scope.values.Url = $scope.external;
                    $scope.values.Name = $scope.title ? $scope.title : $scope.external;
                    $scope.values.Icon = 'icon-link';
                }
                else {
                    $scope.values.IsExternal = false;
                    $scope.values.IsMedia = false;
                    $scope.values.IsInternal = true;
                }
            }
            if ($scope.values.Url) {
                $scope.submit($scope.values);
            }
            else {
                $scope.error.noLink = true;
            }
        };

        $scope.search = function () {
            $scope.models = angular.copy($scope.dialogData.model);
            if ($scope.keyword) {
                $scope.nodes = $scope.filter($scope.models);
                $scope.syncValue($scope.nodes, $scope.values);
            }
            else {
                $scope.nodes = angular.copy($scope.dialogData.model);
                $scope.syncValue($scope.nodes, $scope.values);
            }
        };

        $scope.filter = function (models) {
            var results = [];
            angular.forEach(models, function (node) {
                var tempChilds = angular.copy(node.childs);
                node.childs = $scope.filter(tempChilds);
                if (node.Name.toLowerCase().indexOf($scope.keyword.toLowerCase()) > -1) {
                    results.push(node);
                    node.isMatch = true;
                    node.expand = true;
                }
                else if (node.childs.length) {
                    node.expand = true;
                    results.push(node);
                }
            });
            return results;
        };

        $scope.setPanelHeight = function (timing) {
            $timeout(function () {
                $('.node-pickers').height($(window).height() - 215);
            }, timing);
        };

        $scope.validateMaxMin = function () {
            if (($scope.values.length <= $scope.config.max) || $scope.config.max === 0) {
                $scope.error.max = false;
            }
            else {
                $scope.error.max = true;
            }
            if (($scope.values.length >= $scope.config.min) || $scope.config.min === 0) {
                $scope.error.min = false;
            }
            else {
                $scope.error.min = true;
            }
            return (($scope.values.length <= $scope.config.max) || $scope.config.max === 0) &&
                (($scope.values.length >= $scope.config.min) || $scope.config.min === 0);
        };

        $scope.openMedia = function () {
            dialogService.mediaPicker({
                startNodeId: -1,
                multiPicker: false,
                callback: function (data) {
                    $scope.external = data.image ? data.image : '';
                    $scope.values.IsMedia = true;
                    $scope.title = data.name;
                    if (!$scope.isMultiple) {
                        $scope.values.Id = data.id;
                        $scope.values.Guid = data.key;
                    }
                }
            });
        };

        $scope.removeMedia = function () {
            $scope.title = '';
            $scope.external = '';
            $scope.values.IsMedia = false;
            $scope.values.Url = '';
        };

        $scope.adjustNodePosition = function (e) {
            e = angular.element(e.target);
            var list = e.closest('li');
            var level = e.parents('ul').length;
            $timeout(function () {
                var padLeft = (level * 10) / (level / 2) + 'px';
                list.find('li').css('padding-left', padLeft);
            }, 1);
        };

        $(window).resize(function () {
            $scope.setPanelHeight(0);
        });

        $scope.init();

    }]);
