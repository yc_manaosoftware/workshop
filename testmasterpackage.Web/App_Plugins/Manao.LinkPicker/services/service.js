﻿angular.module('umbraco').factory('ManaoLinkPickerService', ['$http',
    function ($http) {
        return {
            getList: function (currentNodeId, allowOnlyRoot) {
                return $http.get('/umbraco/backoffice/api/ManaoLinkApi/GetNodeTree?currentNodeId=' + currentNodeId + '&allowOnlyRoot=' + allowOnlyRoot);
            },
            getNodeByModel: function (data) {
                return $http.post('/umbraco/backoffice/api/ManaoLinkApi/GetNodeByModel', data);
            },
            getMediaByModel: function (data) {
                return $http.post('/umbraco/backoffice/api/ManaoLinkApi/GetMediaByModel', data);
            }
        };
    }
]);
