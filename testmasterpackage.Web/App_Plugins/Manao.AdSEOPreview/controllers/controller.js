﻿angular.module("umbraco").controller("advancedSeoPreviewController", ['$scope', '$element', 'dialogService',  '$timeout', '$rootScope', '$compile', 'dictionaryResource',  'angularHelper',  'shareSeoDataService', 'notificationsService',
    function ($scope, $element, dialogService, $timeout, $rootScope, $compile, dictionaryResource, angularHelper, shareSeoDataService, notificationsService) {

    $scope.loadText = function () {

        dictionaryResource.getText("[SEO] Plugins").then(function (response) {
            $scope.dictionary = response.data;
        });
    }
    $scope.loadText();

    $scope.$on('handlePreview', function () {
        //$timeout(function () {
        $scope.title = shareSeoDataService.getTitle();
        $scope.titleMaxCount = shareSeoDataService.getTitleMaxCount();
        $scope.description = shareSeoDataService.getDescription();
        $scope.descriptionMaxCount = shareSeoDataService.getDescriptionMaxCount();
        $($element).find(".titlePreview,.snippetPreview").width($scope.titleMaxCount);
        // }, 1000);

    });

    $scope.GetUrl = function () {
        $scope.url = '';
        var objectParentContent = $scope.GetParentContent();
        if (objectParentContent) {
            $scope.url = objectParentContent.urls[0];
        }
        return $scope.ProtocolAndHost() + $scope.url ; //dun no y?
    };

    $scope.ProtocolAndHost = function () {
        var http = location.protocol;
        var slashes = http.concat("//");
        return slashes.concat(window.location.hostname);
    };

    $scope.GetParentContent = function () {
        var currentScope = $scope.$parent;
        if (currentScope) {
            for (var i = 0; i < 150; i++) {
                if (currentScope) {
                    if (currentScope.content) { return currentScope.content; }
                    currentScope = currentScope.$parent;
                }
            }
        }
    };
}]);