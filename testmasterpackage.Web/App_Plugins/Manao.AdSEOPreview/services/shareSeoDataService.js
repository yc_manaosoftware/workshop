﻿angular.module("umbraco").service('shareSeoDataService', function ($rootScope, $timeout) {

    var title = '';
    var titleMaxCount = '';
    var description = '';
    var descriptionMaxCount = '';
    var keywords = [];
    var matchList = [];
    var spacialchar = [".", ",", ":", ";"];
    var arrtitle = [];
    var arrdes = [];
    var filterword = '';
    var message = '';

    var settitletoarray = function () {
        if (title) {
            arrtitle = title.split(' ');
            arrtitle = arrtitle.reduce(function (previousValue, currentValue, index, array) {
                currentValue = filterSpchar(currentValue);
                previousValue.push(currentValue)
                return previousValue
            }, []);
        }
    }

    var setdestoarray = function () {
        if (description) {
            arrdes = description.split(' ');
            arrdes = arrdes.reduce(function (previousValue, currentValue, index, array) {
                currentValue = filterSpchar(currentValue);
                previousValue.push(currentValue)
                return previousValue
            }, []);
        }
    }
    var checkHasFoundKeyword = function (txtkeyword) {
        var objhasfound = { title: false, desc: false };
        if (title) {

            objhasfound.title = searchWord(arrtitle, txtkeyword);
        }
        if (description) {

            objhasfound.desc = searchWord(arrdes, txtkeyword);
        }
        return objhasfound;
    };
    var filterSpchar = function (strword) {
        filterword = strword;
        var hasSpecialchar = false;
        var lastChar = strword.substr(strword.length - 1);
        spacialchar.forEach(function (spchar) {
            if (lastChar == spchar) {
                filterword = filterword.replace(spchar, '');
                filterSpchar(filterword);
            }
        });
        return filterword;
    };
    var searchWord = function (arrstr, txtkeyword) {
        if (arrstr.length > 0) {
            var hasfound = false;
            var i = 0;
            arrstr.forEach(function (word) {
                if (word.toLowerCase() == txtkeyword.toLowerCase()) {
                    hasfound = true;
                }
            });
        }
        return hasfound;
    };

    var setDescription = function (strdescription, intMaxCount) {
        description = strdescription;
        setdestoarray();
        descriptionMaxCount = intMaxCount;
        publishDescription();
        publishPreview();
    };
    var setTitle = function (strtitle, intMaxCount) {
        title = strtitle;
        settitletoarray();
        titleMaxCount = intMaxCount;
        publishtittle();
        publishPreview();
    };
    var setKeywords = function (keyword) {
        keywords.push(keyword);
        //checkKeywords(keyword);
        publishKeywords();
        //publishPreview();      
    };
    var setMessage = function (text) {
        //console.log('setMessage ', text);
        message = text;
    };
    var removeKeywords = function (index) {
        keywords.splice(index, 1);
        publishKeywords();
    };
    var getDescription = function () {
        return description;
    };
    var getKeywords = function () {
        return keywords;
    };
    var getDescriptionMaxCount = function () {
        return descriptionMaxCount;
    };
    var getMessage = function () {
        return message;
    };
    var getTitleMaxCount = function () {
        return titleMaxCount;
    };
    var getTitle = function () {
        return title;
    };

    var publishtittle = function () {
        $rootScope.$broadcast('handleTitle');
    };
    var publishDescription = function () {
        $rootScope.$broadcast('handleDescription');
    };
    var publishKeywords = function () {
        $rootScope.$broadcast('handleKeywords');
    };
    var publishPreview = function () {
        $rootScope.$broadcast('handlePreview');
    };

    return {
        getDescription: getDescription,
        getDescriptionMaxCount: getDescriptionMaxCount,
        getTitleMaxCount: getTitleMaxCount,
        getTitle: getTitle,
        getKeywords: getKeywords,
        getMessage: getMessage,
        checkHasFoundKeyword: checkHasFoundKeyword,
        removeKeywords: removeKeywords,
        setMessage: setMessage,
        setTitle: setTitle,
        setDescription: setDescription,
        setKeywords: setKeywords

    };

});