﻿angular.module("umbraco").controller("ManaoBootstrapTableController", ['$scope', 'dialogService', function ($scope, dialogService) {
    var doing = '';
    var index = 0;
    $scope.dictionaries = {};
    $scope.model.hideLabel = false;

    function init() {
        setMainModel();
        setSubModel();
    }

    function setMainModel() {
        if (!$scope.model.value) {
            $scope.model.value = [];
        }
    }

    function setSubModel() {
        $scope.submodel = {};
        _.each($scope.model.config.textField, function (text) {
            $scope.submodel[text.value] = '';
        });
        $scope.submodelKey = Object.keys($scope.submodel);
    }

    $scope.create = function () {
        doing = 'Create';
        openDialog(doing, null, $scope.submodel);
    };

    $scope.update = function (index) {
        doing = 'Update';
        index = index;
        openDialog(doing, index, $scope.model.value[index]);
    };

    $scope.delete = function (index) {
        var isConfirm = confirm('Are you sure?');
        if (isConfirm) {
            $scope.model.value.splice(index, 1);
        }
    };

    var openDialog = function (title, index, object) {
        var temp = {};
        angular.copy(object, temp);
        dialogService.open({
            template: "/App_Plugins/Manao.BootstrapTable/dialog.html",
            dialogData: {
                title: title,
                index: index,
                label: $scope.model.config.labelField,
                model: $scope.model.value,
                value: temp,
                debug: $scope.model.config.debugger
            },
            callback: function (object) {
                if ($scope.model.config.debugger) {
                    console.log('callback value : ');
                    console.log(object);
                }
                switch (doing) {
                    case 'Create':
                        $scope.model.value.push(object);
                        break;
                    case 'Update':
                        $scope.model.value[index] = object;
                        break;
                }
            }
        });
    }

    init();

}]);

angular.module("umbraco").controller("ManaoBootstrapTableDialogController", function ($scope) {
    $scope.title = $scope.dialogData.title;
    $scope.label = $scope.dialogData.label;
    $scope.model = $scope.dialogData.value;
    $scope.modelKey = Object.keys($scope.model);
    var debug = $scope.dialogData.debug;
    if (debug) {
        console.log('dialog data : ')
        console.log($scope.dialogData);
    }
    
    $scope.readkey = function () {
        if (debug) {
            console.log('type value : ');
            console.log($scope.model);
        }
    }

    $scope.save = function () {
        $scope.submit($scope.model);
    };

});
