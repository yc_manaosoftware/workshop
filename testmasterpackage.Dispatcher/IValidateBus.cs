﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testmasterpackage.Core.ValidationHandler.Base;
using testmasterpackage.Core.DomainModel.Base;

namespace testmasterpackage.Dispatcher
{
    public interface IValidateBus
    {
        ValidationResult Validate<TDomainModel>(TDomainModel domainModel) where TDomainModel : IDomainModel;
    }
}
