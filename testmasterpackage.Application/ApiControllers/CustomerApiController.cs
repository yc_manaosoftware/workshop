﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Umbraco.Web.WebApi;

namespace testmasterpackage.Application.ApiControllers
{
    public class CustomerApiController : UmbracoApiController
    {

        // Call by Umbraco/Api/CustomerApi/GetCustomers
        public object GetCustomers()
        {
            var culture = CultureInfo.CurrentCulture;
            var aa = 12;
            int a = (int)aa;
            var x = new[]{
                new
                {
                    Name = "Customer1 Name1", Id = "001"
                },
                new
                {
                    Name = "Customer2 Name2", Id = "002"
                }
            };
            
            //JavaScriptSerializer js = new JavaScriptSerializer();
            return x;
        }
    }
}
