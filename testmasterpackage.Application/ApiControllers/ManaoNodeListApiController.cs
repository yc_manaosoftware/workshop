﻿using System.Collections.Generic;
using Umbraco.Web.Editors;
using testmasterpackage.Core.DomainModel.Tree;
using testmasterpackage.Core.Services.TreeService;
using testmasterpackage.Core.Services.UmbracoServices;
using testmasterpackage.Core.DomainModel.Editors; 
using System;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using testmasterpackage.Core.Services.SerializerServices;

namespace testmasterpackage.Application.Controllers.ApiControllers
{
    public class ManaoNodeListApiController : UmbracoAuthorizedJsonController
    {
        private List<NodeTreeModel> nodes = new List<NodeTreeModel>();
        private ITreeService treeService;
        private INodeModelSerializerService nodeModelSerializerService;

        public ManaoNodeListApiController(ITreeService treeService,INodeModelSerializerService _nodeModelSerializerService) {
            this.treeService = treeService;
            nodes = new List<NodeTreeModel>();
            nodeModelSerializerService = _nodeModelSerializerService;
        }

        public List<NodeTreeModel> GetByAlias(string parents, string childs,Guid currentNodeId,int allowOnlyRoot)
        {
            if (isValid(parents, childs))
            {
                bool isAllowOnlyRoot = false;
                if(allowOnlyRoot == 1)
                {
                    isAllowOnlyRoot = true;
                }
                string [] parentAliases = parents.Split(',');
                string [] childAliases = childs.Split(',');
                nodes = treeService.GetTreesByParentAndChildAliases(parentAliases, childAliases, currentNodeId, isAllowOnlyRoot);
            }
            return nodes;
        }

        private bool isValid(string parents, string childs)
        {
            if(string.IsNullOrEmpty(parents) || string.IsNullOrEmpty(childs))
            {
                return false;
            }
            return true;
        }

        [HttpPost]
        public JObject GetNodeByModel([FromBody] JObject model)
        {
            JObject updatedModel = nodeModelSerializerService.GetUpdateJsonModel(model);

            return updatedModel;
        }

        [HttpPost]
        public List<JObject> GetNodeByModels([FromBody] List<JObject> models)
        {
            List<JObject> updatedValue = nodeModelSerializerService.GetUpdateJsonModels(models);

            return updatedValue;
        }
    }
}
