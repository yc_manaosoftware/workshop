﻿using System.Collections.Generic;
using System.Linq;
using Umbraco.Web.Editors;
using Umbraco.Web;
using Umbraco.Core.Models;
using System.Collections;
using testmasterpackage.Core.Services.TreeService;
using testmasterpackage.Infrastructure.UmbracoServices;
using testmasterpackage.Core.DomainModel.Tree;
using testmasterpackage.Core.DomainModel.Editors;
using System;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using testmasterpackage.Core.Services.SerializerServices;

namespace testmasterpackage.Application.Controllers.ApiControllers
{
    public class ManaoLinkApiController : UmbracoAuthorizedJsonController
    {
        private ITreeService treeService;
        private ILinkModelSerializerService linkModelSerializerService;
        private INodeModelSerializerService nodeModelSerializerService;
        private List<NodeTreeModel> nodes = new List<NodeTreeModel>();

        public ManaoLinkApiController
            (
                ITreeService treeService, 
                ILinkModelSerializerService _linkModelSerializerService,
                INodeModelSerializerService _nodeModelSerializerService
            )
        {
            this.treeService = treeService;
            linkModelSerializerService = _linkModelSerializerService;
            nodeModelSerializerService = _nodeModelSerializerService;
        }

        public List<NodeTreeModel> GetNodeTree(Guid currentNodeId, int allowOnlyRoot)
        {
            if (allowOnlyRoot == 1)
            {
                this.nodes = treeService.GetTrees(currentNodeId, true);
            }
            else
            {
                this.nodes = treeService.GetTrees();
            }
            return nodes;
        }

        [HttpPost]
        public JObject GetNodeByModel([FromBody] JObject model)
        {
            JObject updatedModel = nodeModelSerializerService.GetUpdateJsonModel(model);

            return updatedModel;
        }

        [HttpPost]
        public JObject GetMediaByModel([FromBody] JObject model)
        {
            JObject updatedModel = linkModelSerializerService.GetUpdateJsonModel(model);

            return updatedModel;
        }
    }
}
