﻿using testmasterpackage.Core.DomainModel.Media;
using testmasterpackage.Core.Services.UmbracoServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Web;
using Umbraco.Web.WebApi;

namespace testmasterpackage.Application.ApiControllers
{
    public class ManaoMediaApiController : UmbracoApiController
    {
        private UmbracoHelper umbracoHelper;
        private IManaoMediaService mediaService;
        public ManaoMediaApiController(UmbracoHelper umbracoHelper, IManaoMediaService mediaService)
        {
            this.umbracoHelper = umbracoHelper;
            this.mediaService = mediaService;
        }
        public ManaoImage GetImage (Guid guid)
        {
            var image = mediaService.GetImageByGuid(guid);
            return image;
        }
    }
}
