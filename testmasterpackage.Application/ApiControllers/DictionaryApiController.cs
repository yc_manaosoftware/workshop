﻿using System.Collections.Generic;
using Umbraco.Web.WebApi;
using Umbraco.Web.Dictionary;
using System.Globalization;
using Umbraco.Core.Services;
using Umbraco.Core;
using testmasterpackage.Core.Services.UmbracoServices;
using testmasterpackage.Infrastructure.UmbracoServices;

namespace testmasterpackage.Application.ApiControllers
{
    public class DictionaryApiController : UmbracoApiController
    {
        private IDictionaryService dictionaryService;
        public DictionaryApiController(IDictionaryService dictionaryService)
        {
            this.dictionaryService = dictionaryService;
        }
        public IDictionary<string, string> GetDictionary(string locale, string key)
        {
            //Get all dictionary from root key by specific language.
            return dictionaryService.GetDictionary(locale, key);
        }
        public IDictionary<string, string> GetDictionary(string key)
        {
            //Get all dictionary from root key with no specific language.
            return dictionaryService.GetDictionary(key);
        }
        public string GetDictionaryItem(string locale, string key)
        {
            //Get single dictionary by specific language.
            return dictionaryService.GetDictionaryItem(CultureInfo.GetCultureInfo(locale), key);
        }
    }
}
