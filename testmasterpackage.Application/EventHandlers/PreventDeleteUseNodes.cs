﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;
using testmasterpackage.Core.DomainModel;

namespace d8SK.EventHandlers
{
    public class PreventDeleteUsedNodes : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            ContentService.Trashing += NodeInUseCheck;
        }

        void NodeInUseCheck(IContentService sender, MoveEventArgs<IContent> e)
        {
            var database = ApplicationContext.Current.DatabaseContext.Database;

            List<cmsPropertyData> propertyIds =
                database.Fetch<cmsPropertyData>("select * from cmsPropertyData order by id desc");

            List<string> nodeNames = new List<string>();
            Dictionary<int, string> nodeIdChecked = new Dictionary<int, string>();
            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            var firstOrDefault = e.MoveInfoCollection.FirstOrDefault();
            foreach (var data in propertyIds)
            {
                if (!nodeIdChecked.ContainsKey(data.contentNodeId))
                {
                    nodeIdChecked.Add(data.contentNodeId, data.versionId.ToString());
                }
                if (nodeIdChecked[data.contentNodeId] == data.versionId.ToString())
                {
                    if (data.dataNvarchar != null && data.dataNvarchar.Contains(firstOrDefault.Entity.Id.ToString()) ||
                        data.dataInt == firstOrDefault.Entity.Id || data.dataNtext != null &&
                        data.dataNtext.Contains(firstOrDefault.Entity.Id.ToString()))
                    {
                        var node = umbracoHelper.TypedContent(data.contentNodeId);
                        if (node != null)
                        {
                            nodeNames.Add(node.Name);
                        }
                    }
                }
            }

            if (nodeNames.Count > 0)
            {
                e.Cancel = true;
                e.Messages.Add(new EventMessage("Warning ",
                    "Node is used within  " + NodeList(nodeNames) +
                    "  please remove the instances first.", EventMessageType.Warning));
            }
        }

        private string NodeList(List<string> nodeNames)
        {
            return nodeNames.Aggregate("", (current, nodeName) => current + nodeName + ", ");
        }
    }

}
