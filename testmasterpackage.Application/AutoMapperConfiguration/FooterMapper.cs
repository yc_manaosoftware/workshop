﻿using AutoMapper;
using testmasterpackage.Application.AutoMapperConfiguration.Base;
using testmasterpackage.Core.DomainModel.Footer;
using testmasterpackage.Core.Services.UmbracoServices;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace testmasterpackage.Application.AutoMapperConfiguration
{
    public class FooterMapper : IMapperBase
    {
        public void Configure(IContainer container)
        {
            var configurationService = container.GetInstance<IConfigurationService>();
            Mapper.CreateMap<IPublishedContent, Footer>()
                .ConvertUsing(source => configurationService.GetFooterContent(source.GetKey()))
                ;
        }
    }
}
