﻿using AutoMapper;
using testmasterpackage.Application.AutoMapperConfiguration.Base;
using testmasterpackage.Application.Models;
using testmasterpackage.Core.DomainModel.Editors;
using testmasterpackage.Core.Services.SerializerServices;
using Newtonsoft.Json.Linq;
using StructureMap;
using System.Collections.Generic;
using Umbraco.Core.Models;
using Umbraco.Web;
namespace testmasterpackage.Application.AutoMapperConfiguration
{
    public class SubpageMapper : IMapperBase
    {
        public void Configure(IContainer container)
        {
            ILinkModelSerializerService linkModelSerializer = container.GetInstance<ILinkModelSerializerService>();
            INodeModelSerializerService nodeModelSerializer = container.GetInstance<INodeModelSerializerService>();

            Mapper.CreateMap<IPublishedContent, SubpageViewModel>()
               .ForMember(dest => dest.Link, opt => { opt.MapFrom(source => linkModelSerializer.GetDeserializeLinkModel(source.GetPropertyValue<string>("link"))); })
               .ForMember(dest => dest.Node, opt => { opt.MapFrom(source => nodeModelSerializer.GetDeserializeNodeModel(source.GetPropertyValue<string>("nodePicker"))); })
               .ForMember(dest => dest.NodeList, opt => { opt.MapFrom(source => nodeModelSerializer.GetDeserializeNodeModels(source.GetPropertyValue<string>("nodeListPicker"))); })
               .ForMember(dest => dest.Id, opt => opt.Ignore())
               .ForMember(dest => dest.CurrentCulture, opt => opt.Ignore())
               ;
        }
    }
}
