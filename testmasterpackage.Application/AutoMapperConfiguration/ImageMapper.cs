﻿using AutoMapper;
using Lecoati.LeBlender.Extension.Models;
using testmasterpackage.Application.AutoMapperConfiguration.Base;
using testmasterpackage.Core.DomainModel.Image;
using testmasterpackage.Core.DomainModel.Media;
using testmasterpackage.Core.Services.UmbracoServices;
using StructureMap;
using Umbraco.Core.Models;
using testmasterpackage.Application.Helpers;
using System.Linq;

namespace testmasterpackage.Application.AutoMapperConfiguration
{
    public class ImageMapper : IMapperBase
    {
        public void Configure(IContainer container)
        {
            var mediaService = container.GetInstance<IManaoMediaService>();
            Mapper.CreateMap<IMedia, ManaoImage>()
                .ForMember(dest => dest.Guid, opt => { opt.MapFrom(source => source.Key); })
                .ForMember(dest => dest.Url, opt => { opt.MapFrom(source => mediaService.GetMediaUrl(source.Id)); })
                .ForMember(dest => dest.Title, opt => { opt.MapFrom(source => source.Name); })
                .ForMember(dest => dest.Alt, opt => { opt.MapFrom(source => mediaService.GetMediaAlt(source.Name)); })
                ;

            Mapper.CreateMap<LeBlenderModel, ImageOrDefault>()
               .ForMember(dest => dest.Image, opt => { opt.MapFrom(source => ImageHelper.GetImageUrlOrDefault(source, "image")); })
               .ForMember(dest => dest.Link, opt => { opt.MapFrom(source => LinkHelper.AsUrl(source, "link")); })
               ;
        }
    }
}


