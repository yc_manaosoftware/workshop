﻿using AutoMapper;
using testmasterpackage.Application.AutoMapperConfiguration.Base;
using testmasterpackage.Core.DomainModel.SEO;
using testmasterpackage.Core.Services.UmbracoServices;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace testmasterpackage.Application.AutoMapperConfiguration
{
     public class SEOMapper : IMapperBase
    {
        public void Configure(IContainer container)
        {
            ISEOService service = container.GetInstance<ISEOService>();

            Mapper.CreateMap<IPublishedContent, SEO>()
                .ConvertUsing(source => service.GetSeoContent(source.GetKey()));
            ;
        }
    }
}
