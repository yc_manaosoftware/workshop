﻿using AutoMapper;
using testmasterpackage.Application.AutoMapperConfiguration.Base;
using testmasterpackage.Application.Models;
using testmasterpackage.Core.DomainModel;
using Lecoati.LeBlender.Extension.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Web;
using StructureMap;

namespace testmasterpackage.Application.AutoMapperConfiguration
{
    public class DefaultMapper : IMapperBase
    {
        public void Configure(IContainer container)
        {
            Mapper.CreateMap<IContent, News>()
                .ForMember(dest => dest.Title, opt => { opt.MapFrom(source => source.GetValue<string>("title")); })
                .ForMember(dest => dest.Details, opt => { opt.MapFrom(source => source.GetValue<string>("details")); })
                ;

            Mapper.CreateMap<IPublishedContent, News>()
                .ForMember(dest => dest.Title, opt => { opt.MapFrom(source => source.GetPropertyValue<string>("title")); })
                .ForMember(dest => dest.Details, opt => { opt.MapFrom(source => source.GetPropertyValue<string>("details")); })
                ;


            Mapper.CreateMap<IPublishedContent, SearchResultViewModel>()
                .ForMember(dest => dest.Title, opt => { opt.MapFrom(source => source.HasValue("menuTitle") ? source.GetPropertyValue("menuTitle").ToString() : source.Name); })
                .ForMember(dest => dest.Content, opt => { opt.MapFrom(source => source); })
                ;
        }
    }
}
