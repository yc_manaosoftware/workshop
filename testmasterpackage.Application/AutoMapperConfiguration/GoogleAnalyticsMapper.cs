﻿using AutoMapper;
using testmasterpackage.Application.AutoMapperConfiguration.Base;
using testmasterpackage.Core.DomainModel.GoogleAnalytics;
using testmasterpackage.Core.Services.UmbracoServices;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace testmasterpackage.Application.AutoMapperConfiguration
{
    public class GoogleAnalyticsMapper : IMapperBase
    {
        public void Configure(IContainer container)
        {
            IGoogleAnalyticsService service = container.GetInstance<IGoogleAnalyticsService>();

            Mapper.CreateMap<IPublishedContent, GoogleAnalytics>()
                .ConvertUsing(source => service.GetGoogleAnalytics(source.GetKey()));
            ;
        }
    }
}
