﻿using AutoMapper;
using testmasterpackage.Application.AutoMapperConfiguration.Base;
using testmasterpackage.Core.DomainModel.Header;
using testmasterpackage.Core.Services.UmbracoServices;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace testmasterpackage.Application.AutoMapperConfiguration
{
    public class HeaderMapper : IMapperBase
    {
        public void Configure(IContainer container)
        {
            var configurationService = container.GetInstance<IConfigurationService>();
            Mapper.CreateMap<IPublishedContent, Favicon>()
                .ForMember(dest => dest.Src, opt => { opt.MapFrom(source => configurationService.GetFavoriteIcon(source.GetKey())); }) 
                ;

            Mapper.CreateMap<IPublishedContent, Header>()
                .ConvertUsing(source => configurationService.GetHeaderContent(source.GetKey()))
                ;
        }
    }
}
