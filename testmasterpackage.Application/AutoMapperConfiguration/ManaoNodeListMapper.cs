﻿using AutoMapper;
using testmasterpackage.Application.AutoMapperConfiguration.Base;
using testmasterpackage.Core.DomainModel.Editors;
using testmasterpackage.Core.Services.UmbracoServices;
using testmasterpackage.Infrastructure.UmbracoServices;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace testmasterpackage.Application.AutoMapperConfiguration
{
    public class ManaoNodeListMapper : IMapperBase
    {
        public void Configure(IContainer container)
        {
            UmbracoService umbracoSerivce = container.GetInstance<UmbracoService>();

            Mapper.CreateMap<Guid, NodeModel>()
               .ForMember(dest => dest.Id, opt => { opt.MapFrom(source => umbracoSerivce.GetContentByGuid(source).Id); })
               .ForMember(dest => dest.DocumentTypeId, opt => { opt.MapFrom(source => umbracoSerivce.GetContentByGuid(source).DocumentTypeId); })
               .ForMember(dest => dest.Name, opt => { opt.MapFrom(source => umbracoSerivce.GetContentByGuid(source).Name); })
               .ForMember(dest => dest.Url, opt => { opt.MapFrom(source => umbracoSerivce.GetContentByGuid(source).Url); })
               .ForMember(dest => dest.Guid, opt => { opt.MapFrom(source => source); })
               ;
        }
    }
}
