﻿using testmasterpackage.Application.AutoMapperConfiguration.Base;
using StructureMap;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using testmasterpackage.Core.DomainModel.CookiesAlert;
using testmasterpackage.Core.Services.UmbracoServices;
using Umbraco.Web;

namespace testmasterpackage.Application.AutoMapperConfiguration
{
    public class CookiesAlertMapper : IMapperBase
    {
        public void Configure(IContainer container)
        {
            ICookiesAlertService service = container.GetInstance<ICookiesAlertService>();

            Mapper.CreateMap<IPublishedContent, CookiesAlert>()
                .ConvertUsing(source => service.GetCookiesAlert(source.GetKey()));
            ;
        }
    }
}
