﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Application.Alias
{
    public class PropertyAlias
    {
        public static readonly string GridContent = "content";
        public static readonly string GridArticleContent = "articleContent";
        public static readonly string NodeTypeAlias = "nodeTypeAlias";
    }
}
