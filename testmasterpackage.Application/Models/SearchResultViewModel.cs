﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace testmasterpackage.Application.Models
{
    public class SearchResultViewModel
    {
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Url { get; set; }
        public IPublishedContent Content { get; set; }
        public IEnumerable<Tag> Tags { get; set; }
    }
}
