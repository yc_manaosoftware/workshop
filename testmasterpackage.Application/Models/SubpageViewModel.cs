﻿using testmasterpackage.Core.DomainModel.Editors;
using testmasterpackage.Core.DomainModel.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Application.Models
{
    public class SubpageViewModel: DomainModel
    {
        public LinkModel Link { get; set; }
        public NodeModel Node { get; set; }
        public List<NodeModel> NodeList { get; set; }
    }
}
