﻿using testmasterpackage.Application.AutoMapperConfiguration.Base;
using testmasterpackage.Core.DomainModel.Base;
using testmasterpackage.Core.Repositories.Base;
using testmasterpackage.Core.Services.SearchServices;
using testmasterpackage.Core.Services.SerializerServices;
using testmasterpackage.Core.Services.TreeService;
using testmasterpackage.Core.Services.UmbracoServices;
using testmasterpackage.Core.ValidationHandler.Base;
using testmasterpackage.Dispatcher;
using testmasterpackage.Infrastructure.DAL.EF;
using testmasterpackage.Infrastructure.SerializerServices;
using testmasterpackage.Infrastructure.Services;
using testmasterpackage.Infrastructure.UmbracoServices;
using testmasterpackage.Services.Interfaces;
using StructureMap;
using StructureMap.Web.Pipeline;
using Umbraco.Core;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Trees;

namespace testmasterpackage.Application.IoC
{
    public class StructureMapContainerInit
    {
        private static IContainer container;
        public static IContainer InitializeContainer()
        {
            if(container == null)
            {
                container = new Container(c => c.AddRegistry<DefaultRegistry>());

                //This function below is for load registries from DLL which implement with Structure Map IOC container
                //For use with Manao Packages that implemented with new CMS Template
                string dllPath = System.Web.HttpContext.Current.Server.MapPath("/bin");
                container.Configure(c => c.Scan(scan => {
                    scan.AssembliesFromPath(dllPath);
                    scan.LookForRegistries();
                }));
            }
            return container;
        }
    }

    internal class DefaultRegistry : Registry
    {
        #region Constructors and Destructors

        public DefaultRegistry()
        {
            Scan(
                scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                    scan.AssemblyContainingType<IDomainModel>();
                    scan.AddAllTypesOf<IDomainModel>();
                    scan.AddAllTypesOf<IMapperBase>();
                    scan.ConnectImplementationsToTypesClosing(typeof(IValidationHandler<>));
                });

            Policies.FillAllPropertiesOfType<IContainer>();

            For<IValidateBus>().Use<DefaultValidateBus>();

            // Unit Of Work
            For<EfUnitOfWork>().Use<EfUnitOfWork>().Named("UnitOfWorkObject").LifecycleIs<HttpContextLifecycle>();
            For<IUnitOfWork>().Use(x => x.GetInstance<EfUnitOfWork>("UnitOfWorkObject")).LifecycleIs<HttpContextLifecycle>();

            // Domain Services
            //For<ICustomerValidationService>().Use<CustomerValidationService>();

            //Umbraco Content Service
            For<UmbracoHelper>().Use(new UmbracoHelper(UmbracoContext.Current));
            For<Examine.ExamineManager>().Use(Examine.ExamineManager.Instance);
            For<UserTreeController>().Use(new UserTreeController(UmbracoContext.Current));
            For<ILocalizationService>().Use(ApplicationContext.Current.Services.LocalizationService);

            For<IContentService>().Use(ApplicationContext.Current.Services.ContentService);
            For<IContentTypeService>().Use(ApplicationContext.Current.Services.ContentTypeService);

            For<IMediaService>().Use(ApplicationContext.Current.Services.MediaService);

            For<UmbracoService>().Use<UmbracoService>();
            For<ISearchService>().Use<SearchService>();

            For<ITreeService>().Use<TreeService>();

            //For<IPositionRepository>().Use<PositionRepository>()/*.Named("PositionRepository")*/.LifecycleIs<HttpContextLifecycle>();
            //For<IPositionRepository>().Use(x => x.GetInstance<EfUnitOfWork>("UnitOfWorkObject").PositionRepository).LifecycleIs<HttpContextLifecycle>();
           
            //Manao Media service
            For<IManaoMediaService>().Use<ManaoMediaService>();

            //Cookies Alert Service
            For<ICookiesAlertService>().Use<CookiesAlertService>();

            //Google Analytics Service
            For<IGoogleAnalyticsService>().Use<GoogleAnalyticsService>();

            //SEO Service
            For<ISEOService>().Use<SEOService>();

            For<ISitemapService>().Use<SitemapService>();

            //Robots Service
            For<IRobotsService>().Use<RobotsService>();

            //Configuration Service
            For<IConfigurationService>().Use<ConfigurationService>();

            //Serializer Service
            For<INodeModelSerializerService>().Use<NodeModelSerializerService>();
            For<ILinkModelSerializerService>().Use<LinkModelSerializerService>();
        }

        #endregion
    }
}
