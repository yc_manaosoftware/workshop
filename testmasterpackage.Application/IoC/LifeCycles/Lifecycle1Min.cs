﻿using StructureMap.Pipeline;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StructureMap;

namespace testmasterpackage.Application.IoC.LifeCycles
{
    public class Lifecycle1Min : ILifecycle
    {
        protected ExpirationObjectCache cache;

        //Duration in minute
        private readonly int cacheDuration;

        public Lifecycle1Min()
        {
            this.cacheDuration = 1;
        }

        protected Lifecycle1Min(int cacheDuration)
        {
            this.cacheDuration = cacheDuration;
        }

        public string Description { get { return "Custom Cached Minute"; } }

        public void EjectAll(ILifecycleContext context)
        {
            FindCache(context).DisposeAndClear();
        }

        public IObjectCache FindCache(ILifecycleContext context)
        {
            if (cache == null)
                cache = new ExpirationObjectCache(cacheDuration);

            return cache;
        }
    }
}
