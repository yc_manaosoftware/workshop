﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Application.IoC.LifeCycles.Extensions
{
    public static class ObjectCacheExtension
    {
        public static void SafeDispose(this object target)
        {
            IDisposable disposable = target as IDisposable;
            if (disposable == null)
            {
                return;
            }
            try
            {
                disposable.Dispose();
            }
            catch (Exception exception)
            {
            }
        }
    }
}
