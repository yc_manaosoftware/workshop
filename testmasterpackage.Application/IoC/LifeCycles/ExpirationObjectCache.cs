﻿using AutoMapper;
using testmasterpackage.Application.IoC.LifeCycles.Extensions;
using StructureMap;
using StructureMap.Pipeline;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace testmasterpackage.Application.IoC.LifeCycles
{
    public class ExpirationObjectCache : IObjectCache
    {
        private readonly int cacheDuration;
        private readonly ReaderWriterLockSlim _lock;
        private readonly IDictionary<int, (object Result, DateTime TimeStored)> _objects = new Dictionary<int, (object Result, DateTime TimeStored)>();
        private readonly IList<Instance> _instances = new List<Instance>();

        public int Count { get { return _lock.Read(() => _objects.Count); } }

        public ExpirationObjectCache(int cacheDuration)
        {
            this.cacheDuration = cacheDuration;
            _lock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
        }

        public void DisposeAndClear()
        {
            _lock.Write(() => {
                _objects.Values.Each(@object => {
                    if (@object.Result is Container) return;

                    if (@object.Result != null) @object.SafeDispose();
                });

                _objects.Clear();
            });
        }

        public void Eject(Type pluginType, Instance instance)
        {
            // Prevent null reference exception.
            if (pluginType.AssemblyQualifiedName == null)
                return;

            var key = instance.InstanceKey(pluginType);
            _lock.MaybeWrite(() => {
                if (!_objects.ContainsKey(key)) return;

                _lock.Write(() => {
                    var disposable = _objects[key].Result as IDisposable;
                    _objects.Remove(key);
                    disposable.SafeDispose();
                });
            });
        }

        public object Get(Type pluginType, Instance instance, IBuildSession session)
        {
            object result;
            var key = instance.InstanceKey(pluginType);
            _lock.EnterUpgradeableReadLock();
            try
            {
                if (_objects.ContainsKey(key))
                {
                    (object Result, DateTime TimeStored) = _objects[key];

                    if ((DateTime.Now - TimeStored).TotalSeconds > (cacheDuration * 60))
                    {
                        DisposeAndClear();
                        result = Get(pluginType, instance, session);
                    }
                    else
                        result = Result;
                }
                else
                {
                    _lock.EnterWriteLock();
                    try
                    {
                        _instances.Add(instance);
                        result = buildWithSession(pluginType, instance, session);
                        _objects.Add(key, (Result: result, TimeStored: DateTime.Now));
                    }
                    finally
                    {
                        _instances.Remove(instance);
                        _lock.ExitWriteLock();
                    }
                }
            }
            finally
            {
                _lock.ExitUpgradeableReadLock();
            }

            return result;
        }

        protected virtual object buildWithSession(Type pluginType, Instance instance, IBuildSession session)
        {
            return session.BuildNewInOriginalContext(pluginType, instance);
        }

        public bool Has(Type pluginType, Instance instance)
        {
            return _lock.Read(() => {
                var key = instance.InstanceKey(pluginType);
                return _objects.ContainsKey(key);
            });
        }
    }
}
