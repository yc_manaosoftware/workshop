﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace testmasterpackage.Application.Controllers
{
    public class UploadContentController : SurfaceController
    {
        private static IEnumerable<EmbeddedResourceItem> EmbeddedResources { get; set; }

        public UploadContentController()
        {
            EmbeddedResources = GetEmbeddedResources();
        }

        public ActionResult Index()
        {
            string virtualPath = "Views/UploadContent.cshtml";
            var virtualFile = GetEmbeddedResourceFile(virtualPath);

            return File(virtualFile.Open(), EmbeddedResourceTypes.GetContentType(virtualPath));


            //return View();
        }

        private static EmbeddedResourceFile GetEmbeddedResourceFile(string virtualPath)
        {
            var manifestResourceStream = GetManifestResourceStream(virtualPath);
            return (manifestResourceStream == null) ? null : new EmbeddedResourceFile(virtualPath, manifestResourceStream);
        }

        private static Stream GetManifestResourceStream(string virtualPath)
        {
            var embeddedResource = FindEmbeddedResource(virtualPath);

            if (embeddedResource == null)
                return null;

            var assembly = GetAssemblies().FirstOrDefault(a => a.FullName.Equals(embeddedResource.AssemblyFullName, StringComparison.InvariantCultureIgnoreCase));

            return (assembly == null) ? null : assembly.GetManifestResourceStream(embeddedResource.Name);
        }

        private static EmbeddedResourceItem FindEmbeddedResource(string virtualPath)
        {
            var name = GetNameFromPath(virtualPath);
            var fileName = Path.GetFileName(virtualPath);

            return string.IsNullOrEmpty(name)
                ? null
                : EmbeddedResources.FirstOrDefault(r => r.Name.EndsWith(name, StringComparison.InvariantCultureIgnoreCase));
        }

        private static string GetNameFromPath(string virtualPath)
        {
            if (string.IsNullOrEmpty(virtualPath))
                return null;

            return virtualPath.Replace("~", string.Empty).Replace("/", ".");
        }

        private static IEnumerable<Assembly> GetAssemblies()
        {
            try
            {
                return AppDomain.CurrentDomain.GetAssemblies();
            }
            catch
            {
                return null;
            }
        }

        private static IEnumerable<EmbeddedResourceItem> GetEmbeddedResources()
        {
            var assemblies = GetAssemblies();

            if (assemblies == null || assemblies.Count() == 0)
                return null;

            var embeddedResources = new List<EmbeddedResourceItem>();

            foreach (var assembly in assemblies)
            {
                var names = GetNamesOfAssemblyResources(assembly);

                if (names == null || names.Length == 0)
                    continue;

                var validNames = from name in names
                                 let ext = Path.GetExtension(name)
                                 where EmbeddedResourceTypes.Contains(ext) || name.ToLowerInvariant().Contains(".views.")
                                 select name;

                foreach (var name in validNames)
                {
                    embeddedResources.Add(new EmbeddedResourceItem { Name = name, AssemblyFullName = assembly.FullName });
                }
            }

            return embeddedResources;
        }

        private static string[] GetNamesOfAssemblyResources(Assembly assembly)
        {
            // GetManifestResourceNames will throw a NotSupportedException when run on a dynamic assembly
            try
            {
                return assembly.GetManifestResourceNames();
            }
            catch
            {
                return new string[] { };
            }
        }
    }

    public class EmbeddedResourceFile : VirtualFile
    {
        private readonly Stream _manifestResourceStream;

        public EmbeddedResourceFile(string virtualPath, Stream manifestResourceStream) :
            base(virtualPath)
        {
            _manifestResourceStream = manifestResourceStream;
        }

        public override Stream Open()
        {
            return _manifestResourceStream;
        }
    }

    public static class EmbeddedResourceTypes
    {
        public static string GetContentType(string path)
        {
            return MimeTypes[Path.GetExtension(path)];
        }

        public static bool Contains(string extension)
        {
            return MimeTypes.ContainsKey(extension.ToLowerInvariant());
        }

        public static readonly Dictionary<string, string> MimeTypes = new Dictionary<string, string>
        {
            {".js", "text/javascript"},
            {".css", "text/css"},
            {".gif", "image/gif"},
            {".png", "image/png"},
            {".jpg", "image/jpeg"},
            {".xml", "application/xml"},
            {".txt", "text/plain"},
            {".html", "text/html"},
            { ".cshtml", "text/html"}
        };
    }

    public class EmbeddedResourceItem
    {
        public string Name { get; set; }
        public string AssemblyFullName { get; set; }
    }
}
