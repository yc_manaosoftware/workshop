﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Core.Models;
using Umbraco.Web.Mvc;
using testmasterpackage.Services.Interfaces;
using testmasterpackage.Services;
using testmasterpackage.Core.Alias;

namespace testmasterpackage.Controllers
{
    public class SitemapController : UmbracoController
    {
        private ISitemapService sitemapService;
        public SitemapController(ISitemapService sitemapService) : base(UmbracoContext.EnsureContext(new HttpContextWrapper(System.Web.HttpContext.Current)
                , ApplicationContext.Current
                , new global::Umbraco.Web.Security.WebSecurity(new HttpContextWrapper(System.Web.HttpContext.Current)
                , ApplicationContext.Current)
                , true))
        {
            this.sitemapService = sitemapService;
        }
        public ActionResult GetSitemap()
        {
            UmbracoHelper umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            IPublishedContent currentContent = null;
            IPublishedContent configNode = null;
            IPublishedContent sitemapxmlConfigNode = null;
            foreach (IPublishedContent content in umbracoHelper.ContentAtRoot())
            {
                if (content.Url().Equals("/"))
                {
                    currentContent = content;
                    break;
                }
            }
            configNode = currentContent != null ? currentContent.Children.Where(c => c.ContentType.Alias.Equals(PropertyAlias.Configuration)).FirstOrDefault() : null;
            sitemapxmlConfigNode = configNode != null ? configNode.Children.Where(c => c.ContentType.Alias.Equals(PropertyAlias.SitemapXml)).FirstOrDefault() : null;
            if (sitemapxmlConfigNode != null)
            {
                return Content(sitemapService.GetSitemapXML(currentContent.Id, System.Web.HttpContext.Current.Request.Url.Scheme, System.Web.HttpContext.Current.Request.ServerVariables["HTTP_HOST"]), "application/xml");
            }
            else
            {
                throw new HttpException(404, "Not found");
            }
        }
    }
}
