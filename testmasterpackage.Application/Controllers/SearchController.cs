﻿using AutoMapper;
using testmasterpackage.Application.Helpers;
using testmasterpackage.Application.Models;
using testmasterpackage.Core.DomainModel.Search;
using testmasterpackage.Core.Services.SearchServices;
using testmasterpackage.Core.Services.UmbracoServices;
using testmasterpackage.Dispatcher;
using testmasterpackage.Infrastructure.UmbracoServices;
using System;
using System.Collections.Generic;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace testmasterpackage.Application.Controllers
{
    public class SearchController : SurfaceController
    {
        private IValidateBus validateBus;
        private ISearchService searchService;
        private UmbracoService umbracoService;
        //private UmbracoHelper umbracoHelper;
        public SearchController(IValidateBus validateBus, ISearchService searchService, UmbracoService umbracoService)
        {
            this.validateBus = validateBus;
            this.searchService = searchService;
            this.umbracoService = umbracoService;
        }
        public IList<SearchResultViewModel> GetSearch(string keyword)
        {
            SearchQuery search = new SearchQuery()
            {
                Keyword = keyword
            };

            var validateResult = validateBus.Validate(search);

            IList<SearchResultViewModel> searchList = new List<SearchResultViewModel>();

            if (validateResult.IsValid)
            {
                IEnumerable<Examine.SearchResult> searchResults = searchService.GetSearchResult(search) as IEnumerable<Examine.SearchResult>;

                foreach (var s in searchResults)
                {
                    IPublishedContent content = umbracoService.GetNodeById(s.Id);

                    //SearchResultViewModel searchResult = Mapper.Map<SearchResultViewModel>(content);

                    SearchResultViewModel searchResult = new SearchResultViewModel();
                    searchResult.Title = content.HasValue("menuTitle") ? content.GetPropertyValue("menuTitle").ToString() : content.Name;
                    searchResult.Content = content;
                    searchResult.Summary = GetSearchSummary(s, search, searchResult.Title);

                    searchList.Add(searchResult);
                }
            }

            return searchList;
        }

        private string GetSearchSummary(Examine.SearchResult result, SearchQuery searchQuery, string defaultTitle)
        {
            string summary = string.Empty;
            try
            {
                if (searchQuery.LimitLength == 0)
                {
                    summary = string.Empty;
                }
                else
                {
                    if (result.Fields.ContainsKey(Alias.PropertyAlias.GridArticleContent))
                    {
                        summary = result.Fields[Alias.PropertyAlias.GridArticleContent];
                    }
                    if (string.IsNullOrEmpty(summary) && result.Fields.ContainsKey(Alias.PropertyAlias.GridContent))
                    {
                        summary = result.Fields[Alias.PropertyAlias.GridContent];
                    }

                    summary = SearchHelper.ReplaceNewLineToSpace(summary);

                    //When no content in the page; select preview text from the Title instead..
                    if (string.IsNullOrEmpty(summary))
                    {
                        summary = defaultTitle.Length > searchQuery.LimitLength ? defaultTitle.Trim().Substring(0, searchQuery.LimitLength) + "..." : defaultTitle;
                    }
                    else
                    {
                        summary = summary.Length > searchQuery.LimitLength ? summary.Trim().Substring(0, searchQuery.LimitLength) + "..." : summary;
                    }

                    if (searchQuery.EnableHightLightKeyword)
                    {
                        summary = SearchHelper.HighlightKeywords(summary, searchQuery.Keyword);
                    }
                }
            }
            catch (Exception ex)
            {
                //LogHelper.Error<string>("Error occurs in GetSearchSummary()", ex);
            }
            return summary;
        }
    }
}
