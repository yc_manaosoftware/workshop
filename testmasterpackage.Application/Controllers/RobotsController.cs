﻿using testmasterpackage.Core.Services.UmbracoServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using umbraco;
using Umbraco.Core;
using Umbraco.Core.Configuration.UmbracoSettings;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Umbraco.Web.Routing;

namespace testmasterpackage.Application.Controllers
{
    public class RobotsController : UmbracoController
    { 
        private IRobotsService robotService;

        public RobotsController(IRobotsService robotService) : base(UmbracoContext.EnsureContext(new HttpContextWrapper(System.Web.HttpContext.Current)
                , ApplicationContext.Current
                , new global::Umbraco.Web.Security.WebSecurity(new HttpContextWrapper(System.Web.HttpContext.Current)
                , ApplicationContext.Current)
                , true))
        { 
            this.robotService = robotService;
        }

        public RobotsController() : base(UmbracoContext.EnsureContext(new HttpContextWrapper(System.Web.HttpContext.Current)
                , ApplicationContext.Current
                , new global::Umbraco.Web.Security.WebSecurity(new HttpContextWrapper(System.Web.HttpContext.Current)
                , ApplicationContext.Current)
                , true))
        {
            
        }

        public ActionResult GetRobotsText()
        {

            string robotsContentText = string.Empty;

            var currentContent = robotService.GetCurrentContent();

            if (currentContent == null)
            {
                currentContent = robotService.GetFirstContent();      
            }
            if (currentContent != null)
            {
                robotsContentText = robotService.GetRobotsContent(currentContent);

            }
            if (currentContent != null)
            {
                return Content(robotsContentText, "text/plain");
            }
            else
            {
                throw new HttpException(404, "Not found");
            }
        }
    }
}
