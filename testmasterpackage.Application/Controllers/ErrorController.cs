﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using testmasterpackage.Core.Alias;
using testmasterpackage.Services;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Umbraco.Web.Routing;
using testmasterpackage.Infrastructure.UmbracoServices;

namespace testmasterpackage.Application.Controllers
{
    public class ErrorController : SurfaceController
    {
        private UmbracoHelper umbracoHelper;
        private ErrorRedirectService errorRedirectService;
        public ErrorController(UmbracoHelper umbracoHelper)
        {
            this.umbracoHelper = umbracoHelper;
            this.errorRedirectService = new ErrorRedirectService(umbracoHelper);
        }
        public ActionResult ErrorRedirect()
        {
            var errorPage= errorRedirectService.GetContentId();
            return RedirectToUmbracoPage(errorPage);
        }
    }
}
