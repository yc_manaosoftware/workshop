﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;

namespace testmasterpackage.Application.Helpers
{
    public class CriticalCssHelper
    {
        public static string GetCriticalCss(IPublishedContent node)
        {
            string css = String.Empty;
            string cssPath = HttpContext.Current.Server.MapPath("~/App_Data/css/" + node.Id + ".css");
            if (System.IO.File.Exists(cssPath))
            {
                try
                {
                    DateTime lastModufy = System.IO.File.GetLastWriteTime(cssPath);
                    CssCached cssCached = (CssCached)ApplicationContext.Current.ApplicationCache.RuntimeCache.GetCacheItem("css_" + node.Id);

                    if (cssCached == null || cssCached.LastModify != lastModufy)
                    {
                        css = System.IO.File.ReadAllText(cssPath);

                        css = css.Replace("\r\n", "").Replace("\r", "").Replace("\n", "").Replace(";     ", "");
                        css = "<style id=\"criticalCss\">\r\n" + css + "\r\n</style>";

                        ApplicationContext.Current.ApplicationCache.RuntimeCache.InsertCacheItem("css_" + node.Id, () => new CssCached() { LastModify = lastModufy, Css = css }, new TimeSpan(24, 0, 0));
                    }
                    else
                        css = cssCached.Css;
                }
                catch { }
            }

            return css;
        }

        public class CssCached
        {
            public DateTime LastModify { get; set; }
            public string Css { get; set; }
        }
    }
}
