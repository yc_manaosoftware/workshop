﻿using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace testmasterpackage.Application.Helpers
{
    public static class ContentHelper
    {
        public static IPublishedContent GetConfiguration(int currentContentId, string configAlias, UmbracoHelper UmbracoHelper)
        {
            string configurationPropertyValue = string.Empty;
            IPublishedContent configurationNode = null;
            if (UmbracoHelper.TypedContent(currentContentId) != null)
            {
                IPublishedContent content = UmbracoHelper.Content(currentContentId);
                IPublishedContent homeNode = content.AncestorOrSelf(1);
                if (homeNode != null)
                {
                    configurationNode = homeNode.Descendants(configAlias).FirstOrDefault();
                }
            }
            return configurationNode;
        }
    }
}
