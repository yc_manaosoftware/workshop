﻿using System.Linq;
using HtmlAgilityPack;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace testmasterpackage.Application.Helpers
{
    public class RTEHelper
    {
        public static HtmlDocument ConvertLink(string strdocument)
        {
            HtmlDocument document = new HtmlDocument();
            document.LoadHtml(strdocument);
            var links = document.DocumentNode.SelectNodes("//a[@href]");
            if (links != null)
            {
                foreach (HtmlNode link in links.ToList())
                {
                    HtmlAttribute atthref = link.Attributes["href"];
                    string strhref = link.GetAttributeValue("href", "");
                    // if extranal url start with / or /umbraco/ , not file , not content
                    if ((strhref.StartsWith("/") || strhref.StartsWith("/umbraco/")) && !strhref.StartsWith("/media/") && !strhref.Contains("localLink"))
                    {
                        IPublishedContent page = UmbracoContext.Current.ContentCache.GetByRoute(strhref);
                        if (page == null)
                        {
                            string startWith = strhref.Contains("umbraco") ? "/umbraco/" : "/";
                            atthref.Value = "http://" + strhref.Replace(startWith, "");
                        }
                    }
                }
            }
            return document;
        }
    }
}
