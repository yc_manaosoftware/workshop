﻿using testmasterpackage.Core.DomainModel.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace testmasterpackage.Application.Helpers
{
    public static class SearchHelper
    {
        public static string ReplaceNewLineToSpace(string text)
        {
            text = text.Replace("\r", " ").Replace("\n", " ").Replace(Environment.NewLine, " ");
            text = text.Trim();
            return text;
        }

        public static string HighlightKeywords(string input, string keywords)
        {
            if (input == string.Empty || keywords == string.Empty)
            {
                return input;
            }

            string[] sKeywords = keywords.Split(' ');
            foreach (string sKeyword in sKeywords)
            {
                try
                {
                    input = Regex.Replace(input, sKeyword, string.Format("<span>{0}</span>", "$0"), RegexOptions.IgnoreCase);
                }
                catch (Exception ex)
                {
                    //LogHelper.Error<string>("Error occurs in HighlightKeywords()", ex);
                }
            }
            return input;
        }
    }
}
