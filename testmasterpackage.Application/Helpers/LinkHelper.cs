﻿using Lecoati.LeBlender.Extension.Models;
using testmasterpackage.Core.Alias;
using testmasterpackage.Core.DomainModel.Editors;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace testmasterpackage.Application.Helpers
{
    public static class LinkHelper
    {
        private static UmbracoHelper _umbracoHelper;
        private static UmbracoHelper umbracoHelper
        {
            get
            {
                if (UmbracoContext.Current != null)
                    _umbracoHelper = new UmbracoHelper(UmbracoContext.Current);

                return _umbracoHelper;
            }
            set
            {
                _umbracoHelper = value;
            }
        }

        public static string AsUrl(string url)
        {
            if (!string.IsNullOrEmpty(url))
            {
                Uri output;
                if (!url.ToLower().StartsWith("mailto:") && !url.ToLower().StartsWith("tel:"))
                {
                    url = !url.StartsWith("http") && !url.Contains("media") ? "http://" + url : url;
                }

                if (Uri.TryCreate(url, UriKind.Absolute, out output))
                {
                    url = output.AbsoluteUri;
                }
            }
            return url;
        }
        public static LinkModel AsUrl(LeBlenderModel model, string alias)
        {
            if (!string.IsNullOrEmpty(model.Items.FirstOrDefault().GetValue(alias)))
            {
                LinkModel link = new LinkModel();
                link = JsonConvert.DeserializeObject<LinkModel>(model.Items.FirstOrDefault().GetValue(alias));
                link = GetPageTitle(link);
                if (link != null)
                {
                    link.Url = link.IsExternal && link.Url[0] != '/' ? ConvertUrl(link.Url) : link.Url;
                }
                return link;
            }
            return null;
        }
        private static LinkModel GetPageTitle(LinkModel link)
        {
            if (link != null)
            {
                if (link.Id != 0)
                {
                    IPublishedContent page = umbracoHelper.TypedContent(link.Id);
                    if (page != null)
                    {
                        link.Name = page.HasValue(PropertyAlias.MenuTitle) ? page.GetPropertyValue<string>(PropertyAlias.MenuTitle) : page.Name;
                    }
                }
            }

            return link;
        }
        private static string ConvertUrl(string url)
        {
            if (!string.IsNullOrEmpty(url))
            {
                Uri output;
                if (!url.ToLower().StartsWith("mailto:") && !url.ToLower().StartsWith("tel:"))
                {
                    url = !url.StartsWith("http") && !url.Contains("media") ? "http://" + url : url;
                }

                if (Uri.TryCreate(url, UriKind.Absolute, out output))
                {
                    url = output.AbsoluteUri;
                }
            }
            return url;
        }
    }
}
