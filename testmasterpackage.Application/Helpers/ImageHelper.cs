﻿using Umbraco.Core.Models;
using Umbraco.Web;
using System.Web;
using System.IO;
using testmasterpackage.Core.Services.UmbracoServices;
using testmasterpackage.Infrastructure.UmbracoServices;
using testmasterpackage.Core.DomainModel.Media;
using System;
using Umbraco.Core;
using Umbraco.Web.Models;
using StructureMap;
using testmasterpackage.Application.IoC;
using Lecoati.LeBlender.Extension.Models;
using System.Linq;
using Newtonsoft.Json;

namespace testmasterpackage.Application.Helpers
{
    public class ImageHelper
    {
        private static IManaoMediaService _manaoMediaService = null;

        static IContainer container = StructureMapContainerInit.InitializeContainer();

        private static UmbracoHelper umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
        private static IManaoMediaService manaoMediaService
        {
            get
            {
                if (_manaoMediaService == null)
                    _manaoMediaService = container.GetInstance<IManaoMediaService>();

                return _manaoMediaService;
            }
        }
        private static HttpContext _httpContext;
        private static HttpContext httpContext
        {
            get
            {
                if (HttpContext.Current != null)
                    _httpContext = HttpContext.Current;

                return _httpContext;
            }
            set
            {
                _httpContext = value;
            }
        }
        public static HtmlString RenderImage(Guid guid, string alt, string title = "", string css = "")
        {
            HtmlString output = new HtmlString("");
            var altTag = string.Empty;
            var titleTag = string.Empty;
            var cssTag = string.Empty;
            ManaoImage image = manaoMediaService.GetImageByGuid(guid);
            if (image != null)
            {
                altTag = string.IsNullOrEmpty(alt)? altTag = string.Format("alt=\"{0}\"", HttpUtility.HtmlEncode(Path.GetFileNameWithoutExtension(image.Url))) :
                    altTag = string.Format("alt=\"{0}\"", HttpUtility.HtmlEncode(alt));

                if (!string.IsNullOrEmpty(title))
                    titleTag = string.Format("title=\"{0}\"", HttpUtility.HtmlEncode(title));

                if (!string.IsNullOrEmpty(css))
                    cssTag = string.Format("class=\"{0}\"", HttpUtility.HtmlEncode(css));

                output = new HtmlString(string.Format("<img src=\"{0}\" {1} {2} {3} />", image.Url, altTag, titleTag, cssTag));
            }

            return output;
        }
   
        public static HtmlString RenderImageSlimsy(Guid guid, string alt, string title, int width, int height = 0)
        {
            var media = umbracoHelper.TypedMedia(guid);
            dynamic umbracoFile = media.GetPropertyValue("umbracoFile");
            Umbraco.Web.Models.ImageCropFocalPoint focalPoint = umbracoFile.focalPoint;
            var culture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            string x = focalPoint != null ? String.Format("data-center-x={0}", focalPoint.Left.ToString("N4", culture)) : String.Empty;
            string y = focalPoint != null ? String.Format("data-center-y={0}", focalPoint.Top.ToString("N4", culture)) : String.Empty;

            HtmlString output = new HtmlString(String.Empty);
            string altTag = String.Empty;
            string titleTag = String.Empty;
            var url = GetImageSlimsyUrl(guid, width, height);
            if (!String.IsNullOrEmpty(url))
            {
                altTag = !String.IsNullOrEmpty(alt) ? String.Format("alt = \"{0}\"", HttpUtility.HtmlEncode(alt)) : String.Format("alt = \"{0}\"", HttpUtility.HtmlEncode(Path.GetFileNameWithoutExtension(url)));
                titleTag = !String.IsNullOrEmpty(title) ? String.Format("title = \"{0}\"", HttpUtility.HtmlEncode(title)) : String.Empty;
                output = new HtmlString(String.Format("<img src=\"{0}\" {1} {2} {3} {4} class=\"image-item\">", url, altTag, titleTag, x.Trim(), y.Trim()));
            }
            return output;
        }   

        public static ManaoImage GetImageUrlOrDefault(LeBlenderModel leBlenderModel, string alias)
        {
            ManaoImage image = null;
            if (leBlenderModel!=null && leBlenderModel.Items!=null && leBlenderModel.Items.First() != null)
            {
                image = JsonConvert.DeserializeObject<ManaoImage>(leBlenderModel.Items.First().GetValue("image"));
            }
            var pageId = UmbracoContext.Current?.PageId;
            if (image != null)
            {             
                if (!string.IsNullOrEmpty(image.Url) && System.IO.File.Exists(httpContext.Server.MapPath(image.Url))) // image was removed from media section
                {
                    return image;
                }
                else
                {
                    if (pageId != null)
                    {
                        var config = ContentHelper.GetConfiguration((int)pageId, "defaultImage", umbracoHelper);
                        return GetDefaultImage(config, "image");
                    }
                    return image;
                }
            }
            else
            {
                return null;
            }

        }

        public static ManaoImage GetDefaultImage(IPublishedContent imageContent, string alias)
        {
            ManaoImage defaultImage = new ManaoImage(); ;
            var json = imageContent.GetPropertyValue<string>(alias);
            if (json != null)
            {
                var image = JsonConvert.DeserializeObject<ManaoImage>(json);
                var media = umbracoHelper.TypedMedia(image.Guid);
                if (media != null)
                {
                    if (media != null)
                    {
                        defaultImage = image;
                    }
                }
            }
            return defaultImage;
        }
        public static string GetImageSlimsyUrl(Guid guid, int width, int height = 0)
        {
            string result = string.Empty;
            IPublishedContent media = umbracoHelper.TypedMedia(guid);
            if (media != null)
            {
                if (height > 0)
                {
                    result = media.GetCropUrl(width, height, furtherOptions: "&slimmage=true", quality: 85);
                }
                else
                {
                    result = media.GetCropUrl(width, furtherOptions: "&slimmage=true", quality: 85);
                }
            }

            return result;
        }
        public static string GetImageUrl(Guid guid, int width, int height, ImageCropMode cropMode, int quality = 85, string format = "jpg", string bgcolor = "ffffff")
        {
            string result = string.Empty;
            var mediaContent = umbracoHelper.TypedMedia(guid);
            if (mediaContent != null)
            {
                if (mediaContent != null)
                {
                    if (height == 0)
                {
                        result = mediaContent.Url + "?width=" + width + "&mode=pad" + "&format=" + format + "&bgcolor=" + bgcolor;
                }
                else if (width == 0)
                {
                    result = mediaContent.Url + "?height=" + height + "&mode=boxpad" + "&format=" + format + "&bgcolor=" + bgcolor;
                }
                else
                {
                    result = mediaContent.GetCropUrl(width, height, ratioMode: ImageCropRatioMode.Height, furtherOptions: "", imageCropMode: cropMode) + "&format=" + format;
                    if (bgcolor != null)
                    {
                        result += "&bgcolor=" + bgcolor;
                    }
                }
                if (quality != 0)
                    result += "&quality=" + quality;
                }
            }

            return result;
        }
        public static string GetAltImageOrDefault(Guid guid, string alt)
        {
            if (String.IsNullOrEmpty(alt))
            {
                var mediaContent = umbracoHelper.TypedMedia(guid);
                if (mediaContent != null)
                {
                    return Path.GetFileNameWithoutExtension(mediaContent.Name);
                }
                else
                {
                    return String.Empty;
                }

            }
            else
            {
                return alt;
            }


        }

    }
}
