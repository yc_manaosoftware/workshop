﻿using Lecoati.LeBlender.Extension.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Logging;

namespace testmasterpackage.Application.Helpers
{
    public static class LeBlenderHelper
    {
        public static GridEditor GetGridEditor(string alias)
        {
            var gridEditor = GetLeBlenderGridEditors(true).Where(x => x.Alias == alias).First();
            return gridEditor;
        }
        public static HtmlString RenderEmptyModule(string alias)
        {
            var gridEditor = GetLeBlenderGridEditors(true).Where(x => x.Alias == alias).First();
            string html = "<div class=\"umb-editor-placeholder\">";
            html += "<div class=\"usky-editor-placeholder\">";
            html += "<i class=\"icon " + gridEditor.Icon + "\"></i>";
            html += "<div class=\"help-text ng-binding\">" + gridEditor.Name + "</div>";
            html += "</div>";
            html += "</div>";
            HtmlString result = new HtmlString(html);
            return result;
        }
        private static IEnumerable<GridEditor> GetLeBlenderGridEditors(bool onlyLeBlenderEditor)
        {
            var editors = new List<GridEditor>();
            var gridConfig = HttpContext.Current.Server.MapPath("~/Config/grid.editors.config.js");
            if (System.IO.File.Exists(gridConfig))
            {
                try
                {
                    var arr = JArray.Parse(System.IO.File.ReadAllText(gridConfig));
                    var json = arr.ToString();
                    var parsed = JsonConvert.DeserializeObject<IEnumerable<GridEditor>>(json);
                    editors.AddRange(parsed);

                    if (onlyLeBlenderEditor)
                    {
                        editors = editors.Where(r => r.View.Equals("/App_Plugins/LeBlender/core/LeBlendereditor.html", StringComparison.InvariantCultureIgnoreCase) ||
                            r.View.Equals("/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html", StringComparison.InvariantCultureIgnoreCase)).ToList();
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.Error<string>("Could not parse the contents of grid.editors.config.js into a JSON array", ex);
                }
            }
            return editors;
        }
    }
}
