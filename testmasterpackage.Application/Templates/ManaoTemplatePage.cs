﻿using AutoMapper;
using testmasterpackage.Core.DomainModel.Base;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using testmasterpackage.Core.DomainModel;
using testmasterpackage.Application.Templates;

namespace testmasterpackage.Application.Templates
{
    public abstract class ManaoTemplatePage<TModel> : UmbracoViewPage<TModel> where TModel : DomainModel
    {
        protected override void SetViewData(ViewDataDictionary viewData)
        {
            CultureInfo culture = CultureInfo.CurrentCulture;
            ////// bind the model (use context culture as default, if available)
            if (UmbracoContext.PublishedContentRequest != null && UmbracoContext.PublishedContentRequest.Culture != null)
                culture = UmbracoContext.PublishedContentRequest.Culture;

            viewData = new ViewDataDictionary<TModel>(BindModel(viewData.Model, typeof(TModel), culture)); ;

            // set the view data

            base.SetViewData(viewData);
        }

        private dynamic BindModel(object source, Type modelType, CultureInfo culture)
        {
            // null model, return
            if (source == null) return null;

            var sourceContent = source as IPublishedContent;
            if (sourceContent == null && typeof(IRenderModel).IsAssignableFrom(source.GetType()))
            {
                // else check if it's an IRenderModel, and get the content
                sourceContent = ((IRenderModel) source).Content;
            }

            TModel domain = null;
            var model = Mapper.Map(sourceContent, domain);
            
            model.CurrentCulture = culture;
            //model.Content = sourceContent;

            return model;
        }

    }
}
