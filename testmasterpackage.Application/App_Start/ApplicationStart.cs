﻿using AutoMapper;
using testmasterpackage.Application.Alias;
using testmasterpackage.Application.IoC;
using testmasterpackage.Core.DomainModel;
using testmasterpackage.Core.DomainModel.Base;
using testmasterpackage.Core.Extensions;
using testmasterpackage.Dispatcher;
using Lecoati.LeBlender.Extension.Models;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Skybrud.Umbraco.GridData;
using StructureMap;
using StructureMap.Web.Pipeline;
using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Core.Persistence;
using Umbraco.Core.Publishing;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Models.Trees;
using Umbraco.Web.Trees;
using System.Web.Optimization;
using System.Configuration;
using System.IO;
using System.Net;

namespace testmasterpackage.Application.App_Start
{
    public class ApplicationStart : ApplicationEventHandler
    {
        private IValidateBus validateBus;
        private IContainer container;
        public static StructureMapDependencyScope StructureMapDependencyScope { get; set; }

        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
      {
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            //Setting Cinfiguration
            this.container = StructureMapContainerInit.InitializeContainer();
            StructureMapDependencyScope = new StructureMapDependencyScope(container);
            DependencyResolver.SetResolver(StructureMapDependencyScope);
            GlobalConfiguration.Configuration.DependencyResolver = new StructureMapWebApiDependencyResolver(container);
            //NEED FIX HERE


            AutoMapperConfiguration.Configure(this.container);

            validateBus = container.GetInstance<IValidateBus>();

            //Listen for when content is being saved
            ContentService.Publishing += ContentService_Publishing;

            ContentService.Published += ContentService_GeneratedCriticalCss;
            //TreeControllerBase.MenuRendering += TreeControllerBase_MenuRendering;

        }

        private void ContentService_GeneratedCriticalCss(IPublishingStrategy sender, PublishEventArgs<IContent> e)
        {
            try
            {
                string generatorPath = ConfigurationManager.AppSettings["CssGeneratorUrl"];
                if (!String.IsNullOrEmpty(generatorPath))
                {
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/App_Data/css/")))
                    {
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/App_Data/css/"));
                    }
                    string host = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port != 80 ? ":" + HttpContext.Current.Request.Url.Port : "");

                    string cssUrl = generatorPath + "?host=" + host + "{0}" +
                                    "&css=" + host + Styles.RenderFormat("{0}", "~/bundles/master-css").ToHtmlString().Replace("\r\n", "");

                    string height = ConfigurationManager.AppSettings["CriticalCssHeight"];
                    string width = ConfigurationManager.AppSettings["CriticalCssWidth"];
                    if (!String.IsNullOrEmpty(height)) cssUrl += "&height=" + height;
                    if (!String.IsNullOrEmpty(width)) cssUrl += "&width=" + width;

                    string targetTemplate = HttpContext.Current.Server.MapPath("~/App_Data/css/") + "{0}.{1}";

                    var urlProvider = Umbraco.Web.UmbracoContext.Current.UrlProvider;
                    urlProvider.Mode = Umbraco.Web.Routing.UrlProviderMode.AutoLegacy;

                    new System.Threading.Tasks.Task(() =>
                    {
                        foreach (var content in e.PublishedEntities)
                        {
                            //if (content.ContentType.Alias == NodeAlias.Homepage
                            //    || content.ContentType.Alias == NodeAlias.Subpage
                            //    || content.ContentType.Alias == NodeAlias.ProductList
                            //    || content.ContentType.Alias == NodeAlias.ProductPage
                            //    || content.ContentType.Alias == NodeAlias.IRPage
                            //    || content.ContentType.Alias == NodeAlias.IRSectionPage
                            //    || content.ContentType.Alias == NodeAlias.IRSubpage
                            //    || content.ContentType.Alias == NodeAlias.FinancialPage
                            //    || content.ContentType.Alias == NodeAlias.EventList
                            //    || content.ContentType.Alias == NodeAlias.AnnualListPage
                            //    || content.ContentType.Alias == NodeAlias.PressReleases
                            //    || content.ContentType.Alias == NodeAlias.RmaPage)
                            //{
                            string nodeUrl = urlProvider.GetUrl(content.Id);
                            string generatorUrl = String.Format(cssUrl, nodeUrl);
                            GenerateCriticalCss(content, generatorUrl, targetTemplate);
                            //}
                        }
                    }).Start();
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error<Exception>("Can't connect to Critical Css Generator", ex);
            }
        }

        private void GenerateCriticalCss(IContent content, string generatorUrl, string targetTemplate)
        {
            using (WebClient cssClient = new WebClient())
            {

                if (!String.IsNullOrEmpty(generatorUrl))
                {
                    string tmpFile = String.Format(targetTemplate, content.Id, "tmp");
                    string targetFile = String.Format(targetTemplate, content.Id, "css");

                    try
                    {
                        cssClient.DownloadFile(generatorUrl, tmpFile);
                        System.IO.File.Copy(tmpFile, targetFile, true);
                        System.IO.File.Delete(tmpFile);
                    }
                    catch (Exception ex)
                    {
                        LogHelper.Error<Exception>("Can't connect to Critical Css Generator", ex);
                    }
                }
            }
        }

        private void TreeControllerBase_MenuRendering(TreeControllerBase sender, MenuRenderingEventArgs e)
        {
            var path = "/App_Plugins/UploadContent/UploadContent.html";
            var title = "Upload Content to Remote Server";

            var menu = new MenuItem();
            menu.Icon = "wine-glass";
            menu.SeperatorBefore = true;

            menu.LaunchDialogView(path, title);

            e.Menu.Items.Add(menu);
        }

        private void ContentService_Publishing(IPublishingStrategy sender, PublishEventArgs<IContent> e)
        {
            var domains = container.GetAllInstances<IDomainModel>();

            foreach (var entity in e.PublishedEntities)
            {
                #region validate model in grid 
                if (entity.HasProperty(PropertyAlias.GridContent))
                {
                    var json = entity.GetValue<string>(PropertyAlias.GridContent);

                    GridDataModel grid = GridDataModel.Deserialize(json);
                    if (grid != null)
                    {
                        foreach (var domain in domains)
                        {
                            var gridModels = grid.GetAllControls(domain.GetAliasName());
                            if (gridModels != null && gridModels.Any())
                            {
                                foreach (var gridModel in gridModels)
                                {
                                    LeBlenderModel editor = Newtonsoft.Json.JsonConvert.DeserializeObject<LeBlenderModel>(gridModel.JObject.ToString());
                                    if (editor.Items != null && editor.Items.Any())
                                    {
                                        foreach (var item in editor.Items)
                                        {
                                            var model = Mapper.Map(item, domain);
                                            ValidateModel(e, model);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
                #region Validate model
                // Remove else for some case that have both of grid and property type
                //else
                //{
                var domainItems = domains.Where(x => x.IsAliasOf(entity.ContentType.Alias));
                if (domainItems.Any())
                {
                    foreach (var domainItem in domainItems)
                    {
                        var model = Mapper.Map(entity, domainItem);
                        ValidateModel(e, model);
                    }

                }
                //}
                #endregion
            }
        }

        private void ValidateModel(PublishEventArgs<IContent> e, IDomainModel model)
        {
            dynamic castModel = Convert.ChangeType(model, model.GetType());
            var result = validateBus.Validate(castModel);

            if (!result.IsValid)
            {
                e.Cancel = true;

                foreach (var error in result.Errors)
                {
                    var errors = new EventMessage(error.PropertyName, error.ErrorMessage, EventMessageType.Error);
                    e.CancelOperation(errors);
                }
            }
        }
    }
}
