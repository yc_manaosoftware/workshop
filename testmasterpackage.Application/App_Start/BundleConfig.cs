﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Optimization;

namespace testmasterpackage.Application.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundle/libraries").Include(
                "~/scripts/libs/jquery/jquery-3.2.1.min.js",
                "~/scripts/libs/angular/angular.min.js",
                "~/scripts/libs/bootstrap/popper.min.js",
                "~/scripts/libs/bootstrap/bootstrap.min.js",
                "~/scripts/libs/jasny/jasny-bootstrap.min.js",
                "~/scripts/libs/slimmage/slimmage.settings.min.js",
                "~/scripts/libs/slimmage/slimmage.min.js",
                "~/scripts/libs/jquery_dotdotdot/jquery.dotdotdot.min.js",
                "~/scripts/libs/cookies/CookiesAlert_2.1.js"
                ));

            bundles.Add(new ScriptBundle("~/bundle/scripts").Include( 
                "~/scripts/src/settings.js",
                "~/scripts/src/utility.js",
                "~/scripts/src/app.js",
                "~/scripts/src/cookies-directive.js"
                ));

            bundles.Add(new StyleBundle("~/bundle/styles")
                .Include("~/css/libs/font-awesome/font-awesome.min.css", new CssRewriteUrlTransform())
                .Include("~/css/libs/bootstrap/bootstrap.min.css", new CssRewriteUrlTransform())
                .Include("~/css/Module-Image.css", new CssRewriteUrlTransform())
                .Include("~/css/style.min.css", new CssRewriteUrlTransform())
               );

            #if !DEBUG
                BundleTable.EnableOptimizations = true;
            #endif
        }
    }
}
