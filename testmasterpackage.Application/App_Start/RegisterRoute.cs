﻿using testmasterpackage.Core.Services.UmbracoServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using Umbraco.Core;
using Umbraco.Web;

namespace testmasterpackage.Application.App_Start
{
    public class RegisterRoute : IApplicationEventHandler
    {
        public void OnApplicationStarted(UmbracoApplicationBase umbracoApplication,ApplicationContext applicationContext)
        { 
            RouteTable.Routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            RouteTable.Routes.MapRoute(
                name: "RobotRoute",
                url : "robots.txt",
                defaults : new
                {
                    Controller = "Robots",
                    Action = "GetRobotsText",
                }
                );

            RouteTable.Routes.MapRoute(
              "SitemapRoute",
              "sitemap.xml",
              new
              {
                  Controller = "Sitemap",
                  Action = "GetSitemap",
              });
        }

        public void OnApplicationInitialized(UmbracoApplicationBase umbracoApplication,ApplicationContext applicationContext)
        {

        }
        public void OnApplicationStarting(UmbracoApplicationBase umbracoApplication,ApplicationContext applicationContext)
        {
         
        }
    }
}
