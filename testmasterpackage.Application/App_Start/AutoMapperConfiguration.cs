﻿using AutoMapper;
using testmasterpackage.Application.AutoMapperConfiguration.Base;
using testmasterpackage.Application.Models;
using testmasterpackage.Core.DomainModel;
using Lecoati.LeBlender.Extension.Models;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace testmasterpackage.Application.App_Start
{
    public class AutoMapperConfiguration
    {
        public static void Configure(IContainer container)
        {
            var mappers = container.GetAllInstances<IMapperBase>();
            foreach (var mapper in mappers)
                mapper.Configure(container);

        }
    }
}
