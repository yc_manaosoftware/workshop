﻿using System;
using NUnit.Framework;
using Moq;
using Umbraco.Core.Models;
using testmasterpackage.Services;
using Microsoft.QualityTools.Testing.Fakes;
using System.Xml;
using testmasterpackage.Core.Alias;

namespace Master8._2._0.Test
{
    [TestFixture]
    public class TestSitemapService
    {
        [Test, Category("SitemapService")]
        public void CanGetSitemapXML()
        {
            var content = MockIPublishedContent();
            string xmlString = String.Empty;
            XmlDocument result = new XmlDocument();

            using (ShimsContext.Create())
            {
                Umbraco.Web.Fakes.ShimPublishedContentExtensions.HasPropertyIPublishedContentString = (IPublishedContent rootContent, string alias) => true;
                Umbraco.Web.Fakes.ShimPublishedContentExtensions.GetPropertyValueIPublishedContentString = (IPublishedContent rootContent, string alias) => true;
                Umbraco.Web.Fakes.ShimPublishedContentExtensions.GetPropertyValueOf1IPublishedContentString<bool>((doc, alias) => { return true; });
                Umbraco.Web.Fakes.ShimPublishedContentExtensions.GetPropertyValueOf1IPublishedContentString<string>((doc, alias) => 
                {
                    if (alias.Equals(PropertyAlias.SitemapPriority))
                    {
                        return "{ \"state\": 1,  \"priority\": 0.5 }";
                    }
                    else if (alias.Equals(PropertyAlias.ChangeFrequency))
                    {
                        return "always";
                    } else
                    {
                        return string.Empty;
                    }
                });
                xmlString = new SitemapService().GetSitemapXML(content, "http", "localhost");
            }

            result.LoadXml(xmlString);

            Assert.AreEqual(result.FirstChild.Name, "urlset");
            Assert.AreNotEqual(result.FirstChild["xmlns"], string.Empty);
            Assert.AreNotEqual(result.FirstChild["xmlns:xsi"], string.Empty);
            Assert.AreNotEqual(result.FirstChild["xsi:schemaLocation"], string.Empty);
        }

        [Test, Category("SitemapService")]
        public void CanGetSitemapXMLWithoutPriorityValue()
        {
            var content = MockIPublishedContent();
            string xmlString = String.Empty;
            XmlDocument result = new XmlDocument();

            using (ShimsContext.Create())
            {
                Umbraco.Web.Fakes.ShimPublishedContentExtensions.HasPropertyIPublishedContentString = (IPublishedContent rootContent, string alias) => true;
                Umbraco.Web.Fakes.ShimPublishedContentExtensions.GetPropertyValueIPublishedContentString = (IPublishedContent rootContent, string alias) => true;
                Umbraco.Web.Fakes.ShimPublishedContentExtensions.GetPropertyValueOf1IPublishedContentString<bool>((doc, alias) => { return true; });
                Umbraco.Web.Fakes.ShimPublishedContentExtensions.GetPropertyValueOf1IPublishedContentString<string>((doc, alias) =>
                {
                    if (alias.Equals(PropertyAlias.ChangeFrequency))
                    {
                        return "always";
                    }
                    else
                    {
                        return string.Empty;
                    }
                });
                xmlString = new SitemapService().GetSitemapXML(content, "http", "localhost");
            }

            result.LoadXml(xmlString);
            Assert.AreEqual(result.FirstChild.Name, "urlset");
            Assert.AreNotEqual(result.FirstChild["xmlns"], string.Empty);
            Assert.AreNotEqual(result.FirstChild["xmlns:xsi"], string.Empty);
            Assert.AreNotEqual(result.FirstChild["xsi:schemaLocation"], string.Empty);
        }

        [Test, Category("SitemapService")]
        public void CanGetSitemapXMLWithoutPriorityAndFrequencyValue()
        {
            var content = MockIPublishedContent();
            string xmlString = String.Empty;
            XmlDocument result = new XmlDocument();

            using (ShimsContext.Create())
            {
                Umbraco.Web.Fakes.ShimPublishedContentExtensions.HasPropertyIPublishedContentString = (IPublishedContent rootContent, string alias) => true;
                Umbraco.Web.Fakes.ShimPublishedContentExtensions.GetPropertyValueIPublishedContentString = (IPublishedContent rootContent, string alias) => true;
                Umbraco.Web.Fakes.ShimPublishedContentExtensions.GetPropertyValueOf1IPublishedContentString<bool>((doc, alias) => { return true; });
                Umbraco.Web.Fakes.ShimPublishedContentExtensions.GetPropertyValueOf1IPublishedContentString<string>((doc, alias) =>
                {
                    return string.Empty;
                });
                xmlString = new SitemapService().GetSitemapXML(content, "http", "localhost");
            }

            result.LoadXml(xmlString);
            Assert.AreEqual(result.FirstChild.Name, "urlset");
            Assert.AreNotEqual(result.FirstChild["xmlns"], string.Empty);
            Assert.AreNotEqual(result.FirstChild["xmlns:xsi"], string.Empty);
            Assert.AreNotEqual(result.FirstChild["xsi:schemaLocation"], string.Empty);
        }

        [Test, Category("SitemapService")]
        public void CanGetSitemapXMLWithoutFrequencyValue()
        {
            var content = MockIPublishedContent();
            string xmlString = String.Empty;
            XmlDocument result = new XmlDocument();

            using (ShimsContext.Create())
            {
                Umbraco.Web.Fakes.ShimPublishedContentExtensions.HasPropertyIPublishedContentString = (IPublishedContent rootContent, string alias) => true;
                Umbraco.Web.Fakes.ShimPublishedContentExtensions.GetPropertyValueIPublishedContentString = (IPublishedContent rootContent, string alias) => true;
                Umbraco.Web.Fakes.ShimPublishedContentExtensions.GetPropertyValueOf1IPublishedContentString<bool>((doc, alias) => { return true; });
                Umbraco.Web.Fakes.ShimPublishedContentExtensions.GetPropertyValueOf1IPublishedContentString<string>((doc, alias) =>
                {
                    if (alias.Equals(PropertyAlias.SitemapPriority))
                    {
                        return "{ \"state\": 1,  \"priority\": 0.5 }";
                    }
                    else
                    {
                        return string.Empty;
                    }
                });
                xmlString = new SitemapService().GetSitemapXML(content, "http", "localhost");
            }

            result.LoadXml(xmlString);
            Assert.AreEqual(result.FirstChild.Name, "urlset");
            Assert.AreNotEqual(result.FirstChild["xmlns"], string.Empty);
            Assert.AreNotEqual(result.FirstChild["xmlns:xsi"], string.Empty);
            Assert.AreNotEqual(result.FirstChild["xsi:schemaLocation"], string.Empty);
        }

        private static IPublishedContent MockIPublishedContent(int id = 1000, string name = "Home", string url = "/")
        {
            var mock = new Mock<IPublishedContent>();
            mock.Setup(x => x.Id).Returns(id);
            mock.Setup(x => x.Name).Returns(name);
            mock.Setup(x => x.Url).Returns(url);
            mock.Setup(x => x.UpdateDate).Returns(DateTime.Now);

            return mock.Object;
        }
    }
}
