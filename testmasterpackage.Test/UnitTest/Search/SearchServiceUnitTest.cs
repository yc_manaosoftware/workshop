﻿using testmasterpackage.Application.Controllers;
using testmasterpackage.Core.DomainModel.Search;
using testmasterpackage.Core.Services.SearchServices;
using testmasterpackage.Core.Services.UmbracoServices;
using testmasterpackage.Core.ValidationHandler.Base;
using testmasterpackage.Dispatcher;
using testmasterpackage.Infrastructure.UmbracoServices;
using testmasterpackage.Test.UnitTest.Base;
using Examine;
using Examine.SearchCriteria;
using Lucene.Net.Store;
using Moq;
using NUnit.Framework;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Umbraco.Core.Models;
using Umbraco.Core.Strings;
using Umbraco.Tests.UnitTest.Base;
using Umbraco.Tests.UnitTest.Search.TestFiles;
using Umbraco.Web;
using UmbracoExamine;

namespace Umbraco.Tests.UnitTest.Search
{
    [TestFixture]
    //[Microsoft.VisualStudio.TestTools.UnitTesting.DeploymentItem("AutoMapper.Net4.dll")]
    public class SearchServiceUnitTest : UnitTestBase
    {
        protected override void FreezeResolution()
        {
            UmbracoExamineSearcher.DisableInitializationCheck = true;
            BaseUmbracoIndexer.DisableInitializationCheck = true;
            ShortStringHelperResolver.Current = new ShortStringHelperResolver(new DefaultShortStringHelper(SettingsForTests.GetDefault()));

            base.FreezeResolution();
        }

        public override void TearDown()
        {
            base.TearDown();

            UmbracoExamineSearcher.DisableInitializationCheck = null;
            BaseUmbracoIndexer.DisableInitializationCheck = null;
        }

        private IPublishedContent GetNode(int id)
        {
            var ctx = GetUmbracoContext("/test", 1234);
            var doc = ctx.ContentCache.GetById(id);
            Assert.IsNotNull(doc);
            return doc;
        }



        [Test, Category("Search_Controller_UnitTest")]
        public void TestSearch_Successful()
        {
            SearchQuery query = new SearchQuery() { Keyword = "test" };

            IList<Examine.SearchResult> searchResult = new List<Examine.SearchResult>();
            searchResult.Add(new SearchResult() { Id = 1, Score = 0.56f });
            searchResult.Add(new SearchResult() { Id = 2, Score = 0.43f });
            searchResult.Add(new SearchResult() { Id = 3, Score = 0.89f });

            ValidationResult validateResult = new ValidationResult();
            var ctx = GetUmbracoContext("/test", SearchTestFiles.subpage1);

            Mock<ISearchService> searchService = new Mock<ISearchService>();
            Mock<IValidateBus> validateBus = new Mock<IValidateBus>();
            Mock<IUmbracoService> umbracoService = new Mock<IUmbracoService>();
            Mock<ISearcher> examineSearch = new Mock<ISearcher>();

            validateBus.Setup(x => x.Validate(It.IsAny<SearchQuery>())).Returns(validateResult);
            searchService.Setup(x => x.GetSearchResult(It.IsAny<SearchQuery>())).Returns(searchResult);
            examineSearch.Setup(x => x.Search(It.IsAny<ISearchCriteria>())).Returns(It.IsAny<ISearchResults>());


            umbracoService.Setup(x => x.GetNodeById(1)).Returns(GetNode(1173));
            umbracoService.Setup(x => x.GetNodeById(2)).Returns(GetNode(1174));
            umbracoService.Setup(x => x.GetNodeById(3)).Returns(GetNode(1176));

            SearchController searchController = new SearchController(validateBus.Object, searchService.Object, umbracoService.Object);

            var result = searchController.GetSearch("test");

            Assert.Greater(result.Count, 0);

        }

        [Test, Category("Search_Service_UnitTest")]
        public void TestSearch_Service_Successful()
        {
            var newIndexFolder = new DirectoryInfo(Path.Combine(TestHelper.CurrentAssemblyDirectory, @"App_Data\SearchTests"));

            System.IO.Directory.CreateDirectory(newIndexFolder.FullName);

            using (var luceneDir = new SimpleFSDirectory(newIndexFolder))
            {
                SearchQuery query = new SearchQuery() { Keyword = "Content" };

                var indexer = IndexInitializer.GetUmbracoIndexer(luceneDir, null,
                    new TestDataService()
                    {
                        ContentService = new TestContentService(SearchTestFiles.subpage1)
                    });

                indexer.RebuildIndex();

                var searcher = IndexInitializer.GetUmbracoSearcher(luceneDir);
                searcher.EnableLeadingWildcards = true;

                ISearchService service = new SearchService(new UmbracoHelper(UmbracoContext.Current), searcher);

                var result = service.GetSearchResult(query) as IOrderedEnumerable<Examine.SearchResult>;

                Assert.Greater(result.Count(), 0);
            }
        }


    }
}
