﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Tests.UnitTest.Base;
using UmbracoExamine.DataServices;

namespace Umbraco.Tests.UnitTest.Search
{
    public class TestDataService : IDataService
    {
        public TestDataService()
        {
            ContentService = new TestContentService();
            LogService = new TestLogService();
            MediaService = new TestMediaService();
        }
        public IContentService ContentService { get; internal set; }

        public ILogService LogService { get; internal set; }

        public IMediaService MediaService { get; internal set; }

        public string MapPath(string virtualPath)
        {
            return new DirectoryInfo(TestHelper.CurrentAssemblyDirectory) + "\\" + virtualPath.Replace("/", "\\");
        }
    }
}
