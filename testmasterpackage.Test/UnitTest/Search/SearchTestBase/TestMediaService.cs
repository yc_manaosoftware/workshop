﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.XPath;
using Umbraco.Tests.UnitTest.Search.TestFiles;
using UmbracoExamine.DataServices;

namespace Umbraco.Tests.UnitTest.Search
{
    public class TestMediaService : IMediaService
    {
        public TestMediaService()
        {
            m_Doc = XDocument.Parse(SearchTestFiles.media);
        }

        #region IMediaService Members

        public System.Xml.Linq.XDocument GetLatestMediaByXpath(string xpath)
        {
            var xdoc = XDocument.Parse("<media></media>");
            xdoc.Root.Add(m_Doc.XPathSelectElements(xpath));
            return xdoc;
        }

        #endregion

        private XDocument m_Doc;
    }
}
