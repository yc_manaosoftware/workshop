﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.XPath;
using Umbraco.Tests.UnitTest.Search.TestFiles;
using UmbracoExamine;
using UmbracoExamine.DataServices;

namespace Umbraco.Tests.UnitTest.Search
{
    public class TestContentService : IContentService
    {
        public const int ProtectedNode = 1142;
        private readonly XDocument _xContent;
        private readonly XDocument _xMedia;

        public TestContentService(string contentXml = null, string mediaXml = null)
        {
            if (contentXml == null)
            {
                contentXml = SearchTestFiles.umbraco;
            }
            if (mediaXml == null)
            {
                mediaXml = SearchTestFiles.media;
            }
            _xContent = XDocument.Parse(contentXml);
            _xMedia = XDocument.Parse(mediaXml);
        }

        #region IContentService Members

        /// <summary>
        /// Return the XDocument containing the xml from the umbraco.config xml file
        /// </summary>
        /// <param name="xpath"></param>
        /// <returns></returns>
        /// <remarks>
        /// This is no different in the test suite as published content
        /// </remarks>
        public XDocument GetLatestContentByXPath(string xpath)
        {
            var xdoc = XDocument.Parse("<content></content>");
            xdoc.Root.Add(_xContent.XPathSelectElements(xpath));

            return xdoc;
        }

        /// <summary>
        /// Return the XDocument containing the xml from the umbraco.config xml file
        /// </summary>
        /// <param name="xpath"></param>
        /// <returns></returns>
        public XDocument GetPublishedContentByXPath(string xpath)
        {
            return GetContentByXPath(xpath, _xContent);
        }

        private XDocument GetContentByXPath(string xpath, XDocument content)
        {
            var xdoc = XDocument.Parse("<content></content>");
            xdoc.Root.Add(content.XPathSelectElements(xpath));

            return xdoc;
        }

        public string StripHtml(string value)
        {
            const string pattern = @"<(.|\n)*?>";
            return Regex.Replace(value, pattern, string.Empty);
        }

        public bool IsProtected(int nodeId, string path)
        {
            // single node is marked as protected for test indexer
            // hierarchy is not important for this test
            return nodeId == ProtectedNode;
        }

        private List<string> _userPropNames;
        public IEnumerable<string> GetAllUserPropertyNames()
        {
            if (_userPropNames == null)
            {
                var xpath = "//*[count(@id)>0 and @id != -1]";
                _userPropNames = GetPublishedContentByXPath(xpath)
                    .Root
                    .Elements() //each page
                    .SelectMany(x => x.Elements().Where(e => e.Attribute("id") == null)) //each page property (no @id)         
                    .Select(x => x.Name.LocalName) //the name of the property
                    .Distinct()
                    .Union(GetContentByXPath(xpath, _xMedia)
                               .Root
                               .Elements() //each page
                               .SelectMany(x => x.Elements().Where(e => e.Attribute("id") == null)) //each page property (no @id)         
                               .Select(x => (string) x.Attribute("alias")) //the name of the property NOTE: We are using the legacy XML here.
                               .Distinct()).ToList();
            }
            return _userPropNames;
        }

        private List<string> _sysPropNames;
        public IEnumerable<string> GetAllSystemPropertyNames()
        {
            if (_sysPropNames == null)
            {
                _sysPropNames = UmbracoContentIndexer.IndexFieldPolicies.Select(x => x.Name).ToList();
            }
            return _sysPropNames;
        }

        #endregion
    }
}
