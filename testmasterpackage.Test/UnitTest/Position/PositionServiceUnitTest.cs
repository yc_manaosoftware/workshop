﻿using testmasterpackage.Test.UnitTest.Base;
using NUnit.Framework;

namespace testmasterpackage.Test.UnitTest
{
    [TestFixture]
    public class PositionServiceUnitTest : UnitTestBase
    {
        //protected Mock<IPositionValidationService> positionValidationService = new Mock<IPositionValidationService>();

        //[Test, Category("Position_Service_UnitTest")]
        //public void GetAll_Successful()
        //{
        //    var unitOfWork = this.UnitOfWorkMock;

        //    IPositionService service = IoCContainer.GetInstance<IPositionService>();

        //    var position = service.GetPositions();

        //    Assert.IsTrue(position != null);
        //}

        //[Test, Category("Position_Service_UnitTest")]
        //public void Add_Successful()
        //{
        //    var unitOfWork = this.UnitOfWorkMock;
        //    var positionRepositoryMock = this.PositionRepositoryMock;

        //    unitOfWork.Setup(x => x.PositionRepository).Returns(positionRepositoryMock.Object);
        //    positionRepositoryMock.Setup(x => x.AddPosition(It.IsAny<Position>())).Returns(true);

        //    IValidateBus validateBus = IoCContainer.GetInstance<IValidateBus>();

        //    Position position = new Position() { PositionName = "Test", IsManager = true };

        //    var validateResult = validateBus.Validate(position);

        //    PositionService service = new PositionService(unitOfWork.Object);

        //    var result = service.AddPosition(position);


        //    Assert.IsTrue(validateResult.IsValid);
        //    Assert.IsTrue(result.IsSuccess);

        //    positionRepositoryMock.Verify(x => x.AddPosition(It.IsAny<Position>()), Times.Exactly(1));
        //    unitOfWork.Verify(x => x.Commit(), Times.Exactly(1));
        //}

        //[Test, Category("Position_Service_UnitTest")]
        //public void Add_Name_Length_Failed()
        //{
        //    var unitOfWork = this.UnitOfWorkMock;
        //    var positionRepositoryMock = this.PositionRepositoryMock;

        //    unitOfWork.Setup(x => x.PositionRepository).Returns(positionRepositoryMock.Object);

        //    IValidateBus validateBus = IoCContainer.GetInstance<IValidateBus>();
        //    Position position = new Position() { PositionName = new string('0', 101), IsManager = true };

        //    var validateResult = validateBus.Validate(position);

        //    PositionService service = new PositionService(unitOfWork.Object);
        //    var result = service.AddPosition(position);

        //    Assert.IsFalse(validateResult.IsValid);
        //    Assert.IsTrue(validateResult.Errors.Any(x => x.PropertyName.Equals(nameof(position.PositionName))));
        //}

        //[Test, Category("Position_Service_UnitTest")]
        //public void Add_Name_Duplicate_Failed()
        //{
        //    var unitOfWork = this.UnitOfWorkMock;
        //    var positionRepositoryMock = this.PositionRepositoryMock;

        //    unitOfWork.Setup(x => x.PositionRepository).Returns(positionRepositoryMock.Object);


        //    IValidateBus validateBus = IoCContainer.GetInstance<IValidateBus>();
        //    Position position = new Position() { PositionName = new string('0', 101), IsManager = true };
        //    var validateResult = validateBus.Validate(position);

        //    Assert.IsFalse(validateResult.IsValid);
        //    Assert.IsTrue(validateResult.Errors.Any(x => x.PropertyName.Equals(nameof(position.PositionName))));
        //}
    }
}
