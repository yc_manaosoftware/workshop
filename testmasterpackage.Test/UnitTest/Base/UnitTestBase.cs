﻿using testmasterpackage.Core.Repositories;
using testmasterpackage.Core.Services;
using testmasterpackage.Test.IoC;
using Moq;
using NUnit.Framework;
using StructureMap;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Configuration.UmbracoSettings;
using Umbraco.Tests.IoC;
using Umbraco.Tests.UnitTest.Base;
using Umbraco.Web;
using Umbraco.Web.Routing;
using Umbraco.Web.Security;

namespace testmasterpackage.Test.UnitTest.Base
{
    [TestFixture]
    public abstract class UnitTestBase : BaseRoutingTest/*PublishedContentTestBase*/
    {
        protected IContainer IoCContainer { get; set; }


        public override void Initialize()
        {
            base.Initialize();
            IoCContainer = StructureMapContainerInit.InitilizeTestContainer();

            //FakeUmbracoContext();
            var appCtx = CreateApplicationContext();
            FakeUmbracoContext(appCtx);

            IoCContainer.Configure(x => x.For<UmbracoHelper>().Use(new UmbracoHelper(UmbracoContext.Current)));
            IoCContainer.Configure(x => x.For<Examine.ExamineManager>().Use(Examine.ExamineManager.Instance));

            AutoMapperConfiguration.Configure();
        }

        protected ServiceResult ServiceResultSuccess { get { return new ServiceResult(); } }
        protected Mock<Core.Repositories.Base.IUnitOfWork> UnitOfWorkMock { get { return new Mock<Core.Repositories.Base.IUnitOfWork>(); } }
        //protected Mock<IPositionRepository> PositionRepositoryMock { get { return new Mock<IPositionRepository>(); } }

        private void FakeUmbracoContext(ApplicationContext appCtx)
        {
            //var appCtx = new ApplicationContext(
            //   CacheHelper.CreateDisabledCacheHelper(),
            //   new ProfilingLogger(Mock.Of<ILogger>(), Mock.Of<IProfiler>()));

            //var appCtx = CreateApplicationContext();

            // Mock Umbraco Context
            var umbCtx = UmbracoContext.EnsureContext(
                Mock.Of<HttpContextBase>(),
                appCtx,
                new Mock<WebSecurity>(null, null).Object,
                Mock.Of<IUmbracoSettingsSection>(),
                Enumerable.Empty<IUrlProvider>(),
                true);

            // Mock UmbracoHelper
            //var helper = new UmbracoHelper(umbCtx,
            //    Mock.Of<IPublishedContent>(),
            //    Mock.Of<ITypedPublishedContentQuery>(),
            //    Mock.Of<IDynamicPublishedContentQuery>(),
            //    Mock.Of<ITagQuery>(),
            //    Mock.Of<IDataTypeService>(),
            //    new UrlProvider(umbCtx, new[] { Mock.Of<IUrlProvider>() }, UrlProviderMode.Auto), Mock.Of<ICultureDictionary>(),
            //    Mock.Of<IUmbracoComponentRenderer>(),
            //    new MembershipHelper(umbCtx, Mock.Of<MembershipProvider>(), Mock.Of<RoleProvider>()));
        }
    }
}
