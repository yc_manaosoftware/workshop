﻿using testmasterpackage.Core.DomainModel;
using testmasterpackage.Dispatcher;
using testmasterpackage.Test.UnitTest.Base;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Umbraco.Tests.UnitTest.News
{
    [TestFixture]
    public class NewsTest : UnitTestBase
    {
        [Test, Category("News_UnitTest")]
        public void Add_Name_Length_Failed()
        {
            IValidateBus validateBus = IoCContainer.GetInstance<IValidateBus>();
            testmasterpackage.Core.DomainModel.News position = new testmasterpackage.Core.DomainModel.News();

            var validateResult = validateBus.Validate(position);

            Assert.IsFalse(validateResult.IsValid);
        }

        [Test, Category("News_UnitTest")]
        public void Add_Name_Length_Failed2()
        {
            IValidateBus validateBus = IoCContainer.GetInstance<IValidateBus>();
            testmasterpackage.Core.DomainModel.News position = new testmasterpackage.Core.DomainModel.News()
            {
                Title = "ll",
                Details = "sdsds"
            };

            var validateResult = validateBus.Validate(position);

            Assert.IsTrue(validateResult.IsValid);
        }

    }
}
