﻿using testmasterpackage.Core.Repositories.Base;
using testmasterpackage.Infrastructure.DAL.EF;
using testmasterpackage.Test.IoC;
using Moq;
using NUnit.Framework;
using StructureMap;
using System;
using System.Data.Entity;
using System.Security.Principal;
using System.Web;
using umbraco.BusinessLogic;
using Umbraco.Tests.IoC;

namespace Umbraco.Tests.IntegrationTests
{
    [TestFixture]
    public class IntegrationTestBase
    {
        private DbContextTransaction transaction;

        #region Properties
        protected IContainer IoCContainer { get; set; }
        protected Mock<IUnitOfWork> UnitOfWorkMock { get { return new Mock<IUnitOfWork>(); } }

        protected IUnitOfWork UnitOfWork
        {
            get
            {
                return this.IoCContainer.GetInstance<IUnitOfWork>();
            }
        }

        #endregion

        [SetUp]
        public void SetupTest()
        {
            IoCContainer = StructureMapContainerInit.InitilizeTestContainer();
            AutoMapperConfiguration.Configure();
            EfUnitOfWork context = (EfUnitOfWork) IoCContainer.GetInstance<IUnitOfWork>();
            context.Database.Initialize(force: true);
            transaction = context.Database.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);
        }

        [TearDown]
        public void TearDownTest()
        {
            transaction.Rollback();
        }

        protected void FakeApplicationContext(User user)
        {
            HttpContext.Current = FakeHttpContext(user, IoCContainer);
        }

        public static HttpContext FakeHttpContext(User user, IContainer container = null)
        {
            var httpRequest = new HttpRequest("", "http://stackoverflow/", "");
            var stringWriter = new System.IO.StringWriter();
            var httpResponse = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httpRequest, httpResponse);

            var sessionContainer = new System.Web.SessionState.HttpSessionStateContainer("id", new System.Web.SessionState.SessionStateItemCollection(),
                                                    new HttpStaticObjectsCollection(), 10, true,
                                                    HttpCookieMode.AutoDetect,
                                                    System.Web.SessionState.SessionStateMode.InProc, false);

            httpContext.Items["AspSession"] = typeof(System.Web.SessionState.HttpSessionState).GetConstructor(
                                        System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance,
                                        null, System.Reflection.CallingConventions.Standard,
                                        new[] { typeof(System.Web.SessionState.HttpSessionStateContainer) },
                                        null)
                                .Invoke(new object[] { sessionContainer });

            httpContext.User = new GenericPrincipal(new GenericIdentity(user.Email), null);
            try
            {
                httpContext.Items.Add("User", user);
                httpContext.Items.Add("IoC", container);
            }
            catch (Exception)
            {
                httpContext.Items["User"] = user;
            }

            return httpContext;
        }
    }
}
