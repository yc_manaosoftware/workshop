﻿using AutoMapper;
using testmasterpackage.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace Umbraco.Tests.IoC
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.CreateMap<IPublishedContent, SearchResultViewModel>()
                .ForMember(dest => dest.Title, opt => { opt.MapFrom(source => source.HasValue("menuTitle") ? source.GetPropertyValue("menuTitle").ToString() : source.Name); })
                .ForMember(dest => dest.Content, opt => { opt.MapFrom(source => source); })
                ;
        }
    }
}
