﻿using testmasterpackage.Core.DomainModel.Base;
using testmasterpackage.Core.Repositories.Base;
using testmasterpackage.Core.Services.SearchServices;
using testmasterpackage.Core.Services.UmbracoServices;
using testmasterpackage.Core.ValidationHandler.Base;
using testmasterpackage.Dispatcher;
using testmasterpackage.Infrastructure.DAL.EF;
using testmasterpackage.Infrastructure.Mock;
using testmasterpackage.Infrastructure.UmbracoServices;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Web;

namespace testmasterpackage.Test.IoC
{
    internal class StructureMapContainerInit
    {
        public static IContainer InitilizeTestContainer()
        {
            var container = new Container(c => c.AddRegistry<TestRegistry>());
            return container;
        }
    }

    internal class TestRegistry : MockRegistry
    {
        #region Constructors and Destructors

        public TestRegistry() : base()
        {
            Scan(
                scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                    scan.AssemblyContainingType<IDomainModel>();
                    scan.AddAllTypesOf<IDomainModel>();
                    scan.ConnectImplementationsToTypesClosing(typeof(IValidationHandler<>));
                });

            Policies.SetAllProperties(prop =>
            {
                prop.OfType<IContainer>();
            });

            For<IValidateBus>().Use<DefaultValidateBus>();
            // Command
            //For<ICommandBus>().Use<DefaultCommandBus>();
            //For<IQueryParser>().Use<DefaultQueryParser>();

            // Unit Of Work
            For<EfUnitOfWork>().Use<EfUnitOfWork>().Named("UnitOfWorkObject");
            For<IUnitOfWork>().Use(x => x.GetInstance<EfUnitOfWork>("UnitOfWorkObject"));

            For<DbContext>().Singleton().Use<EfUnitOfWork>(x => x.GetInstance<EfUnitOfWork>("UnitOfWorkObject"));

            //Umbraco Content Service
            //For<UmbracoHelper>().Use(new UmbracoHelper(UmbracoContext.Current));
            //For<IPositionService>().Use<PositionService>();
            For<ISearchService>().Use<SearchService>();
            For<IUmbracoService>().Use<UmbracoService>();
        }
        #endregion
    }
}
