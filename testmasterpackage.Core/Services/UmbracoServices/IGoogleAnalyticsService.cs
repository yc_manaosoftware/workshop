﻿using testmasterpackage.Core.DomainModel.GoogleAnalytics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace testmasterpackage.Core.Services.UmbracoServices
{
    public interface IGoogleAnalyticsService
    {
        GoogleAnalytics GetGoogleAnalytics(Guid currentNodeId);
    }
}
