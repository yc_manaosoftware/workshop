﻿using System;

namespace testmasterpackage.Core.Services.UmbracoServices
{
    public interface IRobotsService
    {
        string GetRobotsContent(int rootNodeId);
        int GetCurrentContent();
        int GetFirstContent();
        bool HasRobotsNode(Guid content);

    }
}
