﻿using testmasterpackage.Core.DomainModel.CookiesAlert;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Core.Services.UmbracoServices
{
    public interface ICookiesAlertService
    {
        CookiesAlert GetCookiesAlert(Guid cookiesAlertNodeId);
    }
}
