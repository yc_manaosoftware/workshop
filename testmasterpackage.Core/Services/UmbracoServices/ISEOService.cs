﻿using testmasterpackage.Core.DomainModel.SEO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Core.Services.UmbracoServices
{
    public interface ISEOService
    {
        SEO GetSeoContent(Guid currentNodeId);
    }
}
