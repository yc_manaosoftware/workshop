﻿using testmasterpackage.Core.DomainModel.Footer;
using testmasterpackage.Core.DomainModel.Header;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Core.Services.UmbracoServices
{
    public interface IConfigurationService
    {
        string GetFavoriteIcon(Guid contentGuid);
        Header GetHeaderContent(Guid contentGuid); 
        Footer GetFooterContent(Guid contentGuid); 
    }
}
