﻿using System.Collections.Generic;
using System.Globalization;

namespace testmasterpackage.Core.Services.UmbracoServices
{
    public interface IDictionaryService
    {
        IDictionary<string, string> GetDictionary(string locale, string rootKey);
        IDictionary<string, string> GetDictionary(string rootKey);
        string GetDictionaryItem(CultureInfo language, string key);
    }
}
