﻿using testmasterpackage.Core.DomainModel.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Core.Services.UmbracoServices
{
    public interface IManaoMediaService
    {
        ManaoImage GetImageByGuid(Guid guid);
        String GetMediaUrl(int id);
        String GetMediaAlt(string name);
    }
}
