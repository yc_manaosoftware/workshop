﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using testmasterpackage.Core.DomainModel.Editors;

namespace testmasterpackage.Core.Services.SerializerServices
{
    public interface ILinkModelSerializerService : ISerializerService
    {
        LinkModel GetDeserializeLinkModel(string modelJson);
        LinkModel UpdateModel(LinkModel linkModel);
    }
}
