﻿using testmasterpackage.Core.DomainModel.Editors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Core.Services.SerializerServices
{
    public interface INodeModelSerializerService : ISerializerService
    {
        NodeModel GetDeserializeNodeModel(string modelJson);
        List<NodeModel> GetDeserializeNodeModels(string modelJsons);
        NodeModel UpdateModel(Guid id);
    }
}
