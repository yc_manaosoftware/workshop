﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Core.Services.SerializerServices
{
    public interface ISerializerService
    {
        JObject GetUpdateJsonModel(JObject model);
        List<JObject> GetUpdateJsonModels(List<JObject> models);
    }
}
