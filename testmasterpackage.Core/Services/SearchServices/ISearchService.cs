﻿using testmasterpackage.Core.DomainModel.Search;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Core.Services.SearchServices
{
    public interface ISearchService
    {
        IEnumerable GetSearchResult(SearchQuery query);

        SearchResultModel GetSearchSummary(int nodeId);
    }
}
