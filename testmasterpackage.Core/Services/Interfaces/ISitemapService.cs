﻿namespace testmasterpackage.Services.Interfaces
{
    public interface ISitemapService
    {
        string GetSitemapXML(int contentNodeId, string scheme, string httpHost);
    }
}
