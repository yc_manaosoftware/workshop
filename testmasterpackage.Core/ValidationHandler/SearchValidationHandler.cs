﻿using testmasterpackage.Core.DomainModel.Search;
using testmasterpackage.Core.ValidationHandler.Base;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Core.ValidationHandler
{
    public class SearchValidationHandler : CustomAbstractValidator<SearchQuery>
    {
        public SearchValidationHandler()
        {
            RuleFor(x => x.Keyword).NotEmpty().WithMessage("Field Keyword is require");
            RuleFor(x => x.Keyword).NotEmpty().Length(0, 100).WithMessage("Field Keyword is not meet length");
        }
    }
}
