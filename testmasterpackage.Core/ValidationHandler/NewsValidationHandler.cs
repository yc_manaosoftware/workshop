﻿using testmasterpackage.Core.DomainModel;
using testmasterpackage.Core.ValidationHandler.Base;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Core.ValidationHandler
{
    public class NewsValidationHandler : CustomAbstractValidator<News>
    {
        public NewsValidationHandler()
        {
            RuleFor(x => x.Title).NotEmpty().WithMessage("Title Eror");
            RuleFor(x => x.Details).NotEmpty().WithMessage("Detail error");
        }
    }
}
