﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Core.DomainModel.Editors
{
    public class LinkModel
    {
        public int Id { get; set; }
        public Guid Guid { get; set; } 
        public string Url { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Target { get; set; }
        public bool IsMedia { get; set; }
        public bool IsInternal { get; set; }
        public bool IsExternal { get; set; }
    }
}
