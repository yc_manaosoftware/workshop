﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Core.DomainModel.Enums
{
    public enum MediaType
    {
        [Description("Image")]
        Image,
        [Description("Media")]
        Media,
    }
}
