﻿using testmasterpackage.Core.DomainModel.Enums;
using System;

namespace testmasterpackage.Core.DomainModel.Media
{
    public class ManaoImage
    {
        public Guid Guid { get; set; }
        public string Url { get; set; }
        public string Title { get; set; }
        public string Alt { get; set; }
        public string Caption { get; set; }
        public MediaType Type { get; set; }
    }
}
