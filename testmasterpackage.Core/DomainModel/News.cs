﻿using testmasterpackage.Core.DomainModel.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Core.DomainModel
{
    [DocumentTypeAlias(Name = "news")]
    public class News : Base.DomainModel
    {
        public string Title { get; set; }
        public string Details { get; set; }

  
    }
}
