﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Core.DomainModel.Base
{
    public abstract class DomainModel : IDomainModel
    {
        public int Id { get; set; }
        public CultureInfo CurrentCulture { get; set; }

    }
}
