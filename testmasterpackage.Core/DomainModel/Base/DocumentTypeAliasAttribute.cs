﻿using System;

namespace testmasterpackage.Core.DomainModel.Base
{
    public class DocumentTypeAliasAttribute : Attribute
    {
        public string Name { get; set; }
    }
}
