﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Core.DomainModel.SEO
{
    public class SEO
    {
        public string KeywordSEO { get; set; }
        public string DescriptionSEO { get; set; }
        public string TitleSEO { get; set; }
        public string Noindex { get; set; }
        public string Nofollow { get; set; }

    }
}
