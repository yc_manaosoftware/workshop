﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Core.DomainModel.Robots
{
    public class Robots
    {
        public int Option { get; set; }
        public string Content { get; set; }
        public string ShowContent { get; set; }
    }
}
