﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Core.DomainModel.GoogleAnalytics
{
    public class GoogleAnalytics
    {
        public string TrackingCode { get; set; }
    }
}
