﻿using testmasterpackage.Core.DomainModel.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Core.DomainModel.CookiesAlert
{
    public class CookiesAlert
    {
        public Guid Id { get; set; }
        public int ExplicitConsent { get; set; }
        public string Position { get; set; }
        public int Duration { get; set; }
        public int Limit { get; set; }
        public string Message { get; set; }
        public string AcceptText { get; set; }
        public string ButtonText { get; set; }
    }
}
