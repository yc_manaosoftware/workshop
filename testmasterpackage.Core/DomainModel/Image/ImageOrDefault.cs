﻿using testmasterpackage.Core.DomainModel.Media;
using testmasterpackage.Core.DomainModel.Base;
using testmasterpackage.Core.DomainModel.Editors;

namespace testmasterpackage.Core.DomainModel.Image
{
    public class ImageOrDefault
    {
        public ManaoImage Image { get; set; }
        public LinkModel Link { get; set; }
    }
}
