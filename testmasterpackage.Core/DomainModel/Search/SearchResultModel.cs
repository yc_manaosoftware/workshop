﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Core.DomainModel.Search
{
    public class SearchResultModel
    {
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Url { get; set; }
        public dynamic Content { get; set; }
        public IEnumerable Tags { get; set; }
    }
}
