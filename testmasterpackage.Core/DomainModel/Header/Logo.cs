﻿using testmasterpackage.Core.DomainModel.Editors;
using testmasterpackage.Core.DomainModel.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Core.DomainModel.Header
{
    public class Logo
    {
        public bool hasLink { get; set; }
        public bool hasImage { get; set; }
        public ManaoImage Image { get; set; }
        public LinkModel Link { get; set; }
    }
}
