﻿using testmasterpackage.Core.DomainModel.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Core.DomainModel.Header
{
    public class Favicon : Base.DomainModel
    {
        public string Src { get; set; }
    }
}
