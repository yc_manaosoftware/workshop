﻿using testmasterpackage.Core.DomainModel.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Core.DomainModel.Header
{
    public class Header : Base.DomainModel
    {
        public bool hasHeader { get; set; }
        public Logo Logo { get; set; }
        public List<Menu> Menus { get; set; }
    }
}
