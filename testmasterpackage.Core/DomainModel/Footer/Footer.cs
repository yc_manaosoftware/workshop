﻿using testmasterpackage.Core.DomainModel.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Core.DomainModel.Footer
{
    public class Footer : Base.DomainModel
    {
        public bool HasFooter { get; set; }
    }
}
