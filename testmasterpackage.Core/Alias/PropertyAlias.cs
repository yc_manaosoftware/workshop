﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Core.Alias
{
    public class PropertyAlias
    {
        //Header
        public static readonly string FavariteIcon = "favoriteIcon";
        public static readonly string Logo = "logo";
        public static readonly string LogoLink = "logoLink";
        public static readonly string HideFromMenu = "hideFromMenu";
        public static readonly string MenuTitle = "menuTitle";

        //Configuration
        public static readonly string Configuration = "configuration";

        //Google Analytics
        public static readonly string TrackingCode = "googleAnalyticsTrackingCode";

        //Cookies Alert
        public static readonly string Consent = "consent";
        public static readonly string AlertPosition = "alertPosition";
        public static readonly string AlertContent = "alertContent";
        public static readonly string AlertDisplayTime = "alertDisplayTime";
        public static readonly string AlertTimes = "alertTimes";

        //404 Redirect
        public static readonly string PageTarget = "PageTarget";

        //Sitemap.xml
        public static readonly string SitemapXml = "sitemapXml";
        public static readonly string ShowInSitemapxml = "showInSiteMapXml";
        public static readonly string ChangeFrequency = "changeFrequency";
        public static readonly string SitemapPriority = "sitemapPriority";

        //Robots.txt
        public static readonly string Robots = "robots";
        public static readonly string RobotsContent = "robotsContent";
        public static readonly string RobotsCustom = "robotsCustom";
        public static readonly string BlockInRobots = "blockInRobots";
        public static readonly string IncludeSitemap = "includeSitemap";

        //SEO 
        public static readonly string SEOKeyword = "seoKeywords";
        public static readonly string SEOTitle = "seoTitle";
        public static readonly string SEODescription = "seoDescription";
        public static readonly string SEONoIndex = "seoNoindex";
        public static readonly string SEONoFollow = "seoNofollow";

        //Error Redirect
        public static readonly string ErrorRedirectPage = "errorRedirectPage";

    }
}
