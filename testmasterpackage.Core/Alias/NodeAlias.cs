﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testmasterpackage.Core.Alias
{
    public class NodeAlias
    {
        public static readonly string Homepage = "homePage";
        public static readonly string Subpage = "subPage";
        public static readonly string Configuration = "configuration";
        public static readonly string Header = "header";
        public static readonly string CookiesAlert = "cookiesAlert";
        public static readonly string GoogleAnalytics = "googleAnalytics";
        public static readonly string Footer = "footer";
        public static readonly string Robots = "robots";
        public static readonly string FourOFourRedirect = "fourOFourRedirect";
        public static readonly string ErrorRedirect = "errorRedirect";
        public static readonly string XMLSitemap = "sitemapXml";
    }
}
