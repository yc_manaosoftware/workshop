﻿using testmasterpackage.Core.Services.UmbracoServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Xml;
using umbraco.cms.businesslogic.packager.standardPackageActions;
using umbraco.interfaces;
using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;

namespace testmasterpackage.Core.Services.SetupServices
{
    public class ImportMedia : IPackageAction
    {
        private static readonly string defaultMediaFolderName = "MANAO TEST MEDIA";
        private static readonly string defaultMediaSourcePath = "/images/manao test media/";
        private static readonly List<string> imageExtensions = new List<string> { ".jpg", ".jpe", ".bmp", ".gif", ".png", ".jpeg", ".tif" };
        private static readonly List<string> allowUploadExtensions = new List<string> {".pdf",".txt",".zip",".rar",".svg",".ico",".wav", ".mid", ".midi", ".wma",
            ".mp3", ".ogg",".webm", ".rma", ".avi", ".mp4", ".divx", ".wmv", ".asmx", ".doc", ".docx", ".xml" };
        Umbraco.Core.Services.IMediaService mediaService;
        public ImportMedia()
        {
            mediaService = ApplicationContext.Current.Services.MediaService;
        }
        public string Alias()
        {
            return "ImportTestMedia";
        }
        public bool Execute(string packageName, XmlNode xmlData)
        {
            try
            {
                var rootFolder = CreateFolder(defaultMediaFolderName, -1, "Folder");

                DirectoryInfo directoryInfo = new DirectoryInfo(HttpContext.Current.Server.MapPath(string.Format(defaultMediaSourcePath)));
                if (directoryInfo.Exists)
                {
                    ProcessDirectory(directoryInfo, rootFolder.Id);
                }

                //Deletes all source files
                ClearSourceImages(directoryInfo);
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Error(GetType(), "Import Media Error: ", ex);
                return false;
            }
        }
        private void ProcessDirectory(DirectoryInfo directoryInfo, int parentFolderId)
        {
            var fileEntries = directoryInfo.GetFiles();
            foreach (var file in fileEntries)
            {
                if (allowUploadExtensions.Contains(file.Extension) || imageExtensions.Contains(file.Extension))
                {
                    var newmedia = mediaService.CreateMedia(file.Name, parentFolderId, imageExtensions.Contains(file.Extension) ? "Image" : "File");
                    FileStream s = new FileStream(file.FullName, FileMode.Open);
                    newmedia.SetValue("umbracoFile", file.Name, s);
                    s.Close();
                    mediaService.Save(newmedia);
                }
            }
            var subdirectoryEntries = directoryInfo.GetDirectories();
            foreach (var subdirectory in subdirectoryEntries)
            {
                var newFolder = CreateFolder(subdirectory.Name, parentFolderId, "Folder");
                mediaService.Save(newFolder);
                ProcessDirectory(subdirectory, newFolder.Id);
            }
        }
        public XmlNode SampleXml()
        {
            string sample = string.Format("<Action runat=\"install\" undo=\"false\" alias=\"{0}\"/>", Alias());
            return helper.parseStringToXmlNode(sample);
        }
        public bool Undo(string packageName, XmlNode xmlData)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(HttpContext.Current.Server.MapPath(string.Format(defaultMediaSourcePath)));
            return ClearSourceImages(directoryInfo);
        }
        private bool ClearSourceImages(DirectoryInfo directoryInfo)
        {
            var fileEntries = directoryInfo.GetFiles();
            foreach (var file in fileEntries)
            {
                file.Delete();
            }
            var subdirectoryEntries = directoryInfo.GetDirectories();
            foreach (var directory in subdirectoryEntries)
            {
                directory.Delete(true);
            }
            return true;
        }
        private IMedia CreateFolder(string name, int parentId, string mediaAlias)
        {
            var media = mediaService.CreateMedia(name, parentId, mediaAlias);
            mediaService.Save(media);
            return media;
        }
        private Dictionary<string, string> GetTestMediaFolder()
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(HttpContext.Current.Server.MapPath(string.Format(defaultMediaSourcePath)));
            Dictionary<string, string> mediaFolders = new Dictionary<string, string>();
            mediaFolders.Add("1", "Images");
            mediaFolders.Add("2", "Images with size");
            return mediaFolders;
        }
    }
}
