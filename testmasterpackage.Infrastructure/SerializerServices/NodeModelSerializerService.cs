﻿using testmasterpackage.Core.Services.SerializerServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testmasterpackage.Core.DomainModel.Editors;
using Newtonsoft.Json.Linq;
using Umbraco.Core.Logging;
using Newtonsoft.Json;
using testmasterpackage.Core.Services.UmbracoServices;
using Umbraco.Core.Models;
using testmasterpackage.Infrastructure.UmbracoServices;

namespace testmasterpackage.Infrastructure.SerializerServices
{
    public class NodeModelSerializerService : INodeModelSerializerService
    {
        private UmbracoService umbracoService;
        public NodeModelSerializerService(UmbracoService umbracoService)
        {
            this.umbracoService = umbracoService;
        }
        public JObject GetUpdateJsonModel(JObject model)
        {
            try
            {
                if (model != null && !string.IsNullOrEmpty(model.Value<string>("Guid")))
                {
                    if ((Guid)model["Guid"] != Guid.Empty)
                    {
                        NodeModel nodeModel = UpdateModel((Guid)model["Guid"]);

                        if (nodeModel.Id > 0)
                        {
                            model.Remove("DocumentTypeId");
                            model.Add("DocumentTypeId", nodeModel.DocumentTypeId);

                            model.Remove("Id");
                            model.Add("Id", nodeModel.Id);

                            model.Remove("Name");
                            model.Add("Name", nodeModel.Name);

                            model.Remove("Url");
                            model.Add("Url", nodeModel.Url);
                        }
                        else
                        {
                            model = null;
                        }

                    }

                }

                return model;
            }
            catch (Exception e)
            {
                LogHelper.Error<string>("Error on GetUpdateJsonModel()", e);

                return null;
            }
        }

        public List<JObject> GetUpdateJsonModels(List<JObject> models)
        {
            try
            {
                List<JObject> updatedValue = null;

                if (models != null)
                {
                    updatedValue = new List<JObject>();

                    for (int i = 0; i < models.Count; i++)
                    {
                        JObject model = GetUpdateJsonModel(models[i]);

                        if (model != null)
                        {
                            updatedValue.Add(model);
                        }
                    }
                }

                return updatedValue;
            }
            catch (Exception e)
            {
                LogHelper.Error<string>("Error on GetUpdateJsonModel()", e);

                return null;
            }
        }

        public NodeModel GetDeserializeNodeModel(string modelJson)
        {
            try
            {
                NodeModel nodeModel = null;

                if (!string.IsNullOrEmpty(modelJson))
                {
                    nodeModel = JsonConvert.DeserializeObject<NodeModel>(modelJson);

                    nodeModel = UpdateModel(nodeModel.Guid);

                }

                return nodeModel;

            }
            catch (Exception e)
            {
                LogHelper.Error<string>("Error on GetDeserializeNodeModel()", e);

                return null;
            }
        }

        public List<NodeModel> GetDeserializeNodeModels(string modelJsons)
        {
            try
            {
                List<NodeModel> nodeModels = null;

                if (!string.IsNullOrEmpty(modelJsons))
                {
                    nodeModels = JsonConvert.DeserializeObject<List<NodeModel>>(modelJsons);

                    for (int i = 0; i < nodeModels.Count; i++)
                    {
                        nodeModels[i] = UpdateModel(nodeModels[i].Guid);
                    }

                }

                return nodeModels;

            }
            catch (Exception e)
            {
                LogHelper.Error<string>("Error on GetDeserializeNodeModels()", e);

                return null;
            }
        }

        public NodeModel UpdateModel(Guid id)
        {
            try
            {
                NodeModel nodeModel = null;

                IPublishedContent node = umbracoService.GetContentByGuid(id);

                if (node != null)
                {
                    nodeModel = AutoMapper.Mapper.Map<NodeModel>(id);
                }
                else
                {
                    nodeModel = null;
                }

                return nodeModel;
            }
            catch (Exception e)
            {
                LogHelper.Error<string>("Error on UpdateModel()", e);

                return null;
            }
        }
    }
}
