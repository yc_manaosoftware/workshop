﻿using testmasterpackage.Core.Services.SerializerServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testmasterpackage.Core.DomainModel.Editors;
using Newtonsoft.Json.Linq;
using Umbraco.Core.Services;
using Umbraco.Core.Models;
using testmasterpackage.Core.DomainModel.Media;
using Umbraco.Core.Logging;
using Newtonsoft.Json;

namespace testmasterpackage.Infrastructure.SerializerServices
{
    public class LinkModelSerializerService : ILinkModelSerializerService
    {
        private IMediaService mediaService;
        private INodeModelSerializerService nodeModelSerializerService;
        public LinkModelSerializerService(IMediaService _mediaService, INodeModelSerializerService _nodeModelSerializerService)
        {
            mediaService = _mediaService;
            nodeModelSerializerService = _nodeModelSerializerService;
        }

        public JObject GetUpdateJsonModel(JObject model)
        {
            try
            {
                if (model != null && !string.IsNullOrEmpty(model.Value<string>("Guid")))
                {
                    if ((Guid)model["Guid"] != Guid.Empty)
                    {
                        IMedia media = mediaService.GetById((Guid)model["Guid"]);

                        ManaoImage mediaModel = AutoMapper.Mapper.Map<ManaoImage>(media);

                        if ((media != null && !media.Trashed)
                            && (mediaModel != null && !string.IsNullOrEmpty(mediaModel.Url)))
                        {
                            model.Remove("Url");
                            model.Add("Url", mediaModel.Url);
                        }
                        else
                        {
                            model = null;
                        }
                    }
                }

                return model;
            }
            catch (Exception e)
            {
                LogHelper.Error<string>("Error on GetUpdateJsonModel()", e);

                return null;
            }

        }

        public LinkModel GetDeserializeLinkModel(string modelJson)
        {
            try
            {
                LinkModel linkModel = null;

                if (!string.IsNullOrEmpty(modelJson))
                {
                    linkModel = JsonConvert.DeserializeObject<LinkModel>(modelJson);

                    linkModel = UpdateModel(linkModel);

                }

                return linkModel;
            }
            catch (Exception e)
            {
                LogHelper.Error<string>("Error on GetDeserializeLinkModel()", e);

                return null;
            }
        }

        public LinkModel UpdateModel(LinkModel linkModel)
        {
            try
            {
                if (linkModel != null && !string.IsNullOrEmpty(linkModel.Url))
                {
                    if (linkModel.IsMedia && linkModel.Guid != Guid.Empty)
                    {
                        IMedia media = mediaService.GetById(linkModel.Guid);

                        ManaoImage mediaModel = AutoMapper.Mapper.Map<ManaoImage>(media);

                        if ((media != null && !media.Trashed)
                            && (mediaModel != null && !string.IsNullOrEmpty(mediaModel.Url)))
                        {
                            linkModel.Url = mediaModel.Url;
                        }
                    }
                    else if (linkModel.IsInternal && linkModel.Guid != Guid.Empty)
                    {
                        NodeModel nodeModel = nodeModelSerializerService.UpdateModel(linkModel.Guid);

                        if (nodeModel.Id > 0)
                        {
                            linkModel.Url = nodeModel.Url;

                            linkModel.Name = nodeModel.Name;

                            linkModel.Id = nodeModel.Id;

                        }
                        else
                        {
                            linkModel = null;
                        }
                    }
                    else if (linkModel.IsExternal)
                    {
                        linkModel.Url = AsUrl(linkModel.Url);
                    }

                }

                return linkModel;
            }
            catch (Exception e)
            {
                LogHelper.Error<string>("Error on UpdateModel()", e);

                return null;
            }
        }

        //Intentionally return new List<JObject>()
        public List<JObject> GetUpdateJsonModels(List<JObject> models)
        {
            return new List<JObject>();
        }

        private string AsUrl(string url)
        {
            if (!string.IsNullOrEmpty(url))
            {
                Uri output;
                if (!url.ToLower().StartsWith("mailto:") && !url.ToLower().StartsWith("tel:"))
                {
                    url = !url.StartsWith("http") && !url.Contains("media") ? "http://" + url : url;
                }

                if (Uri.TryCreate(url, UriKind.Absolute, out output))
                {
                    url = output.AbsoluteUri;
                }
            }
            return url;
        }
    }
}
