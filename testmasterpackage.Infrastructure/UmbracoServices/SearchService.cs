﻿using testmasterpackage.Core.Services.SearchServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testmasterpackage.Core.DomainModel.Search;
using static Umbraco.Core.Constants;
using System.Text.RegularExpressions;
using Examine.SearchCriteria;
using System.Collections;
using Umbraco.Web;
using UmbracoExamine;
using Examine.LuceneEngine.SearchCriteria;

namespace testmasterpackage.Infrastructure.UmbracoServices
{
    public class SearchService : ISearchService
    {
        private UmbracoHelper umbracoHelper;
        private UmbracoExamineSearcher searcher;
        public SearchService(UmbracoHelper umbracoHelper, UmbracoExamineSearcher searcher)
        {
            this.umbracoHelper = umbracoHelper;
            this.searcher = searcher;
        }
        public IEnumerable GetSearchResult(SearchQuery searchQuery)
        {
            searchQuery.Keyword = searchQuery.Keyword.MultipleCharacterWildcard().Value;

            var criteria = searcher.CreateSearchCriteria(BooleanOperation.Or);

            string rawQuery = string.Format("nodeName:{0} content:*{0}*", searchQuery.Keyword);
            var query = criteria.RawQuery(rawQuery);

            var searchResults = searcher.Search(criteria).OrderByDescending(x => x.Score);

            return searchResults;
        }

        public SearchResultModel GetSearchSummary(int nodeId)
        {
            var content = umbracoHelper.TypedContent(nodeId);
            SearchResultModel result = new SearchResultModel();
            return result;
        }


        protected static string ReplaceMultipleSpaceWithString(string text, string replaceText)
        {
            text = Regex.Replace(text, @"\s+", " ");
            var words = text.Split(' ');

            text = string.Empty;
            foreach (var word in words)
            {
                text += string.Format("*{0}* ", word);
            }
            return text;
        }
    }
}
