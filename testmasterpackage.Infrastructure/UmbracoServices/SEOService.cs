﻿using testmasterpackage.Core.Alias;
using testmasterpackage.Core.DomainModel.SEO;
using testmasterpackage.Core.Services.UmbracoServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Web;
using testmasterpackage.Core.Extensions;
using testmasterpackage.Infrastructure.Extensions;

namespace testmasterpackage.Infrastructure.UmbracoServices
{
    public class SEOService : ISEOService
    {
        private UmbracoService umbracoService;
        public SEOService(UmbracoService umbracoService)
        {
            this.umbracoService = umbracoService;
        }

        public SEO GetSeoContent(Guid currentNodeId)
        {
            IPublishedContent currentPage = umbracoService.GetContentByGuid(currentNodeId);
 
            SEO seo = new SEO();
            if (currentPage != null)
            {
                //Assign value to keywordSEO when metaKeywords property has a value
                seo.KeywordSEO = currentPage.AsString(PropertyAlias.SEOKeyword).Replace(" ", ", ");
                seo.TitleSEO = currentPage.AsString(PropertyAlias.SEOTitle, currentPage.Name);
                //Assign value to descriptionSEO when metaDescription property has value
                seo.DescriptionSEO = currentPage.AsString(PropertyAlias.SEODescription);
                seo.Noindex = !currentPage.AsBoolean(PropertyAlias.SEONoIndex) ? "noindex" : "index";
                seo.Nofollow = !currentPage.AsBoolean(PropertyAlias.SEONoFollow) ? "nofollow" : "follow";
            }

            return seo;
        }

    }
}
