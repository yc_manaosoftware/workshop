﻿using testmasterpackage.Core.Services.UmbracoServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testmasterpackage.Core.DomainModel.Header;
using Umbraco.Web;
using Umbraco.Core.Models;
using testmasterpackage.Core.Alias;
using testmasterpackage.Core.DomainModel.Media;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using testmasterpackage.Core.DomainModel.Editors;
using testmasterpackage.Core.DomainModel.Footer;
using testmasterpackage.Core.Services.SerializerServices;

namespace testmasterpackage.Infrastructure.UmbracoServices
{
    public class ConfigurationService : IConfigurationService
    {
        private IManaoMediaService mediaService;
        private UmbracoHelper umbracoHelper;
        private UmbracoService umbracoService;
        private ILinkModelSerializerService linkModelSerializer;

        public ConfigurationService
            (
                IManaoMediaService mediaService, 
                UmbracoService umbracoService,
                ILinkModelSerializerService _linkModelSerializer,
                UmbracoHelper umbracoHelper
            )
        {
            this.umbracoHelper = umbracoHelper;
            this.mediaService = mediaService;
            this.umbracoService = umbracoService;
            linkModelSerializer = _linkModelSerializer;
        }

        public string GetFavoriteIcon(Guid contentGuid)
        {
            const string defaultUmbracoFavIcon = "/umbraco/Images/PinnedIcons/umb.ico";
            string faviconSrc = defaultUmbracoFavIcon;

            IPublishedContent faviconSetting = umbracoService.GetConfiguration(contentGuid, NodeAlias.Header);
            if (faviconSetting != null)
            {
                if (faviconSetting.HasValue("favoriteIcon"))
                {
                    ManaoImage favIcon = JsonConvert.DeserializeObject<ManaoImage>(faviconSetting.GetPropertyValue("favoriteIcon").ToString());
                    if (favIcon != null && !string.IsNullOrEmpty(favIcon.Url))
                    { 
                        faviconSrc = favIcon.Url; 
                    }
                }
            }
            return faviconSrc;
        }

        public Header GetHeaderContent(Guid contentGuid)
        {
            IPublishedContent currentPage = umbracoService.GetContentByGuid(contentGuid);
            IPublishedContent headerNode = umbracoService.GetConfiguration(contentGuid, NodeAlias.Header);
            Header header = new Header();
            if (headerNode != null)
            {
                header.hasHeader = true;
                header.Logo = GetLogo(headerNode);
                header.Menus = GetListMenus(currentPage);
            }
            return header;
        }

        public Footer GetFooterContent(Guid contentGuid)
        {
            //Implementation goes here ! 
            //Need to Implement this for footer section
            IPublishedContent footerNode = umbracoService.GetConfiguration(contentGuid, NodeAlias.Footer);
            var footer = new Footer();
            if (footerNode != null)
            {
                footer.HasFooter = true;
            }
            return footer; 
        }
        private Logo GetLogo(IPublishedContent header)
        {
            Logo logo = new Logo();

            if (header.HasValue(PropertyAlias.Logo))
            {
                ManaoImage imageLogo = JsonConvert.DeserializeObject<ManaoImage>(header.GetPropertyValue(PropertyAlias.Logo).ToString());

                if (imageLogo != null && !string.IsNullOrEmpty(imageLogo.Url))
                {
                    logo.Image = imageLogo;
                    logo.hasImage = true;
                }
            }

            if (header.HasValue(PropertyAlias.LogoLink))
            {
                LinkModel linkLogo = JsonConvert.DeserializeObject<LinkModel>(header.GetPropertyValue(PropertyAlias.LogoLink).ToString());
                linkLogo = linkModelSerializer.UpdateModel(linkLogo);
                if (linkLogo != null && !string.IsNullOrEmpty(linkLogo.Url))
                {
                    logo.Link = linkLogo;
                    logo.hasLink = true;
                }
            }

            return logo;
        }
        private List<Menu> GetListMenus(IPublishedContent currentPage)
        {
            List<Menu> menus = new List<Menu>();
            IPublishedContent home = umbracoService.GetHome(currentPage.GetKey());
            foreach (IPublishedContent page in home.Children.Where(n => n.DocumentTypeAlias != NodeAlias.Configuration))
            {
                Menu menu = GetMenu(page, currentPage);
                if (menu != null)
                {
                    menus.Add(menu);
                }
            }
            return menus;
        }
        private Menu GetMenu(IPublishedContent page, IPublishedContent currentPage)
        {
            Menu menu = new Menu();
            if (page.HasValue(PropertyAlias.HideFromMenu))
            {
                if (page.GetPropertyValue<string>(PropertyAlias.HideFromMenu).Equals("1"))
                {
                    menu.IsActive = currentPage.Id == page.Id || page.Descendants().Where(child => child.Id == currentPage.Id).Any();
                    string active = menu.IsActive ? " active" : string.Empty;
                    string openMenu = menu.IsActive && page.Level == 3 && currentPage.Level == 4 ? " openMenu" : string.Empty;
                    menu.Class = string.Format("{0} {1}", active, openMenu);
                    menu.Url = page.Url;
                    menu.Name = page.HasValue(PropertyAlias.MenuTitle) ? page.GetPropertyValue<string>(PropertyAlias.MenuTitle) : page.Name;
                    menu.HasChild = page.Children.Where(child => child.HasValue(PropertyAlias.HideFromMenu) ? child.GetPropertyValue<string>(PropertyAlias.HideFromMenu).Equals("1") : false).Any();
                    if (menu.HasChild)
                    {
                        if (page.Level < 4)
                        {
                            List<Menu> list = new List<Menu>();
                            foreach (var item in page.Children)
                            {
                                Menu sub = GetMenu(item, currentPage);
                                if (sub != null)
                                {
                                    list.Add(sub);
                                }

                            }
                            menu.SubMenu = list;
                        }
                    }

                }
                else
                {
                    menu = null;
                }
            }
            return menu;
        }
    }
}
