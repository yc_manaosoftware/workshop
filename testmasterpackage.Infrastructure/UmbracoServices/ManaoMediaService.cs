﻿using testmasterpackage.Core.Services.UmbracoServices;
using System;
using testmasterpackage.Core.DomainModel.Media;
using Umbraco.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Core;
using System.IO;

namespace testmasterpackage.Infrastructure.UmbracoServices
{
    public class ManaoMediaService : IManaoMediaService
    {
        private UmbracoHelper umbracoHelper;
        private IMediaService mediaService;
        public ManaoMediaService(UmbracoHelper umbracoHelper, IMediaService mediaService)
        {
            this.umbracoHelper = umbracoHelper;
            this.mediaService = mediaService;
        }
        public ManaoImage GetImageByGuid(Guid guid)
        {
            ManaoImage image = new ManaoImage();
            var media = mediaService.GetById(guid);
            if (media != null)
            {
                image = AutoMapper.Mapper.Map<ManaoImage>(media);
            }
            return image;
        }

        public string GetMediaAlt(string name)
        {
            return Path.GetFileNameWithoutExtension(name);
        }

        public string GetMediaUrl(int id)
        {
            return umbracoHelper.TypedMedia(id).Url;
        }
    }
}
