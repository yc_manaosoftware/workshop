﻿using testmasterpackage.Core.Alias;
using testmasterpackage.Core.Services.UmbracoServices;
using testmasterpackage.Core.DomainModel.Robots;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace testmasterpackage.Infrastructure.UmbracoServices
{
    public class RobotsService : IRobotsService
    {
        private Robots robots;
        private IPublishedContent currentContent;
        private bool HasrobotsConfigsNode;
        private UmbracoHelper umbracoHelper;
        public RobotsService(UmbracoHelper umbracoHelper)
        {
            this.umbracoHelper = umbracoHelper;
        }
        public int GetCurrentContent()
        {
            IEnumerable<IPublishedContent> rootNodes = umbracoHelper.ContentAtRoot();
            foreach (IPublishedContent rootNode in rootNodes)
            {
                if (rootNode.Url.Equals("/"))
                {
                    currentContent = rootNode;
                    break;
                }

                var domainService = Umbraco.Core.ApplicationContext.Current.Services.DomainService;

                var hostnames = domainService.GetAll(false);

                if (hostnames != null)
                {
                    foreach (var hostname in hostnames)
                    {
                        if (!hostname.DomainName.StartsWith("*"))
                        {
                            if (HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.AbsolutePath, "/") == hostname.DomainName)
                            {
                                currentContent = rootNode;
                                break;
                            }
                        }
                    }
                }
            }
            return currentContent.Id;
        }
        public int GetFirstContent()
        {
            foreach (IPublishedContent firstRootNode in umbracoHelper.ContentAtRoot())
            {
                currentContent = firstRootNode;
                break;
            }
            return currentContent.Id;
        }
        public string GetRobotsContent(int rootNodeId)
        {
            string robotContent = string.Empty;
            bool includeSitemap = false;
            var rootNode = umbracoHelper.TypedContent(rootNodeId);
            var config = rootNode.Children.Where(c => c.ContentType.Alias.Equals(PropertyAlias.Configuration));
            var configNode = config.FirstOrDefault();
            if (configNode != null)
            {

                var robotsConfig = configNode.Children.Where(c => c.ContentType.Alias.Equals(PropertyAlias.Robots)).FirstOrDefault();
                var sitemapConfig = configNode.Children.Where(c => c.ContentType.Alias.Equals(PropertyAlias.SitemapXml)).FirstOrDefault();
                if (robotsConfig != null)
                {
                    string robotsJsonText = robotsConfig.GetPropertyValue<string>(PropertyAlias.RobotsContent);

                    if (sitemapConfig != null)
                    {
                        includeSitemap = IsIncludeSitemap(sitemapConfig);
                    }

                    if (includeSitemap)
                    {
                        robotContent += string.Format("Sitemap: http://{0}/sitemap.xml\n", HttpContext.Current.Request.Url.Host);
                    }

                    if (!string.IsNullOrEmpty(robotsJsonText))
                    {
                        robots = Newtonsoft.Json.JsonConvert.DeserializeObject<Robots>(robotsJsonText);
                        robotContent += robots.Content;
                    }

                    if (robots != null)
                    {
                        if (robots.Option == 2)
                        {
                            string queryResult = string.Empty;
                            QueryContentThatSetRobotsToBlock(rootNode, ref queryResult);
                            robotContent += queryResult;
                        }
                    }

                    robotContent += GetCustomText(robotsConfig);
                }
            }

            return robotContent;
        }
        public bool HasRobotsNode(Guid contentId)
        {
            HasrobotsConfigsNode = false;
            var content = umbracoHelper.TypedContent(contentId);
            if (content != null)
            {
                var Configs = content.Children.Where(c => c.ContentType.Alias.Equals(PropertyAlias.Configuration));
                if (Configs.Count() > 0)
                {
                    var configNode = Configs.First();
                    var robotsConfigs = configNode.Children.Where(c => c.ContentType.Alias.Equals(PropertyAlias.Robots));
                    if (robotsConfigs.Count() > 0)
                    {
                        HasrobotsConfigsNode = true;
                    }
                }
            }
            return HasrobotsConfigsNode;
        }
        private bool IsIncludeSitemap(IPublishedContent content)
        {
            bool includeSitemap = false;
            if (content.HasValue(PropertyAlias.IncludeSitemap))
            {
                if (content.GetPropertyValue<string>(PropertyAlias.IncludeSitemap).Equals("1"))
                {
                    includeSitemap = true;
                }
            }
            return includeSitemap;
        }
        private void QueryContentThatSetRobotsToBlock(IPublishedContent rootContent, ref string queryResult)
        {
            if (rootContent.TemplateId > 0)
            {
                bool isParentBlock = false;
                if (rootContent.HasValue(PropertyAlias.BlockInRobots))
                {
                    if (rootContent.GetPropertyValue<string>(PropertyAlias.BlockInRobots).Equals("0"))
                    {
                        queryResult += string.Format(" \nDisallow: {0}", rootContent.Url);
                        isParentBlock = true;
                    }
                }

                if (!isParentBlock)
                {
                    foreach (var content in rootContent.Children)
                    {
                        if (content.Children.Count() > 0)
                        {
                            QueryContentThatSetRobotsToBlock(content, ref queryResult);
                        }
                        else
                        {
                            if (content.HasValue(PropertyAlias.BlockInRobots))
                            {
                                if (content.GetPropertyValue<string>(PropertyAlias.BlockInRobots).Equals("0"))
                                {
                                    queryResult += string.Format("\nDisallow: {0}", content.Url);
                                }
                            }
                        }
                    }
                }
            }
        }
        private string GetCustomText(IPublishedContent content)
        {
            string manualContent = string.Empty;
            if (content.HasValue(PropertyAlias.RobotsCustom))
            {
                manualContent += "\n" + content.GetPropertyValue<string>(PropertyAlias.RobotsCustom);
            }
            return manualContent;
        }
    }
}
