﻿using System.Linq;
using System.Web;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Routing;
using System;
using testmasterpackage.Core.Alias;

namespace testmasterpackage.Infrastructure.UmbracoServices
{
    public class FourOFourRedirectService : IContentFinder
    {
        public bool TryFindContent(PublishedContentRequest contentRequest)
        {
            IPublishedContent rootContent = null;
            IPublishedContent redirectPage = null;

            if (contentRequest.Is404)
            {
                UmbracoHelper umbracoHelper = new UmbracoHelper(UmbracoContext.Current);

                foreach (IPublishedContent contentAtRoot in umbracoHelper.ContentAtRoot())
                {
                    if (contentAtRoot.Url().Equals("/"))
                    {
                        // Select home node
                        rootContent = contentAtRoot;
                        break;
                    }
                }

                if (rootContent != null)
                {
                    var configNode = rootContent.Children.Where(c => c.ContentType.Alias.Equals(NodeAlias.Configuration)).FirstOrDefault();

                    if (configNode != null)
                    {
                        var fourOFourConfigNode = configNode.Children.Where(c => c.ContentType.Alias.Equals(NodeAlias.FourOFourRedirect)).FirstOrDefault();

                        if (fourOFourConfigNode != null && fourOFourConfigNode.HasValue(PropertyAlias.PageTarget))
                        {
                            //404 page
                            //redirectPage = umbracoHelper.TypedContent(int.Parse(fourOFourConfigNode.GetPropertyValue(PropertyAlias.PageTarget).ToString()));
                            redirectPage = fourOFourConfigNode.GetPropertyValue<IPublishedContent>(PropertyAlias.PageTarget);
                        }
                    }
                }
            }

            LogHelper.Info(GetType(), string.Format("Page not found for '{0}'", HttpContext.Current.Request.Url.AbsoluteUri));

            if (redirectPage != null)
            {
                contentRequest.PublishedContent = redirectPage;
                return contentRequest.PublishedContent != null;
            }
            else
            {
                return false;
            }

            return false;
        }
    }
}
