﻿using testmasterpackage.Core.Alias;
using testmasterpackage.Core.Services.UmbracoServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace testmasterpackage.Infrastructure.UmbracoServices
{
    public class UmbracoService
    {
        private UmbracoHelper umbracoHelper;
        public UmbracoService(UmbracoHelper umbracoHelper)
        {
            this.umbracoHelper = umbracoHelper;
        }
        public IPublishedContent GetNodeById(int id)
        {
            IPublishedContent content = umbracoHelper.TypedContent(id);

            return content;
        }

        public IPublishedContent GetContentByGuid(Guid id)
        {
            IPublishedContent content = umbracoHelper.TypedContent(id);
            
            return content;
        }

        public IMedia GetMediaByGuid(Guid id)
        {
            var service = ApplicationContext.Current.Services.MediaService;

            var media = service.GetById(id);

            return media;
        }

        public IContentType GetContentTypeById(int id)
        {
            IContentType content = ApplicationContext.Current.Services.ContentTypeService.GetContentType(id);

            return content;
        }

        public IPublishedContent GetHome(Guid contentId)
        {
            IPublishedContent content = umbracoHelper.TypedContent(contentId);
            return content.AncestorOrSelf(NodeAlias.Homepage);
        }
        public IPublishedContent GetConfiguration(Guid currentContentId, string configAlias)
        { 
            string configurationPropertyValue = string.Empty;
            IPublishedContent configurationNode = null;
            if (umbracoHelper.TypedContent(currentContentId) != null)
            {
                IPublishedContent content = umbracoHelper.Content(currentContentId);
                IPublishedContent homeNode = content.AncestorOrSelf(NodeAlias.Homepage);
                IPublishedContent configCollectionNode = homeNode.Children.Where(n => n.DocumentTypeAlias == NodeAlias.Configuration).FirstOrDefault();
                if (configCollectionNode != null)
                {
                    configurationNode = configCollectionNode.Children.Where(n => n.DocumentTypeAlias == configAlias).FirstOrDefault();
                }
            }
            return configurationNode;
        }
    }
}
