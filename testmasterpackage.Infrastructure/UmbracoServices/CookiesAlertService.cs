﻿using testmasterpackage.Core.Services.UmbracoServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testmasterpackage.Core.DomainModel.CookiesAlert;
using Umbraco.Core.Models;
using System.Text.RegularExpressions;
using Umbraco.Web.Templates;
using Umbraco.Web;
using testmasterpackage.Core.Alias;
using umbraco;

namespace testmasterpackage.Infrastructure.UmbracoServices
{
    public class CookiesAlertService : ICookiesAlertService
    {
        private UmbracoService umbracoService;
        public CookiesAlertService(UmbracoService umbracoService)
        {
            this.umbracoService = umbracoService;
        }

        public CookiesAlert GetCookiesAlert(Guid nodeId)
        {
            IPublishedContent cookiesAlertNode = umbracoService.GetConfiguration(nodeId, NodeAlias.CookiesAlert);

            CookiesAlert cookiesAlert = null;

            if (cookiesAlertNode != null)
            {
                cookiesAlert = new CookiesAlert();
                cookiesAlert.Id = cookiesAlertNode.GetKey();
                cookiesAlert.ExplicitConsent = cookiesAlertNode.GetPropertyValue<bool>(PropertyAlias.Consent) ? 1 : 0;
                cookiesAlert.Position = cookiesAlertNode.GetPropertyValue<bool>(PropertyAlias.AlertPosition) ? "Top" : "Bottom";
                cookiesAlert.Duration = cookiesAlertNode.GetPropertyValue<int>(PropertyAlias.AlertDisplayTime);
                cookiesAlert.Limit = cookiesAlertNode.GetPropertyValue<int>(PropertyAlias.AlertTimes);
                cookiesAlert.Message = cookiesAlertNode.HasValue(PropertyAlias.AlertContent) ? GetAlertMessage(cookiesAlertNode.GetPropertyValue<string>(PropertyAlias.AlertContent)) : string.Empty;
                cookiesAlert.AcceptText = cookiesAlertNode.GetPropertyValue<bool>(PropertyAlias.Consent) ? GetButtonText(library.GetDictionaryItem("[Cookies Alert] Explicit Accept Text")) : string.Empty;
                cookiesAlert.ButtonText = cookiesAlertNode.GetPropertyValue<bool>(PropertyAlias.Consent) ? GetButtonText(library.GetDictionaryItem("[Cookies Alert] Explicit Button Text")) : GetButtonText(library.GetDictionaryItem("[Cookies Alert] Implicit Button Text"));

            }

            return cookiesAlert;
        }

        private string GetButtonText(string text)
        {
            return Regex.Replace(text, @"\s+", " ").Trim();
        }

        private string GetAlertMessage(string message)
        {
            message = Regex.Replace(message, @"\s+", " ");
            message = message.Replace("'", "\\'");
            message = TemplateUtilities.ParseInternalLinks(message);
            return message;
        }
    }
}
