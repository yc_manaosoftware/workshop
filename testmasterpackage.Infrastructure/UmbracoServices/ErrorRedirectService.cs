﻿using System.Linq;
using System.Web;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Routing;
using System;
using testmasterpackage.Core.Alias;

namespace testmasterpackage.Infrastructure.UmbracoServices
{
    public class ErrorRedirectService
    {
        private UmbracoHelper umbracoHelper;
        public ErrorRedirectService(UmbracoHelper umbracoHelper)
        {
            this.umbracoHelper = umbracoHelper;
        }
        public int GetContentId()
        {
            IPublishedContent rootContent = null;
            IPublishedContent redirectPage = null;
            int contentId = 0;
            foreach (IPublishedContent contentAtRoot in umbracoHelper.ContentAtRoot())
            {
                rootContent = contentAtRoot.Url().Equals("/") ? contentAtRoot : null;
            }

            if (rootContent != null)
            {
                var errorHandlerConfigNode = rootContent.Descendants().Where(c => c.ContentType.Alias.Equals(NodeAlias.ErrorRedirect)).FirstOrDefault();
                if (errorHandlerConfigNode != null && errorHandlerConfigNode.HasValue(PropertyAlias.ErrorRedirectPage))
                {
                    redirectPage = umbracoHelper.TypedContent(errorHandlerConfigNode.GetPropertyValue<int>(PropertyAlias.ErrorRedirectPage));
                    contentId = redirectPage != null ? redirectPage.Id : 0;
                }
            }
            return contentId;
        }
    }
}
