﻿using testmasterpackage.Core.Alias;
using testmasterpackage.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Helpers;
using System.Xml.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace testmasterpackage.Infrastructure.Services
{
    public class SitemapService : ISitemapService
    {
        private UmbracoHelper umbracoHelper;
        public SitemapService(UmbracoHelper umbracoHelper)
        {
            this.umbracoHelper = umbracoHelper;
        }
        public string GetSitemapXML(int rootContentNodeId, string scheme, string httpHost)
        {
            IPublishedContent rootContent = umbracoHelper.TypedContent(rootContentNodeId);
            XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
            return new XElement(ns + "urlset",
                new XAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9"),
                new XAttribute(XNamespace.Xmlns + "xsi", "http://www.w3.org/2001/XMLSchema-instance"),
                new XAttribute(xsi + "schemaLocation", "http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"),
                from content in GetContents(rootContent)
                select new XElement(ns + "url",
                    new XElement(ns + "loc", GetSitemapUrl(content, scheme, httpHost)),
                    new XElement(ns + "lastmod", GetSitemapLastModified(content)),
                    !String.IsNullOrEmpty(GetSitemapchangeFrequency(content)) ? new XElement(ns + "changefreq", GetSitemapchangeFrequency(content)) : null,
                    !String.IsNullOrEmpty(GetSitemapPriority(content)) ? new XElement(ns + "priority", GetSitemapPriority(content)) : null
                )
            ).ToString();
        }

        private List<IPublishedContent> GetContents(IPublishedContent rootContent)
        {
            List<IPublishedContent> contents = new List<IPublishedContent>();
            if (rootContent.HasProperty(PropertyAlias.ShowInSitemapxml))
            {
                if (rootContent.GetPropertyValue<bool>(PropertyAlias.ShowInSitemapxml))
                {
                    contents.Add(rootContent);
                    GetChildContents(rootContent, contents);
                }
            }
            return contents;
        }

        private void GetChildContents(IPublishedContent content, List<IPublishedContent> contents)
        {
            foreach (IPublishedContent c in content.Children)
            {
                if (c.HasProperty(PropertyAlias.ShowInSitemapxml))
                {
                    if (c.GetPropertyValue<bool>(PropertyAlias.ShowInSitemapxml))
                    {
                        contents.Add(c);
                        if (content.Children.Count() > 0)
                        {
                            GetChildContents(c, contents);
                        }
                    }
                }
            }
        }

        private static string GetSitemapUrl(IPublishedContent content, string scheme, string httpHost)
        {
            string url = content.Url;
            if (url.StartsWith("/"))
            {
                url = url.Substring(1);
            }
            var domainPrefix = string.Format("{0}://{1}/", scheme, httpHost);
            return url.StartsWith(domainPrefix) ? url : domainPrefix + url;
        }
        private static string GetSitemapLastModified(IPublishedContent content)
        {
            return string.Format("{0:s}+00:00", content.UpdateDate);
        }
        private static string GetSitemapchangeFrequency(IPublishedContent content)
        {
            string changeFrequency = string.Empty;
            string[] frequenciesList = { "always", "hourly", "daily", "weekly", "monthly", "yearly", "never" };
            if (content.HasProperty(PropertyAlias.ChangeFrequency) && frequenciesList.Contains(content.GetPropertyValue<string>(PropertyAlias.ChangeFrequency)))
            {
                changeFrequency = content.GetPropertyValue<string>(PropertyAlias.ChangeFrequency);
            }
            return changeFrequency;
        }
        private static string GetSitemapPriority(IPublishedContent content)
        {
            string priority = string.Empty;
            string value = content.GetPropertyValue<string>(PropertyAlias.SitemapPriority);
            if (!string.IsNullOrEmpty(value))
            {
                var sitemapSetting = Json.Decode(value);
                if (sitemapSetting.state == 1)
                {
                    priority = sitemapSetting.priority.ToString();
                }
            }
            return priority;
        }
    }
}
