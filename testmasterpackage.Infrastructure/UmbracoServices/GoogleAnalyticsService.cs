﻿using testmasterpackage.Core.Alias;
using testmasterpackage.Core.DomainModel.GoogleAnalytics;
using testmasterpackage.Core.Services.UmbracoServices;
using testmasterpackage.Infrastructure.Extensions;
using System;
using System.Web;
using Umbraco.Core.Models;

namespace testmasterpackage.Infrastructure.UmbracoServices
{
    public class GoogleAnalyticsService : IGoogleAnalyticsService
    {
        private UmbracoService umbracoService;
        public GoogleAnalyticsService(UmbracoService umbracoService)
        {
            this.umbracoService = umbracoService;
        }

        public GoogleAnalytics GetGoogleAnalytics(Guid currentNodeId)
        {
            IPublishedContent analyticsNode = umbracoService.GetConfiguration(currentNodeId, NodeAlias.GoogleAnalytics);

            GoogleAnalytics googleAnalytics = null;

            if(analyticsNode != null)
            {
                IPublishedContent cookiesAlertNode = umbracoService.GetConfiguration(currentNodeId, NodeAlias.CookiesAlert);
                HttpCookie cookiesDirective = HttpContext.Current.Request.Cookies["cookiesDirective"];

                googleAnalytics = new GoogleAnalytics();

                googleAnalytics.TrackingCode = GetTrackingCode(analyticsNode, cookiesAlertNode, cookiesDirective);
            }

            return googleAnalytics;
        }

        private string GetTrackingCode(IPublishedContent analyticsNode,IPublishedContent cookiesAlertNode, HttpCookie cookiesDirective)
        {
            string trackingCode = string.Empty;

            if (cookiesAlertNode != null)
            {
                if (!cookiesAlertNode.AsBoolean(PropertyAlias.Consent))
                {
                    trackingCode = analyticsNode.AsString(PropertyAlias.TrackingCode, string.Empty);
                }
                else if (cookiesDirective != null)
                {
                    trackingCode = analyticsNode.AsString(PropertyAlias.TrackingCode, string.Empty);
                }
            }
            return trackingCode;
        }
    }
}
