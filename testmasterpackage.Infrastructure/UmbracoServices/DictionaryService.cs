﻿using System.Collections.Generic;
using System.Globalization;
using Umbraco.Web.Dictionary;
using System.Linq;
using Umbraco.Core.Services;
using testmasterpackage.Core.Services.UmbracoServices;

namespace testmasterpackage.Infrastructure.UmbracoServices
{
    public class DictionaryService : IDictionaryService
    {
        private ILocalizationService localizationService;
        public DictionaryService(ILocalizationService localizationService)
        {
            this.localizationService = localizationService;
        }
        public IDictionary<string, string> GetDictionary(string locale,string key)
        {
            //Get all dictionary that created under current key.

            DefaultCultureDictionary defaultCultureDictionary = new DefaultCultureDictionary(CultureInfo.GetCultureInfo(locale));

            IDictionary<string, string> dictionary = new Dictionary<string, string>();
            return GetDictionary(defaultCultureDictionary,key, ref dictionary);
        }

        public IDictionary<string, string> GetDictionary(string key)
        {
            //Get all dictionary that created under current key.

            DefaultCultureDictionary defaultCultureDictionary = new DefaultCultureDictionary();

            IDictionary<string, string> dictionary = new Dictionary<string, string>();
            return GetDictionary(defaultCultureDictionary, key, ref dictionary);
        }

        public string GetDictionaryItem(CultureInfo language, string key)
        {
            string dictionaryValue = string.Empty;
            var found = localizationService.GetDictionaryItemByKey(key);
            if (found != null)
            {
                var byLang = found.Translations.FirstOrDefault(x => x.Language.IsoCode==language.IetfLanguageTag);
                if (byLang != null)
                {
                    dictionaryValue = byLang.Value;
                }
            }
            return dictionaryValue;
        }
        private IDictionary<string, string> GetDictionary(DefaultCultureDictionary defaultCultureDictionary,string key, ref IDictionary<string, string> dictionary)
        {
            foreach (var item in defaultCultureDictionary.GetChildren(key))
            {

                dictionary.Add(item);
                if (defaultCultureDictionary.GetChildren(item.Key).Any())
                {
                    GetDictionary(defaultCultureDictionary,item.Key, ref dictionary);
                }
            }
            return dictionary;
        }

    }
}
